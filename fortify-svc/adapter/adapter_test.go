package adapter

import (
	pb "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"encoding/json"
	"gitlab.com/bloom42/libs/rz-go"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
	"time"
	"context"
)

var generic = &pb.Generic{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
}

var user1 = &pb.User{
	Meta: &pb.Meta{
		RequestId: "1",
		Status:    200,
		Requester: "requester",
	},
	Id:        "2",
	Username:  "test1",
	Email:     "test1@test.com",
	FirstName: "test1FirstName",
	LastName:  "test1LastName",
}

var user2 = &pb.User{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
	Id:        "3",
	Username:  "test2",
	Email:     "test2@test.com",
	FirstName: "test2FirstName",
	LastName:  "test2LastName",
}


var createUser1 = fortifyUserRequest{
	UserName:              user1.Username,
	Pwd:                   "aaoy8734j5kl5a9n2v55972nalj542ngsl823",
	Email:                 user1.Email,
	FirstName:             user1.FirstName,
	LastName:              user1.LastName,
	PasswordNeverExpire:   true,
	RequirePasswordChange: false,
	Roles:                 []fortifyUserRoleRequest{{ID: "viewonly"}},
}

var unauthorizedResponse = `{
    "message": "Access Denied. Unauthorized access",
    "responseCode": 401,
    "errorCode": -10301
}`

func TestForityAdapter(t *testing.T) {

	ur := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			testCreateUserRequest(w, r, t)
		case "GET":
			testListUsersRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})
	rur := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {

		case "DELETE":
			testRemoveUserRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})
	tr := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			testTokenRequest(w, r, t)
		default:
			t.Errorf("error invalid token request, %v", r.Method)
		}

	})
	handler := http.NewServeMux()
	handler.HandleFunc("/localUsers", ur)
	handler.HandleFunc("/localUsers/2", rur)

	handler.HandleFunc("/" + "tokens", tr)

	srv := httptest.NewServer(handler)
	defer srv.Close()

	os.Setenv("FORTIFY_BASE_URL", srv.URL )
	os.Setenv("FORTIFY_API_USERNAME", "admin")
	os.Setenv("FORTIFY_API_PASSWORD", "password")



	adapter := FortifyAdapter{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	log := rz.FromCtx(ctx)
	adapter.Startup(log, []string{"admin"})

	testCreateUser(ctx, adapter, t)
	testListUsers(ctx, adapter, t)
	testRemoveUser(ctx, adapter, t)

	_, err := adapter.CreateGroup(&ctx, nil)
	if err == nil {
		t.Errorf("Create group should not be supported")
	}

	_, err = adapter.ListGroups(&ctx, nil)
	if err == nil {
		t.Errorf("List groups should not be supported")
	}

	_, err = adapter.GetGroupMembers(&ctx, nil)
	if err == nil {
		t.Errorf("Get group members should not be supported")
	}

	_, err = adapter.AddUserToGroup(&ctx, nil)
	if err == nil {
		t.Errorf("Add user to group should not be supported")
	}

	_, err = adapter.RemoveUserFromGroup(&ctx, nil)
	if err == nil {
		t.Errorf("Remove user from group should not be supported")
	}

}

func testTokenRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {

	resp := `{
    "data": {
        "id": 2,
        "token": "blahblahblah",
        "creationDate": "2020-12-15T19:43:25.000+0000",
        "terminalDate": "2020-12-16T19:43:25.000+0000",
        "remainingUsages": -1,
        "type": "UnifiedLoginToken",
        "description": null,
        "username": "admin",
        "_href": "https://fortify.dev.code_dev_url.com/api/v1/tokens/2"
    },
    "responseCode": 201
}`
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(resp))

}

func testCreateUser(ctx context.Context, adapter FortifyAdapter,  t *testing.T) {
	u, err := adapter.CreateUser(&ctx, user1)
	if err != nil {
		t.Errorf("error in creating user %v", err)
	} else {
		validateUserResponse(u, user1, t)
		//if !reflect.DeepEqual(createUserRequest, createUser1) {
		//	t.Errorf("error in creating user request %v", createUserRequest)
		//}
	}
}

func validateUserResponse(u *pb.User, orig *pb.User, t *testing.T) {
	if u == nil {
		t.Errorf("error in user %v", u)
	}
	if u.GetUsername() != orig.GetUsername(){
		t.Errorf("error in user %v", u)
	}
	if u.GetEmail()!= orig.GetEmail(){
		t.Errorf("error in user %v", u)
	}
	if u.GetUsername() != orig.GetUsername(){
		t.Errorf("error in user %v", u)
	}
}

func testCreateUserRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	if !isValidToken(r) {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = w.Write([]byte(unauthorizedResponse))
	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			t.Errorf("error in user request %v", err)
		}
		var createUserRequest fortifyUserRequest
		err = json.Unmarshal(body, &createUserRequest)
		if err != nil {
			t.Errorf("error in unmarshalling user request %v", err)
		}

		if !reflect.DeepEqual(createUserRequest, createUser1) {
			t.Errorf("error in creating user request %v", createUserRequest)
		}

		resp := `{
    "data": {
        "id": 2,
        "userName": "test1",
        "firstName": "test1FirstName",
        "lastName": "test1LastName",
        "email": "test1@test.com",
        "requirePasswordChange": false,
        "passwordNeverExpire": true,
        "suspended": false,
        "failedLoginAttempts": 0,
        "dateFrozen": null,
        "roles": [
            {
                "id": "viewonly",
                "name": "View-Only",
                "description": "Users in the View-Only role can view general information and issues for application versions to which they have access. View-Only users cannot upload analysis results or audit issues.",
                "builtIn": true,
                "userOnly": false,
                "allApplicationRole": false,
                "deletable": false,
                "assignedToNonUsers": false,
                "publishVersion": 2,
                "objectVersion": 1,
                "permissionIds": [],
                "default": false
            }
        ],
        "clearPassword": null,
        "adminPassword": null,
        "_href": "https://fortify.dev.code_dev_url.com/api/v1/localUsers/4"
    },
    "responseCode": 201
}`

		w.WriteHeader(http.StatusCreated)
		_, _ = w.Write([]byte(resp))
	}
}

func testRemoveUser(ctx context.Context, adapter FortifyAdapter,  t *testing.T) {
	g, err := adapter.RemoveUser(&ctx, user1)
	if err != nil {
		t.Errorf("error in removing user %v", err)
	} else {
		validateGenericResponse(g, user1, t)
	}

}

func testRemoveUserRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "DELETE", t)
	if !isValidToken(r) {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = w.Write([]byte(unauthorizedResponse))
	} else {
		resp := ""

		b, _ := json.Marshal(resp)
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(b)
	}
}

func testListUsers(ctx context.Context, adapter FortifyAdapter,  t *testing.T) {
	us, err := adapter.ListUsers(&ctx, generic)
	if err != nil {
		t.Errorf("error in listing users %v", err)
	} else {
		if len(us.GetUsers()) != 2 {
			t.Errorf("error in number users %v", us.GetUsers())
		}

		for _, u := range us.GetUsers() {
			if u.GetUsername() == user1.GetUsername() {
				validateUserResponse(u, user1, t)
			} else {
				validateUserResponse(u, user2, t)
			}
		}
	}

}

func testListUsersRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	if !isValidToken(r) {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = w.Write([]byte(unauthorizedResponse))
	} else {
		resp := `{
    "data": [
        {
            "id": 1,
            "userName": "admin",
            "firstName": "default",
            "lastName": "user",
            "email": "my_email@fortify.com",
            "requirePasswordChange": false,
            "passwordNeverExpire": false,
            "suspended": false,
            "failedLoginAttempts": 0,
            "dateFrozen": null,
            "roles": [
                {
                    "id": "admin",
                    "name": "Administrator",
                    "description": "Users in the Administrator role have permission to perform all actions in the system, and automatically have access to all application versions. This role is to be used for system administrators.",
                    "builtIn": true,
                    "userOnly": false,
                    "allApplicationRole": true,
                    "deletable": false,
                    "assignedToNonUsers": false,
                    "publishVersion": 4,
                    "objectVersion": 1,
                    "permissionIds": [],
                    "default": false
                }
            ],
            "clearPassword": null,
            "adminPassword": null,
            "_href": "https://fortify.dev.code_dev_url.com/api/v1/localUsers/1"
        },
        {
            "id": 2,
            "userName": "test1",
            "firstName": "test1",
            "lastName": "test1",
            "email": "test1@test.com",
            "requirePasswordChange": false,
            "passwordNeverExpire": true,
            "suspended": false,
            "failedLoginAttempts": 0,
            "dateFrozen": null,
            "roles": [
                {
                    "id": "viewonly",
                    "name": "View-Only",
                    "description": "Users in the View-Only role can view general information and issues for application versions to which they have access. View-Only users cannot upload analysis results or audit issues.",
                    "builtIn": true,
                    "userOnly": false,
                    "allApplicationRole": false,
                    "deletable": false,
                    "assignedToNonUsers": false,
                    "publishVersion": 2,
                    "objectVersion": 1,
                    "permissionIds": [],
                    "default": false
                }
            ],
            "clearPassword": null,
            "adminPassword": null,
            "_href": "https://fortify.dev.code_dev_url.com/api/v1/localUsers/3"
        },
        {
            "id": 3,
            "userName": "test2",
            "firstName": "test2",
            "lastName": "test2",
            "email": "test2@test.com",
            "requirePasswordChange": false,
            "passwordNeverExpire": true,
            "suspended": false,
            "failedLoginAttempts": 0,
            "dateFrozen": null,
            "roles": [
                {
                    "id": "viewonly",
                    "name": "View-Only",
                    "description": "Users in the View-Only role can view general information and issues for application versions to which they have access. View-Only users cannot upload analysis results or audit issues.",
                    "builtIn": true,
                    "userOnly": false,
                    "allApplicationRole": false,
                    "deletable": false,
                    "assignedToNonUsers": false,
                    "publishVersion": 2,
                    "objectVersion": 1,
                    "permissionIds": [],
                    "default": false
                }
            ],
            "clearPassword": null,
            "adminPassword": null,
            "_href": "https://fortify.dev.code_dev_url.com/api/v1/localUsers/2"
        }
    ],
    "count": 3,
    "responseCode": 200,
    "links": {
        "last": {
            "href": "https://fortify.dev.code_dev_url.com/api/v1/localUsers?start=0"
        },
        "first": {
            "href": "https://fortify.dev.code_dev_url.com/api/v1/localUsers?start=0"
        }
    }
}`

		_, _ = w.Write([]byte(resp))
	}
}



func validateGenericResponse(g *pb.Generic, u *pb.User, t *testing.T) {
	if g == nil {
		t.Errorf("error Generic should not be nil")
	} else {
		if g.Meta.GetRequester() != u.GetMeta().GetRequester() {
			t.Errorf("error requester mismatch %v", g)
		}
		if g.Meta.GetRequestId() != u.GetMeta().RequestId {
			t.Errorf("error requestId mismatch %v", g)
		}
		if g.Meta.GetStatus() != 200 {
			t.Errorf("error status mismatch %v", g)
		}
	}
}

func validateQueryParam(r *http.Request, key string, value string, t *testing.T) {
	temp := r.URL.Query().Get(key)
	if  temp != value {
		t.Errorf("error in param %v should be %v found %v", key, value, temp)
	}
}

func validateRequestMethod(r *http.Request, method string, t *testing.T) {
	if r.Method != method {
		t.Errorf("error request method should be %v got %v",method, r.Method)
	}
}

func isValidToken(r *http.Request) bool{
	tok := r.Header.Get("Authorization")
	if tok != "FortifyToken blahblahblah" {
	   return false
	}
	return true
}
