package adapter

import (
	"bytes"
	pb "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/bloom42/libs/rz-go"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

//FortifyAdapter structure
type FortifyAdapter struct{}

var (
	accessToken   string
	baseURL       string
	usersEndpoint string
	tokenEndpoint string
	apiUsername   string
	apiPwd        string
	client        = &http.Client{}
	//client = &http.Client{ //leaving in for testing purposes
	//	Timeout:   time.Second * 10,
	//	Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}},
	//}
	reservedUsersList []string //users we don't want to return to the controller's diff engine
)

type usersResponse struct {
	Data []userModel `json:"data"`
}

type userResponse struct {
	Data userModel `json:"data"`
}

type userModel struct {
	ID 			int    `json:"id"`
	Username 	string `json:"username"`
	FirstName	string `json:"firstName"`
	LastName	string `json:"lastName"`
	Email 		string `json:"email"`
}

type fortifyUserRequest struct {
	UserName              string                   `json:"userName"`
	Pwd                   string                   `json:"clearPassword"`
	Email                 string                   `json:"email"`
	FirstName             string                   `json:"firstName"`
	LastName              string                   `json:"lastName"`
	PasswordNeverExpire   bool                     `json:"passwordNeverExpire"`
	RequirePasswordChange bool                     `json:"requirePasswordChange"`
	Roles                 []fortifyUserRoleRequest `json:"roles"`
}

type tokenRequest struct {
	Type string `json:"type"`
}

type tokenResponse struct {
	Data tokenDataResponse `json:"data"`
	ResponseCode int `json:"responseCode"`
}

type tokenDataResponse struct {
	Token string `json:"token"`
}

type fortifyUserRoleRequest struct {
	ID string `json:"id"`
}

const (
	fortifyToken      string = "FortifyToken"
	groupNotSupported string = "Group not supported in Fortify"
)

//Startup load the environmental variables and gets the initial token
func (d *FortifyAdapter) Startup(log *rz.Logger, excludedUsers []string) {
	//accessToken = GetEnvOrExit("FORTIFY_ACCESS_TOKEN", log)
	//example: https://fortify.t-rex.code_dev_url.com/api/v1/
	baseURL = GetEnvOrExit("FORTIFY_BASE_URL", log)
	apiUsername = GetEnvOrExit("FORTIFY_API_USERNAME", log)
	apiPwd = GetEnvOrExit("FORTIFY_API_PASSWORD", log)
	usersEndpoint = baseURL + "/localUsers"
	tokenEndpoint = baseURL + "/tokens"
	reservedUsersList = excludedUsers

}

func getToken() (string, error){
	// build URL request
	newTokenRequest := tokenRequest {
		Type: "UnifiedLoginToken",
	}
	jsonBody, err := json.Marshal(newTokenRequest)
	if err != nil {
		return "", err
	}
	req, err := http.NewRequest("POST", tokenEndpoint, bytes.NewBuffer(jsonBody))
	if err != nil {
		return "", err
	}
	req.SetBasicAuth(apiUsername, apiPwd)
	req.Header.Set("Content-type", "application/json")

	// Send req using http client
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	// Fortify API user creation returns 201 if successful
	if resp.StatusCode != 201 {
		return "", errors.New(resp.Status)
	}

	// Read response and un-marshal data
	var tokenResponse tokenResponse
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	err = json.Unmarshal(body, &tokenResponse)
	if err != nil {
		return "", err
	}
	return tokenResponse.Data.Token, nil
}

// Return it's key, otherwise it will return -1 and a bool of false.
func findUser(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

func createNewRequest(method string, endPoint string, body []byte) (*http.Request, error){
	var bodyBuffer *bytes.Buffer
	var req *http.Request
	var err error
	if body != nil {
		bodyBuffer = bytes.NewBuffer(body)

		req, err = http.NewRequest(method, endPoint, bodyBuffer)
		if err != nil {
			return nil, err
		}
	} else {
		req, err = http.NewRequest(method, endPoint, nil)
		if err != nil {
			return nil, err
		}
	}

	req.Header.Set("Authorization", fortifyToken+ " " + accessToken)
	req.Header.Set("Content-type", "application/json")
	return req, nil
}

//CreateUser creates the given user
func (d *FortifyAdapter) CreateUser(ctx *context.Context, request *pb.User) (*pb.User, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Creating user: " + request.GetUsername())

	// build URL request
	newUserRequest := fortifyUserRequest{
		UserName:              request.Username,
		Pwd:                   "aaoy8734j5kl5a9n2v55972nalj542ngsl823",
		Email:                 request.Email,
		FirstName:             request.FirstName,
		LastName:              request.LastName,
		PasswordNeverExpire:   true,
		RequirePasswordChange: false,
		Roles:                 []fortifyUserRoleRequest{{ID: "viewonly"}},
	}
	jsonBody, err := json.Marshal(newUserRequest)
	if err != nil {
		return nil, err
	}
	req, err := createNewRequest("POST", usersEndpoint, jsonBody)
	if err != nil {
		return nil, err
	}
	// Send req using http client
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 401 {
		newToken, err := getToken()
		accessToken = newToken
		if err != nil {
			return nil, err
		}
		newReq, err := createNewRequest("POST", usersEndpoint, jsonBody)
		if err != nil {
			return nil, err
		}
		resp, err = client.Do(newReq)
		if err != nil {
			return nil, err
		}
	}

	// Fortify API user creation returns 201 if successful
	if resp.StatusCode != 201 {
		return nil, errors.New(resp.Status)
	}

	// Read response and un-marshal data
	var userModel userResponse
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &userModel)
	if err != nil {
		return nil, err
	}

	// Return User
	return &pb.User{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Id:        strconv.Itoa(userModel.Data.ID),
		Username:  userModel.Data.Username,
		Email:     userModel.Data.Email,
		FirstName: userModel.Data.FirstName,
		LastName:  userModel.Data.LastName,
	}, nil
}

//RemoveUser removes the given user
func (d *FortifyAdapter) RemoveUser(ctx *context.Context, request *pb.User) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Removing user: " + request.Username)

	// set user ID of user to be removed
	uid := request.Id

	// Formulate the base URL
	base, err := url.Parse(usersEndpoint)
	if err != nil {
		return nil, err
	}

	// Attach the user ID to the base URL
	base.Path += "/" + uid
	urlPath := base.String()

	// Send req using http client
	req, err := createNewRequest("DELETE", urlPath, nil)
	if err != nil {
		return nil, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 401 {
		newToken, err := getToken()
		accessToken = newToken
		if err != nil {
			return nil, err
		}
		newReq, err := createNewRequest("DELETE", urlPath, nil)
		if err != nil {
			return nil, err
		}
		resp, err = client.Do(newReq)
		if err != nil {
			return nil, err
		}
	}

	// Fortify API returns 204 for successful removal of user
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Generic response to controller
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil

}

//ListUsers lists the users in fortify
func (d *FortifyAdapter) ListUsers(ctx *context.Context, request *pb.Generic) (*pb.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting all users...")

	// Build Get Users API
	req, err := http.NewRequest("GET", usersEndpoint+"?limit=0", nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", fortifyToken+ " " + accessToken)

	// Set additional paramaters to return 100 results per request (max in Fortify)
	params := req.URL.Query()
	req.URL.RawQuery = params.Encode()

	// Send req using http client
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Close out connection
	defer resp.Body.Close()

	if resp.StatusCode == 401 {
		newToken, err := getToken()
		accessToken = newToken
		if err != nil {
			return nil, err
		}
		req.Header.Set("Authorization", fortifyToken+ " " + accessToken)
		resp, err = client.Do(req)
		if err != nil {
			return nil, err
		}
	}

	// Fortify API get users returns 200 if successful
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	// Read and parse API request
	var userModels usersResponse
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &userModels)
	if err != nil {
		return nil, err
	}

	// Update User list that will be sent to the controller
	users := make([]*pb.User, 0)
	for _, userModel := range userModels.Data {
		_, reservedUser := findUser(reservedUsersList, userModel.Username)
		if reservedUser {
			continue
		}
		user := pb.User{Id: strconv.Itoa(userModel.ID), Username: userModel.Username, Email: userModel.Email}
		users = append(users, &user)
	}

	log.Info(fmt.Sprintf("Received %d users", len(users)))
	return &pb.Users{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Users: users,
	}, nil
}

//CreateGroup is not supported in fortify
func (d *FortifyAdapter) CreateGroup(ctx *context.Context, request *pb.Group) (*pb.Group, error) {
	log := rz.FromCtx(*ctx)
	log.Info("CreateGroup Method Not Supported.")
	return nil, errors.New(groupNotSupported)
}

//ListGroups is not supported in fortify
func (d *FortifyAdapter) ListGroups(ctx *context.Context, request *pb.Generic) (*pb.Groups, error) {
	log := rz.FromCtx(*ctx)
	log.Info("ListGroups Method Not Supported.")
	return nil, errors.New(groupNotSupported)
}

//GetGroupMembers is not supported in fortify
func (d *FortifyAdapter) GetGroupMembers(ctx *context.Context, request *pb.Group) (*pb.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("GetGroupMembers Method Not Supported.")
	return nil, errors.New(groupNotSupported)
}

//AddUserToGroup is not supported in fortify
func (d *FortifyAdapter) AddUserToGroup(ctx *context.Context, request *pb.GroupMember) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("AddUserToGroup Method Not Supported.")
	return nil, errors.New(groupNotSupported)
}

//RemoveUserFromGroup is not supported in fortify
func (d *FortifyAdapter) RemoveUserFromGroup(ctx *context.Context, request *pb.GroupMember) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("RemoveUserFromGroup Method Not Supported.")
	return nil, errors.New(groupNotSupported)
}

//GetEnvOrExit gets the enviromental variables
func GetEnvOrExit(envKey string, logger *rz.Logger) string {
	// Used for Env variables (this should be a 'common' thing amongst all adapters)
	value, ok := os.LookupEnv(envKey)
	if !ok {
		logger.Fatal(fmt.Sprintf("Environment variable: %s is required but missing", envKey))
	}
	return value
}

