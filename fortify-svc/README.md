
Fortify SVC
-------------
Fortify service adapter for group authorization distribution.

## Dependencies

This project depends on the [svc-base](https://gitlab.com/_pratheesh/gad/svc-base) project as well as other modules specified in [go.mod](go.mod).

## Use

This is intended to be used as part of the [group-sync-controller](https://gitlab.com/_pratheesh/gad/group-sync-controller) project.

## Behavior

|Method|Supported|Notes|
|------|---------|-----|
|CreateUser|Yes|A user is created at a "viewonly" role|
|RemoveUser|Yes||
|ListUser|Yes||
|CreateGroup|No||
|ListGroup|No||
|AddUserToGruop|No||
|RemoveUserFromGroup|No||
|GetGroupMembers|No||

## Development
1) Since this project has a private dependency in order to be able to pull down the svc-base module, you will need to create ~/.netrc to include your token:
```aidl
machine domain.com
  login yourusername
  password yourAccessToken
```
2) Make sure `GOPRIVATE="gitlab.com/_pratheesh/gad/svc-base"` in your `go env`

## Env Variables
```$xslt
FORTIFY_API_USERNAME=admin
FORTIFY_BASE_URL=https://fortify.t-rex.code_dev_url.com/api/v1
FORTIFY_API_PASSWORD=fortify123!@
```