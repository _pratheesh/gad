#!/usr/bin/env bash

set -e

printf "Generating docs..."

# make sure the file exists so we don't have problems with the permissions in docker
touch -a index.md && chmod -f 666 index.md

docker run -it \
  -v "$(pwd):/proto:rw" \
  "$DOCKER_IMAGE:$DOCKER_IMAGE_TAG" \
  protoc \
   -I /proto \
   --doc_out=/proto \
   --doc_opt=markdown,index.md service/*.proto

echo "...done!"
