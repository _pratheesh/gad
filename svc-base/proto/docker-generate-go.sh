#!/usr/bin/env bash

set -e

LANG="go"
export DOCKER_IMAGE="registry.code_dev_url.com/platform-one/private/abms-adce-partybus/project_name-environment-repos/msvc-ecosystem/svc-base/protobuf"
export DOCKER_IMAGE_TAG="3.12.4"

# Get current directory.
HOME_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
mkdir -p "${HOME_DIR}/gen"
chmod -f 777 "${HOME_DIR}/gen"

# Find all directories containing at least one protofile.
# Based on: https://buf.build/docs/migration-prototool#prototool-generate.
for LOCAL_DIR in $(find . -name '*.proto' -print0 | xargs -0 -n1 dirname | sort | uniq); do
  PROTO_FILES=$(find "${LOCAL_DIR}" -name '*.proto')
  # add the docker volume's absolute path '/proto' to the relative path
  DOCKER_PROTO_FILES=${PROTO_FILES//.\///proto/}
  printf "Generating '%s' code from proto files: \n%s\n" "$LANG" "  - $PROTO_FILES"

  # make sure the folders and files exist so we don't have problems with the permissions in docker
  mkdir -p "gen/go/${LOCAL_DIR}"
  touch -a "gen/go/${LOCAL_DIR}/interface.pb.go"
  touch -a "gen/go/${LOCAL_DIR}/interface_grpc.pb.go"
  chmod -f -R 777 "gen/go"

  docker run -it \
    -v "$(pwd):/proto:rw" \
    "$DOCKER_IMAGE:$DOCKER_IMAGE_TAG" \
    protoc \
      -I /proto \
      --go_out=/proto/gen/go \
      --go-grpc_out=/proto/gen/go \
      --go_opt=paths=source_relative \
      --go-grpc_opt=paths=source_relative \
      ${DOCKER_PROTO_FILES}

done

bash docker-generate-docs.sh
