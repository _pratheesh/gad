# Protocol Documentation
<a name="top"></a>

## Table of Contents

- [service/interface.proto](#service/interface.proto)
    - [Generic](#.Generic)
    - [Group](#.Group)
    - [GroupMember](#.GroupMember)
    - [Groups](#.Groups)
    - [Meta](#.Meta)
    - [User](#.User)
    - [Users](#.Users)
  
    - [Adapter](#.Adapter)
  
- [Scalar Value Types](#scalar-value-types)



<a name="service/interface.proto"></a>
<p align="right"><a href="#top">Top</a></p>

## service/interface.proto



<a name=".Generic"></a>

### Generic



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| meta | [Meta](#Meta) |  |  |






<a name=".Group"></a>

### Group



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| meta | [Meta](#Meta) |  |  |
| id | [string](#string) |  |  |
| name | [string](#string) |  |  |
| description | [string](#string) |  |  |
| path | [string](#string) |  |  |






<a name=".GroupMember"></a>

### GroupMember



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| meta | [Meta](#Meta) |  |  |
| group | [Group](#Group) |  |  |
| user | [User](#User) |  |  |






<a name=".Groups"></a>

### Groups



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| meta | [Meta](#Meta) |  |  |
| groups | [Group](#Group) | repeated |  |






<a name=".Meta"></a>

### Meta



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| request_id | [string](#string) |  | If initiating request create random hash id |
| status | [int32](#int32) |  |  |
| requester | [string](#string) |  |  |






<a name=".User"></a>

### User



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| meta | [Meta](#Meta) |  |  |
| id | [string](#string) |  |  |
| username | [string](#string) |  | keycloak field |
| email | [string](#string) |  | keycloak field |
| first_name | [string](#string) |  |  |
| last_name | [string](#string) |  |  |






<a name=".Users"></a>

### Users



| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| meta | [Meta](#Meta) |  |  |
| users | [User](#User) | repeated |  |





 

 

 


<a name=".Adapter"></a>

### Adapter


| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| CreateUser | [.User](#User) | [.User](#User) | User |
| ListUsers | [.Generic](#Generic) | [.Users](#Users) |  |
| RemoveUser | [.User](#User) | [.Generic](#Generic) |  |
| CreateGroup | [.Group](#Group) | [.Group](#Group) | Group |
| ListGroups | [.Generic](#Generic) | [.Groups](#Groups) |  |
| GetGroupMembers | [.Group](#Group) | [.Users](#Users) | Group Members |
| AddUserToGroup | [.GroupMember](#GroupMember) | [.Generic](#Generic) |  |
| RemoveUserFromGroup | [.GroupMember](#GroupMember) | [.Generic](#Generic) |  |

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

