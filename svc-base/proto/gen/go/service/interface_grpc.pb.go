// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package service

import (
	context "context"

	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// AdapterClient is the client API for Adapter service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AdapterClient interface {
	// User
	CreateUser(ctx context.Context, in *User, opts ...grpc.CallOption) (*User, error)
	ListUsers(ctx context.Context, in *Generic, opts ...grpc.CallOption) (*Users, error)
	RemoveUser(ctx context.Context, in *User, opts ...grpc.CallOption) (*Generic, error)
	// Group
	CreateGroup(ctx context.Context, in *Group, opts ...grpc.CallOption) (*Group, error)
	ListGroups(ctx context.Context, in *Generic, opts ...grpc.CallOption) (*Groups, error)
	// Group Members
	GetGroupMembers(ctx context.Context, in *Group, opts ...grpc.CallOption) (*Users, error)
	AddUserToGroup(ctx context.Context, in *GroupMember, opts ...grpc.CallOption) (*Generic, error)
	RemoveUserFromGroup(ctx context.Context, in *GroupMember, opts ...grpc.CallOption) (*Generic, error)
}

type adapterClient struct {
	cc grpc.ClientConnInterface
}

func NewAdapterClient(cc grpc.ClientConnInterface) AdapterClient {
	return &adapterClient{cc}
}

func (c *adapterClient) CreateUser(ctx context.Context, in *User, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/Adapter/CreateUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) ListUsers(ctx context.Context, in *Generic, opts ...grpc.CallOption) (*Users, error) {
	out := new(Users)
	err := c.cc.Invoke(ctx, "/Adapter/ListUsers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) RemoveUser(ctx context.Context, in *User, opts ...grpc.CallOption) (*Generic, error) {
	out := new(Generic)
	err := c.cc.Invoke(ctx, "/Adapter/RemoveUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) CreateGroup(ctx context.Context, in *Group, opts ...grpc.CallOption) (*Group, error) {
	out := new(Group)
	err := c.cc.Invoke(ctx, "/Adapter/CreateGroup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) ListGroups(ctx context.Context, in *Generic, opts ...grpc.CallOption) (*Groups, error) {
	out := new(Groups)
	err := c.cc.Invoke(ctx, "/Adapter/ListGroups", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) GetGroupMembers(ctx context.Context, in *Group, opts ...grpc.CallOption) (*Users, error) {
	out := new(Users)
	err := c.cc.Invoke(ctx, "/Adapter/GetGroupMembers", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) AddUserToGroup(ctx context.Context, in *GroupMember, opts ...grpc.CallOption) (*Generic, error) {
	out := new(Generic)
	err := c.cc.Invoke(ctx, "/Adapter/AddUserToGroup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *adapterClient) RemoveUserFromGroup(ctx context.Context, in *GroupMember, opts ...grpc.CallOption) (*Generic, error) {
	out := new(Generic)
	err := c.cc.Invoke(ctx, "/Adapter/RemoveUserFromGroup", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AdapterServer is the server API for Adapter service.
// All implementations must embed UnimplementedAdapterServer
// for forward compatibility
type AdapterServer interface {
	// User
	CreateUser(context.Context, *User) (*User, error)
	ListUsers(context.Context, *Generic) (*Users, error)
	RemoveUser(context.Context, *User) (*Generic, error)
	// Group
	CreateGroup(context.Context, *Group) (*Group, error)
	ListGroups(context.Context, *Generic) (*Groups, error)
	// Group Members
	GetGroupMembers(context.Context, *Group) (*Users, error)
	AddUserToGroup(context.Context, *GroupMember) (*Generic, error)
	RemoveUserFromGroup(context.Context, *GroupMember) (*Generic, error)
	mustEmbedUnimplementedAdapterServer()
}

type UnimplementedAdapterServer struct {
	// UnimplementedAdapterServer must be embedded to have forward compatible implementations.
}

func (*UnimplementedAdapterServer) CreateUser(context.Context, *User) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateUser not implemented")
}
func (*UnimplementedAdapterServer) ListUsers(context.Context, *Generic) (*Users, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListUsers not implemented")
}
func (*UnimplementedAdapterServer) RemoveUser(context.Context, *User) (*Generic, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveUser not implemented")
}
func (*UnimplementedAdapterServer) CreateGroup(context.Context, *Group) (*Group, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateGroup not implemented")
}
func (*UnimplementedAdapterServer) ListGroups(context.Context, *Generic) (*Groups, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListGroups not implemented")
}
func (*UnimplementedAdapterServer) GetGroupMembers(context.Context, *Group) (*Users, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetGroupMembers not implemented")
}
func (*UnimplementedAdapterServer) AddUserToGroup(context.Context, *GroupMember) (*Generic, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddUserToGroup not implemented")
}
func (*UnimplementedAdapterServer) RemoveUserFromGroup(context.Context, *GroupMember) (*Generic, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveUserFromGroup not implemented")
}
func (*UnimplementedAdapterServer) mustEmbedUnimplementedAdapterServer() {
	// UnimplementedAdapterServer must be embedded to have forward compatible implementations.
}

func RegisterAdapterServer(s *grpc.Server, srv AdapterServer) {
	s.RegisterService(&_Adapter_serviceDesc, srv)
}

func _Adapter_CreateUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(User)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).CreateUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/CreateUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).CreateUser(ctx, req.(*User))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_ListUsers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Generic)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).ListUsers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/ListUsers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).ListUsers(ctx, req.(*Generic))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_RemoveUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(User)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).RemoveUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/RemoveUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).RemoveUser(ctx, req.(*User))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_CreateGroup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Group)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).CreateGroup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/CreateGroup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).CreateGroup(ctx, req.(*Group))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_ListGroups_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Generic)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).ListGroups(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/ListGroups",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).ListGroups(ctx, req.(*Generic))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_GetGroupMembers_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Group)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).GetGroupMembers(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/GetGroupMembers",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).GetGroupMembers(ctx, req.(*Group))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_AddUserToGroup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GroupMember)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).AddUserToGroup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/AddUserToGroup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).AddUserToGroup(ctx, req.(*GroupMember))
	}
	return interceptor(ctx, in, info, handler)
}

func _Adapter_RemoveUserFromGroup_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GroupMember)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AdapterServer).RemoveUserFromGroup(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Adapter/RemoveUserFromGroup",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AdapterServer).RemoveUserFromGroup(ctx, req.(*GroupMember))
	}
	return interceptor(ctx, in, info, handler)
}

var _Adapter_serviceDesc = grpc.ServiceDesc{
	ServiceName: "Adapter",
	HandlerType: (*AdapterServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateUser",
			Handler:    _Adapter_CreateUser_Handler,
		},
		{
			MethodName: "ListUsers",
			Handler:    _Adapter_ListUsers_Handler,
		},
		{
			MethodName: "RemoveUser",
			Handler:    _Adapter_RemoveUser_Handler,
		},
		{
			MethodName: "CreateGroup",
			Handler:    _Adapter_CreateGroup_Handler,
		},
		{
			MethodName: "ListGroups",
			Handler:    _Adapter_ListGroups_Handler,
		},
		{
			MethodName: "GetGroupMembers",
			Handler:    _Adapter_GetGroupMembers_Handler,
		},
		{
			MethodName: "AddUserToGroup",
			Handler:    _Adapter_AddUserToGroup_Handler,
		},
		{
			MethodName: "RemoveUserFromGroup",
			Handler:    _Adapter_RemoveUserFromGroup_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "service/interface.proto",
}
