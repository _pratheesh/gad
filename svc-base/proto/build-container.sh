#!/usr/bin/env bash

set -e

if [ -z "$1" ]; then
  echo "Usage: ./build-container.sh <Docker Image Tag>"
  echo "Must provide an image tag for the new image, probably matching the version of protobuf."
  exit 1
fi

LOCAL_IMAGE_NAME="protobuf:latest"
DOCKER_REGISTRY="registry.code_dev_url.com/platform-one/private/abms-adce-partybus/project_name-environment-repos/msvc-ecosystem/svc-base/protobuf"
# DOCKER_TAG="3.12.4"
DOCKER_TAG="$1"

if ! docker inspect "$DOCKER_REGISTRY:$DOCKER_TAG" &>/dev/null; then
  docker build -t "$LOCAL_IMAGE_NAME" .

  docker tag "$LOCAL_IMAGE_NAME" "$DOCKER_REGISTRY:$DOCKER_TAG"
  docker tag "$LOCAL_IMAGE_NAME" "$DOCKER_REGISTRY:latest"

  docker push "$DOCKER_REGISTRY:$DOCKER_TAG"
  docker push "$DOCKER_REGISTRY:latest"

else
  echo "Docker Image Tag already exists: $DOCKER_TAG"
fi
