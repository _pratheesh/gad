#!/usr/bin/env bash

set -e

if [ -z "$1" ]; then
  echo "Usage: ./generate.sh [all|go|ruby|python]"
  echo "Must provide a language selection to execute"
  exit 1
fi

LANG="$1"

# Check Deps
if ! dpkg-query -l golang-goprotobuf-dev &> /dev/null; then
  sudo apt install -y golang-goprotobuf-dev
fi

if ! dpkg-query -l protobuf-compiler &> /dev/null; then
  sudo apt install -y protobuf-compiler
fi

if [[ "$LANG" =~ (go|all) ]]; then
  (cd $(mktemp -d); GO111MODULE=on go get github.com/golang/protobuf/protoc-gen-go)
  (cd $(mktemp -d); GO111MODULE=on go get google.golang.org/grpc/cmd/protoc-gen-go-grpc)
fi

if [[ "$LANG" =~ (ruby|all) ]]; then
  bundle install
fi

if [[ "$LANG" =~ (python|all) ]]; then
  python3 -m pip install grpcio
  python3 -m pip install grpcio-tools
fi

# Get current directory.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Find all directories containing at least one protofile.
# Based on: https://buf.build/docs/migration-prototool#prototool-generate.
for dir in $(find ${DIR} -name '*.proto' -print0 | xargs -0 -n1 dirname | sort | uniq); do
  files=$(find "${dir}" -name '*.proto')
  printf "Generating '%s' code from proto files: \n%s\n" "$LANG" "  - $files"

  if [[ "$LANG" =~ (go|all) ]]; then
    set +e
    protoc \
      -I ${DIR} \
      --go_out=gen/go \
      --go-grpc_out=gen/go \
      --go_opt=paths=source_relative \
      --go-grpc_opt=paths=source_relative \
      ${files}

    # Support older version of protoc
    if [ $? -ne "0" ]; then
      protoc \
        -I ${DIR} \
        --go_out=plugins=grpc,paths=source_relative:${DIR}/gen/go \
        ${files}
    fi
    set -e
  fi

  if [[ "$LANG" =~ (ruby|all) ]]; then
    grpc_tools_ruby_protoc \
      -I ${DIR} \
      --ruby_out=gen/ruby \
      --grpc_out=gen/ruby \
      ${files}
  fi

  if [[ "$LANG" =~ (python|all) ]]; then
    python3 \
      -m grpc_tools.protoc \
      -I ${DIR} \
      --python_out=gen/python \
      --grpc_python_out=gen/python \
      ${files}
  fi
done

echo "Generating docs..."
# Generate some Docs
(cd $(mktemp -d); GO111MODULE=on go get github.com/pseudomuto/protoc-gen-doc/cmd/protoc-gen-doc)

protoc --doc_out=./ --doc_opt=markdown,index.md service/*.proto

echo "Done"
