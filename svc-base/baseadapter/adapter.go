package baseadapter

import (
	"context"
	"gitlab.com/bloom42/libs/rz-go"
	as "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
)

// Adapter interface
type Adapter interface {
	Startup(log *rz.Logger, excludedUsers []string)
	CreateUser(ctx *context.Context, request *as.User) (*as.User, error)
	ListUsers(ctx *context.Context, request *as.Generic) (*as.Users, error)
	RemoveUser(ctx *context.Context, request *as.User) (*as.Generic, error)
	CreateGroup(ctx *context.Context, request *as.Group) (*as.Group, error)
	ListGroups(ctx *context.Context, request *as.Generic) (*as.Groups, error)
	GetGroupMembers(ctx *context.Context, request *as.Group) (*as.Users, error)
	AddUserToGroup(ctx *context.Context, request *as.GroupMember) (*as.Generic, error)
	RemoveUserFromGroup(ctx *context.Context, request *as.GroupMember) (*as.Generic, error)
}
