package baseadapter

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"gitlab.com/bloom42/libs/rz-go"
	"gitlab.com/bloom42/libs/rz-go/log"
	adaptersvc "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"runtime"
	"strings"
)

func randToken() string {
	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

type meta interface {
	GetMeta() *adaptersvc.Meta
}

func processRequestMeta(meta *adaptersvc.Meta) string {
	var requestID string

	if meta.GetRequestId() == "" {
		requestID = randToken()
		// Overwrite the incoming request's RequestID if it's empty
		meta.RequestId = requestID
	} else {
		requestID = meta.GetRequestId()
	}

	return requestID
}

func logRequest(methodName string, request meta) error {
	// remove the "meta" key from log as those fields are added individually
	requestJSON, err := getJSONBytes(request, "meta")
	if err != nil {
		return err
	}

	log.Debug(
		"Message Received",
		rz.String("method", methodName),
		rz.String("requestID", request.GetMeta().GetRequestId()),
		rz.String("requester", request.GetMeta().GetRequester()),
		rz.RawJSON("messageContent", requestJSON),
	)

	return nil
}

func logResponse(methodName string, response meta) error {
	// remove the "meta" key from the log entry as those fields are added individually
	responseJSON, err := getJSONBytes(response, "meta")
	if err != nil {
		return err
	}

	log.Debug(
		"Message Sent",
		rz.String("method", methodName),
		rz.String("requestID", response.GetMeta().GetRequestId()),
		rz.String("requester", response.GetMeta().GetRequester()),
		rz.RawJSON("messageContent", responseJSON),
	)

	return nil
}

func getJSONBytes(obj interface{}, ignoreFields ...string) ([]byte, error) {
	toJSON, err := json.Marshal(obj)
	if err != nil {
		return nil, err
	}

	if len(ignoreFields) == 0 {
		return toJSON, nil
	}

	toMap := map[string]interface{}{}
	err = json.Unmarshal([]byte(string(toJSON)), &toMap)
	if err != nil {
		return nil, err
	}

	for _, field := range ignoreFields {
		delete(toMap, field)
	}

	toJSON, err = json.Marshal(toMap)
	if err != nil {
		return nil, err
	}
	return toJSON, nil
}

// Get the current function/method name
func methodName() string {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	nameSlices := strings.Split(f.Name(), ".")
	return nameSlices[len(nameSlices)-1]
}

// Add the per-method fields to a new logger and add that to the context
func methodLogger(ctx *context.Context, request *adaptersvc.Meta, methodName string) *rz.Logger {
	var l rz.Logger

	if methodName != "" {
		l = log.With(rz.Fields(
			rz.String("method", methodName)))
	}

	if request.GetRequestId() != "" {
		l = l.With(rz.Fields(
			rz.String("requestID", request.GetRequestId())))
	}

	if request.GetRequester() != "" {
		l = l.With(rz.Fields(
			rz.String("requester", request.GetRequester())))
	}

	*ctx = l.ToCtx(*ctx)

	return &l
}
