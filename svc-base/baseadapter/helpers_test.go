package baseadapter

import (
	adaptersvc "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"testing"
)

func TestProcessRequestMeta(t *testing.T) {
	// Starting with an empty meta with no RequestId
	emptyMeta := adaptersvc.Meta{}
	requestID := processRequestMeta(&emptyMeta)

	// Then the RequestId should get populated
	if requestID == "" {
		t.Error("processRequestMeta() failed, RequestId was blank")
	} else if emptyMeta.GetRequestId() == "" {
		t.Error("processRequestMeta() failed, emptyMeta.RequestId was blank")
	}

	// Starting with a populated meta.RequestId
	id := "123"
	meta := adaptersvc.Meta{RequestId: id}
	requestID = processRequestMeta(&meta)

	// Then the RequestId should stay the same
	if requestID == "" {
		t.Error("processRequestMeta() failed, pre-populated RequestId was blank")
	} else if requestID != id {
		t.Error("processRequestMeta() failed, RequestId didn't match the input")
	} else if meta.GetRequestId() != id {
		t.Error("processRequestMeta() failed, meta.RequestId didn't match the input")
	}
}

func TestGetJsonBytes(t *testing.T) {
	generic := adaptersvc.Generic{Meta: &adaptersvc.Meta{RequestId: "123"}}

	// Should return with the meta field omitted
	json, _ := getJSONBytes(&generic, "meta")

	if string(json) != "{}" {
		t.Error("getJSONBytes() failed, return should be empty")
	}
}

func TestRandToken(t *testing.T) {
	randToken := randToken()
	randTokenExpectedLength := 16
	randTokenActualLength := len(randToken)

	if randTokenActualLength != randTokenExpectedLength {
		t.Errorf("randToken() failed, return length should be 16 but it was %d", randTokenActualLength)
	}
}

func TestMethodName(t *testing.T) {
	actualMethodName := "TestMethodName"
	detectedMethodName := methodName()

	if detectedMethodName != actualMethodName {
		t.Errorf("methodName() failed, expected return to be %s, but got %s", actualMethodName, detectedMethodName)
	}
}
