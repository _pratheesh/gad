package baseadapter

import (
	as "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"context"
	"gitlab.com/bloom42/libs/rz-go"
	"reflect"
	"testing"
	"time"
)

var user1 = &as.User{
	Meta: &as.Meta{
		RequestId: "1",
		Status:    200,
		Requester: "requester",
	},
	Id:        "2",
	Username:  "test1",
	Email:     "test1@test.com",
	FirstName: "test1FirstName",
	LastName:  "test1LastName",
}

var user2 = &as.User{
	Meta: &as.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
	Id:        "3",
	Username:  "test2",
	Email:     "test2@test.com",
	FirstName: "test2FirstName",
	LastName:  "test2LastName",
}

var generic = &as.Generic{
	Meta: &as.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
}

var users = &as.Users{
	Meta: &as.Meta{
		RequestId: "3",
		Status:    200,
		Requester: "requester",
	},
	Users: []*as.User{user1, user2},
}

var group1 = &as.Group{
	Meta: &as.Meta{
		RequestId: "4",
		Status:    200,
		Requester: "requester",
	},
	Id:          "6",
	Name:        "Tron",
	Description: "",
	Path:        "/Platform One/Tron",
}

var group2 = &as.Group{
	Meta: &as.Meta{
		RequestId: "5",
		Status:    200,
		Requester: "requester",
	},
	Id:          "6",
	Name:        "project_name",
	Description: "",
	Path:        "/Platform One/project_name",
}

var groups = &as.Groups{
	Meta: &as.Meta{
		RequestId: "7",
		Status:    200,
		Requester: "requester",
	},
	Groups: []*as.Group{group1, group2},
}

var groupMember = &as.GroupMember{
	Meta: &as.Meta{
		RequestId: "8",
		Status:    200,
		Requester: "requester",
	},
	Group: &as.Group{
		Meta: &as.Meta{
			RequestId: "8",
			Status:    200,
			Requester: "requester",
		},
		Id:          "6",
		Name:        "Tron",
		Description: "",
		Path:        "/Platform One/Tron",
	},
	User:  &as.User{
		Meta: &as.Meta{
			RequestId: "8",
			Status:    200,
			Requester: "requester",
		},
		Id:        "10",
		Username:  "test10",
		Email:     "test10@gmail.com",
		FirstName: "test10",
		LastName:  "test10",
	},
}

type MockAdapter struct{}

func (d *MockAdapter) Startup(log *rz.Logger, excludedUsers []string) {

}
func (d *MockAdapter)CreateUser(ctx *context.Context, request *as.User) (*as.User, error) {
	return user1, nil
}
func (d *MockAdapter)ListUsers(ctx *context.Context, request *as.Generic) (*as.Users, error) {
	return users, nil
}
func (d *MockAdapter)RemoveUser(ctx *context.Context, request *as.User) (*as.Generic, error) {
	return generic, nil
}
func (d *MockAdapter)CreateGroup(ctx *context.Context, request *as.Group) (*as.Group, error) {
	return group1, nil
}
func (d *MockAdapter)ListGroups(ctx *context.Context, request *as.Generic) (*as.Groups, error) {
	return groups, nil
}
func (d *MockAdapter)GetGroupMembers(ctx *context.Context, request *as.Group) (*as.Users, error) {
	return users, nil
}
func (d *MockAdapter)AddUserToGroup(ctx *context.Context, request *as.GroupMember) (*as.Generic, error) {
	return generic, nil
}
func (d *MockAdapter)RemoveUserFromGroup(ctx *context.Context, request *as.GroupMember) (*as.Generic, error) {
	return generic, nil
}

func TestServer(t *testing.T) {
	var mockAdapter = MockAdapter{}
	adapterLogic = &mockAdapter

	s := &server{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	testCreateUser(ctx, s, t)
	testListUsers(ctx, s, t)
	testRemoveUser(ctx, s, t)
	testCreateGroup(ctx, s, t)
	testListGroups(ctx, s, t)
	testGetGroupMembers(ctx, s, t)
	testAddUserToGroup(ctx, s, t)
	testRemoveUserFromGroup(ctx, s, t)
}

func testCreateUser(ctx context.Context, s *server, t *testing.T) {
	u,err := s.CreateUser(ctx, user1)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(u, user1) {
		t.Errorf("Error in create user, response was not as expected %v", u)
	}
}

func testListUsers(ctx context.Context, s *server, t *testing.T) {
	u,err := s.ListUsers(ctx, generic)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(u, users) {
		t.Errorf("Error in llisting users, response was not as expected %v", u)
	}
}

func testRemoveUser(ctx context.Context, s *server, t *testing.T) {
	g,err := s.RemoveUser(ctx, user1)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(g, generic) {
		t.Errorf("Error in llisting users, response was not as expected %v", g)
	}
}

func testCreateGroup(ctx context.Context, s *server, t *testing.T) {
	g,err := s.CreateGroup(ctx, group1)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(g, group1) {
		t.Errorf("Error in creating group, response was not as expected %v", g)
	}
}

func testListGroups(ctx context.Context, s *server, t *testing.T) {
	g,err := s.ListGroups(ctx, generic)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(g, groups) {
		t.Errorf("Error in creating group, response was not as expected %v", g)
	}
}

func testGetGroupMembers(ctx context.Context, s *server, t *testing.T) {
	u,err := s.GetGroupMembers(ctx, group1)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(u, users) {
		t.Errorf("Error in creating group, response was not as expected %v", u)
	}
}

func testAddUserToGroup(ctx context.Context, s *server, t *testing.T) {
	g,err := s.AddUserToGroup(ctx, groupMember)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(g, generic) {
		t.Errorf("Error in creating group, response was not as expected %v", g)
	}
}

func testRemoveUserFromGroup(ctx context.Context, s *server, t *testing.T) {
	g,err := s.RemoveUserFromGroup(ctx, groupMember)
	if err != nil {
		t.Errorf("Error in create user %v", err)
	}
	if !reflect.DeepEqual(g, generic) {
		t.Errorf("Error in creating group, response was not as expected %v", g)
	}
}
