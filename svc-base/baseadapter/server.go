package baseadapter

import (
	"context"

	adaptersvc "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"gitlab.com/bloom42/libs/rz-go"
)

var adapterLogic Adapter

type server struct {
	adaptersvc.UnimplementedAdapterServer
}

func executeServerMethod(ctx *context.Context, request meta, adapterLogicMethod func() (meta, error), methodName string) (meta, error) {
	processRequestMeta(request.GetMeta())
	mLog := methodLogger(ctx, request.GetMeta(), methodName)

	err := logRequest(methodName, request)
	if err != nil {
		mLog.Error("Log request failed", rz.Err(err))
	}

	// Execute the adapter logic
	response, respErr := adapterLogicMethod()
	if respErr != nil {
		mLog.Error("Adapter logic failed", rz.Err(respErr))
		return nil, respErr
	}

	err = logResponse(methodName, response)
	if err != nil {
		mLog.Error("Log response failed", rz.Err(err))
	}

	return response, nil
}

func (s *server) CreateUser(ctx context.Context, request *adaptersvc.User) (*adaptersvc.User, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.CreateUser(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.User), nil
}

func (s *server) ListUsers(ctx context.Context, request *adaptersvc.Generic) (*adaptersvc.Users, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.ListUsers(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Users), nil
}

func (s *server) RemoveUser(ctx context.Context, request *adaptersvc.User) (*adaptersvc.Generic, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.RemoveUser(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Generic), nil
}

func (s *server) CreateGroup(ctx context.Context, request *adaptersvc.Group) (*adaptersvc.Group, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.CreateGroup(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Group), nil
}

func (s *server) ListGroups(ctx context.Context, request *adaptersvc.Generic) (*adaptersvc.Groups, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.ListGroups(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Groups), nil
}

func (s *server) GetGroupMembers(ctx context.Context, request *adaptersvc.Group) (*adaptersvc.Users, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.GetGroupMembers(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Users), nil
}

func (s *server) AddUserToGroup(ctx context.Context, request *adaptersvc.GroupMember) (*adaptersvc.Generic, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.AddUserToGroup(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Generic), nil
}

func (s *server) RemoveUserFromGroup(ctx context.Context, request *adaptersvc.GroupMember) (*adaptersvc.Generic, error) {
	adapterLogicMethod := func() (meta, error) {
		return adapterLogic.RemoveUserFromGroup(&ctx, request)
	}

	response, err := executeServerMethod(&ctx, request, adapterLogicMethod, methodName())
	if err != nil {
		return nil, err
	}

	return response.(*adaptersvc.Generic), nil
}
