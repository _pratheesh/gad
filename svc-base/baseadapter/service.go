package baseadapter

import (
	"flag"
	"fmt"
	"net"
	"os"
	"strings"
	"time"

	adaptersvc "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"

	"gitlab.com/bloom42/libs/rz-go"
	"gitlab.com/bloom42/libs/rz-go/log"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
)

var (
	tls           = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile      = flag.String("cert_file", "", "The gRPC server TLS cert file")
	keyFile       = flag.String("key_file", "", "The gRPC server TLS key file")
	grpcIP        = flag.String("grpc_ip", "", "The gRPC listener IP address")
	grpcPort      = flag.Int("grpc_port", 50051, "The gRPC listener server port")
	cmdLogLevel   = flag.String("log_level", "", "The application log level: info, debug, warn, error")
	consoleLog    = flag.Bool("console_log", false, "Switch to the console logging format")
	excludedUsers = flag.String("excluded_users", "", "The list of users that are excluded from all responses")
)

func startup() {
	flag.Parse()

	// Get the environment variables
	var hostname = os.Getenv("HOSTNAME")
	var podName = os.Getenv("POD_NAME")
	var serviceName = os.Getenv("SERVICE_NAME")
	var logLevel = os.Getenv("LOG_LEVEL")

	// Set the desired log Level
	if *cmdLogLevel != "" {
		// Prefer command-line arg over ENV var
		logLevel = *cmdLogLevel
	} else if logLevel == "" {
		// default level if nothing is provided
		logLevel = "info"
	}

	// Set the Env fields for logging
	if hostname != "" {
		log.SetLogger(log.With(rz.Fields(
			rz.String("hostname", hostname))))
	}
	if podName != "" {
		log.SetLogger(log.With(rz.Fields(
			rz.String("podName", podName))))
	}
	if serviceName != "" {
		log.SetLogger(log.With(rz.Fields(
			rz.String("serviceName", serviceName))))
	}

	// Set the logger's time format and time-zone to UTC
	log.SetLogger(log.With(
		rz.TimeFieldFormat("2006-01-02 15:04:05.000"),
		rz.TimestampFunc(func() time.Time {
			return time.Now().UTC()
		}),
	))

	if *consoleLog {
		log.SetLogger(log.With(
			rz.Formatter(rz.FormatterConsole())))
	}

	// Set the logger's log level
	if logLevel == "info" {
		log.SetLogger(log.With(rz.Level(rz.InfoLevel)))
	} else if logLevel == "debug" {
		log.SetLogger(log.With(rz.Level(rz.DebugLevel)))
	} else if logLevel == "warn" {
		log.SetLogger(log.With(rz.Level(rz.WarnLevel)))
	} else if logLevel == "error" {
		log.SetLogger(log.With(rz.Level(rz.ErrorLevel)))
	}

	log.Log(fmt.Sprintf("Log level set to %s", logLevel))
}

// Start service
func Start(appAdapter Adapter) {
	fmt.Println("yes gere")
	startup()

	// Parsing excluded users list
	excludedUsersList := strings.Split(*excludedUsers, ",")
	for i := range excludedUsersList {
		excludedUsersList[i] = strings.TrimSpace(excludedUsersList[i])
	}

	// Execute the adapter logic startup
	startupLogger := log.With(rz.Fields(rz.String("method", "Startup")))
	//adapterLogic = new(appAdapter)
	appAdapter.Startup(&startupLogger, excludedUsersList)
	adapterLogic = appAdapter

	// Start the network server
	grpcAddress := fmt.Sprintf("%s:%d", *grpcIP, *grpcPort)
	lis, err := net.Listen("tcp", grpcAddress)
	if err != nil {
		log.Fatal("Error starting listener", rz.Err(err))
	}
	defer lis.Close()

	log.Log(fmt.Sprintf("gRPC server is listening on %v", grpcAddress))

	// gRPC TLS server options
	var grpcServerOptions []grpc.ServerOption
	if *tls {
		if *certFile != "" && *keyFile != "" {
			creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
			if err != nil {
				log.Fatal("Failed to generate credentials", rz.Err(err))
			}
			grpcServerOptions = []grpc.ServerOption{grpc.Creds(creds)}
		}
	}

	// Combine all gRPC server options
	grpcServerOptions = append(grpcServerOptions,
		// Server-side keep-alive
		grpc.KeepaliveParams(keepalive.ServerParameters{
			MaxConnectionIdle: 5 * time.Minute,
		}),
	)

	// Create the gRPC server
	grpcServer := grpc.NewServer(grpcServerOptions...)

	// Register the gRPC service implementation
	adaptersvc.RegisterAdapterServer(grpcServer, &server{})

	// Start the gRPC server
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatal("Error starting gRPC server", rz.Err(err))
	}
}
