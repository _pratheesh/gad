## SVC-Base

This Golang microservice skeleton is designed to make developing services in GOLANG simple and standard, allowing other
P1 developers to easily contribute to the effort.

The structure only requires modification to two files, all the other plumbing to the gRPC server is in place.

* Features provided by this skeleton should include:
  * Unit Tests
  * Standard Logging
  * [API Reference Docs](proto/index.md)
  * Example Server & Client

The `.proto` files are in [proto/service](proto/service) and the generated code is placed in [proto/gen](proto/gen)/{language}/.

[Generate the gRPC Go Code](#generate-code) from the proto file using `make build`
![Diagram of Architecture](docs/msvc-architecture-80percent.png)

### Run it

Run the example server and client.

```shell script
make run-server   # this runs the server in debug mode
```

```shell script
make run-client   # more examples/features are detailed within cmd/gRPC/client/run-client.sh
# or
make run-client CLIENT_FLAGS="-method=ListUsers" # only run the MakeUsers call
```

Optional: Watch the HTTP/2 messaging on the wire with tshark
```shell script
sudo tshark -i lo -Y http2 -O http2 -d tcp.port==50051,http2
```

### Server Logic

The methods in the adapter file are the entrypoint for each of the protobuf service's remote procedure calls.

To understand the specs for the protobuf remote procedure calls and messages check out the [API Reference Docs](proto/index.md)
and take a look at the `Example Response` structs that are built for each method in `internal/demoAdapter.go`.

The `Startup()` method will run at the beginning of `main()` in `server/main.go`. Use this method to create a
reusable client and get authenticated.

Each method has its own locally scoped logger with the `method`, `requestId`, and `requester` fields populated from the `Meta`
struct. The logged field keys names should be in `camelCase`. Check out the [server logging examples](#server-logging-examples) section.

Since the controller will try to delete any adapter app's user that it finds that doesn't exist in KeyCloak, and some adapter
applications will need to persist some standalone user accounts, there is a command line flag `-excluded_users` to
provide the list of users that are to be excluded from all responses. This feature should only need to be implemented
on RPC methods that return the `Users` message type. Grab the `excludedUsers` list in `Startup()` and store it somewhere
for use later.

### Logging with rz-go

A logging framework with the [rz-go](https://gitlab.com/bloom42/libs/rz-go) library is setup with a few
examples in [cmd/gRPC/server/main.go](cmd/gRPC/server/main.go).

The following ENV vars map to fields that are logged in every message:

| ENV Var      | Log Field   |
| ------------ | ----------- |
| LOG_LEVEL    | logLevel    |
| HOSTNAME     | hostname    |
| POD_NAME     | podName     |
| SERVICE_NAME | serviceName |

#### Server Logging Examples

Info Log Level:
```go
log.Info("Log Message Here!",
    rz.String("someName", "someValue"),   // Add a string Name/Value pair
    rz.Int("otherName", 1),               // Add an int Name/Value pair
    rz.RawJSON("jsonName", someJSON),     // Add a byte slice of JSON data
)
```

Error Log Level:
```go
// Log the error but don't quit
log.Error("Log Message Here!",
    rz.String("someName", "someValue"),
    rz.Err(err),  // Add an error
)
```

Panic Log Level:
```go
// Exit gracefully with panic()
log.Panic("Log Message Here!",
    rz.String("someName", "someValue"),
    rz.Err(err),  // Add an error
)
```

Fatal Log Level:
```go
// Exit immediately with os.Exit(1)
log.Fatal("Log Message Here!",
    rz.String("someName", "someValue"),
    rz.Err(err), // Add an error
)
```

## Generate Code

Note: It is only necessary to generate new code if the `.proto` file has changed. The proto file should only be changed
through group consensus within the `svc-base` and then propagated to all the other services. This will ensure
that a consistent client library can be used to communicate with the entire ecosystem of adapters.

### Docker Generator Method (golang only, atm)

Use the (preferred) [proto/docker-generate-go.sh](proto/docker-generate-go.sh) script to generate the go code and update the docs.

* Required to run `proto/docker-generate-go.sh` for compiling the server.
  * Docker
  * Access to the `protobuf` Docker container in the registry.code_dev_url.com (GitLab) container registry
    * A GitLab `Personal Access Token` with access to the registry should be in `~/.docker/config.json`

Generate the `protobuf` Docker container with [proto/build-container.sh](proto/build-container.sh)

### Local Generator Method

Use the (legacy) [proto/generate.sh](proto/generate.sh) script to generate the code and update the docs.

`Usage: ./generate.sh [all|go|ruby|python]`

* Required to run `proto/generate.sh` for compiling the server.
  * sudo apt-get install protobuf-compiler
  * sudo apt install golang-goprotobuf-dev
  * go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
  * go get github.com/golang/protobuf/protoc-gen-go

### Using server side TLS Authentication
- Run `make cert` to generate the server and CA certs
- Run `make run-server` to start the server with TLS enabled
- Run `make run-client` to start the client with generated cert

## Developement

This project is a library that uses slightly different commands to build and test.

### Building the project

`go build ./...`

### Running unit tests

`go test ./...`

### Running coverage

```
go test ./... -coverprofile cover.out
go tool cover -func cover.out
go tool cover -func cover.out | grep total | awk '{print substr($3, 1, length($3)-1)}'
```
