package main

import (
	"context"
	"fmt"
	"gitlab.com/bloom42/libs/rz-go"
	"gitlab.com/_pratheesh/gad/svc-base/baseadapter"
	adaptersvc "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
)

// Example Response Data
var (
	group1 = adaptersvc.Group{
		Id:          "1",
		Name:        "admins",
		Description: "admin pplz",
	}

	user1 = adaptersvc.User{
		Id:        "1",
		Username:  "user1",
		FirstName: "John",
		LastName:  "Dough",
		Email:     "john@dough.com",
	}
)

// DemoAdapter storing users
type DemoAdapter struct {
	excludedUsers []string
}

// Startup Invoked in ../server/main.go at the top of main()
func (d *DemoAdapter) Startup(log *rz.Logger, excludedUsers []string) {
	log.Log("Processing adapter logic")

	if (excludedUsers[0] != "") || len(excludedUsers) > 1 {
		log.Log(fmt.Sprintf("Found %d excluded users: %s", len(excludedUsers), excludedUsers))
		d.excludedUsers = excludedUsers
	}

	// Add Startup Code Here
}

// CreateUser demo
func (d *DemoAdapter) CreateUser(ctx *context.Context, request *adaptersvc.User) (*adaptersvc.User, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	response := user1
	response.Meta = &adaptersvc.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Requester: request.GetMeta().GetRequester(),
		Status:    200,
	}

	return &response, nil
}

// ListUsers demo
func (d *DemoAdapter) ListUsers(ctx *context.Context, request *adaptersvc.Generic) (*adaptersvc.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	listUserResponse := adaptersvc.Users{
		Meta: &adaptersvc.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Requester: request.GetMeta().GetRequester(),
			Status:    200,
		},
		Users: []*adaptersvc.User{
			&user1,
			&user1,
		},
	}

	return &listUserResponse, nil
}

// RemoveUser demo
func (d *DemoAdapter) RemoveUser(ctx *context.Context, request *adaptersvc.User) (*adaptersvc.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	response := adaptersvc.Generic{
		Meta: &adaptersvc.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Requester: request.GetMeta().GetRequester(),
			Status:    200,
		},
	}

	return &response, nil
}

// CreateGroup demo
func (d *DemoAdapter) CreateGroup(ctx *context.Context, request *adaptersvc.Group) (*adaptersvc.Group, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	response := group1
	response.Meta = &adaptersvc.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Requester: request.GetMeta().GetRequester(),
		Status:    200,
	}

	return &response, nil
}

// ListGroups demo
func (d *DemoAdapter) ListGroups(ctx *context.Context, request *adaptersvc.Generic) (*adaptersvc.Groups, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	listGroupsResponse := adaptersvc.Groups{
		Meta: &adaptersvc.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Requester: request.GetMeta().GetRequester(),
			Status:    200,
		},
		Groups: []*adaptersvc.Group{
			&group1,
			&group1,
		},
	}

	return &listGroupsResponse, nil
}

// GetGroupMembers demo
func (d *DemoAdapter) GetGroupMembers(ctx *context.Context, request *adaptersvc.Group) (*adaptersvc.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	response := adaptersvc.Users{
		Meta: &adaptersvc.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Requester: request.GetMeta().GetRequester(),
			Status:    200,
		},
		Users: []*adaptersvc.User{
			&user1,
			&user1,
		},
	}

	return &response, nil
}

// AddUserToGroup demo
func (d *DemoAdapter) AddUserToGroup(ctx *context.Context, request *adaptersvc.GroupMember) (*adaptersvc.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	response := adaptersvc.Generic{
		Meta: &adaptersvc.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Requester: request.GetMeta().GetRequester(),
			Status:    200,
		},
	}

	return &response, nil
}

// RemoveUserFromGroup demo
func (d *DemoAdapter) RemoveUserFromGroup(ctx *context.Context, request *adaptersvc.GroupMember) (*adaptersvc.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Processing adapter logic")

	// Add Code Here

	// Example Response
	response := adaptersvc.Generic{
		Meta: &adaptersvc.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Requester: request.GetMeta().GetRequester(),
			Status:    200,
		},
	}

	return &response, nil
}

func main() {
	baseadapter.Start(&DemoAdapter{})
}
