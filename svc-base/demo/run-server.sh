#!/usr/bin/env bash

go run server/server.go \
  -log_level=debug \
  -console_log=1 \
  -tls=true \
  -cert_file="../cert/server-cert.pem" \
  -key_file="../cert/server-key.pem"
  # -grpc_port=8888
