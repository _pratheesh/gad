package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	adaptersvc "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"google.golang.org/grpc/credentials"

	"google.golang.org/grpc"
)

var (
	execMethod = flag.String("method", "", "The specific method to execute")
	grpcIP     = flag.String("grpc_ip", "0.0.0.0", "The gRPC listener IP address")
	grpcPort   = flag.Int("grpc_port", 50051, "The gRPC listener server port")
)

var (
	emptyMeta = adaptersvc.Meta{} // No Request-Id provided to the server

	group11 = adaptersvc.Group{
		Name:        "Devs",
		Description: "Deving it up",
		Path:        "/Path/To/Group",
	}

	user11 = adaptersvc.User{
		Username:  "john.dough",
		FirstName: "John",
		LastName:  "Dough",
		Email:     "john@dough.com",
	}

	groupMember1 = adaptersvc.GroupMember{
		Group: &group11,
		User:  &user11,
	}

	generic1 = adaptersvc.Generic{}
)

func metaWithReqID() *adaptersvc.Meta {
	// Client-side Request-Id
	return &adaptersvc.Meta{RequestId: randToken(), Requester: "Controller1"}
}

func main() {
	flag.Parse()

	grpcAddress := fmt.Sprintf("%s:%d", *grpcIP, *grpcPort)
	log.Printf("Connecting to gRPC server at %v", grpcAddress)

	tlsCredentials, err := credentials.NewClientTLSFromFile("../../cert/ca-cert.pem", "")

	if err != nil {
		log.Fatal("cannot load TLS credentials: ", err)
	}

	// Set up an in-secure connection to the server.
	conn, err := grpc.Dial(grpcAddress, grpc.WithTransportCredentials(tlsCredentials))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := adaptersvc.NewAdapterClient(conn)

	switch {
	case strings.EqualFold(*execMethod, "CreateUser"):
		CreateUser(c)
	case strings.EqualFold(*execMethod, "ListUsers"):
		ListUsers(c)
	case strings.EqualFold(*execMethod, "RemoveUser"):
		RemoveUser(c)
	case strings.EqualFold(*execMethod, "CreateGroup"):
		CreateGroup(c)
	case strings.EqualFold(*execMethod, "ListGroups"):
		ListGroups(c)
	case strings.EqualFold(*execMethod, "GetGroupMembers"):
		GetGroupMembers(c)
	case strings.EqualFold(*execMethod, "AddUserToGroup"):
		AddUserToGroup(c)
	case strings.EqualFold(*execMethod, "RemoveUserFromGroup"):
		RemoveUserFromGroup(c)
	default:
		CreateUser(c)
		ListUsers(c)
		RemoveUser(c)
		CreateGroup(c)
		ListGroups(c)
		GetGroupMembers(c)
		AddUserToGroup(c)
		RemoveUserFromGroup(c)
	}
}

// CreateUser demo
func CreateUser(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := user11
	request.Meta = metaWithReqID()

	log.Println("CreateUser Request:", "\n ", outputJSON(&request))

	createUser, err := c.CreateUser(ctx, &request)
	if err != nil {
		log.Fatalf("could not create user: %v", err)
	}

	log.Println("CreateUser Response:", "\n ", outputJSON(&createUser))
}

// ListUsers demo
func ListUsers(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := generic1
	request.Meta = metaWithReqID()

	log.Println("ListUsers Request:", "\n ", outputJSON(&request))

	listUsers, err := c.ListUsers(ctx, &request)
	if err != nil {
		log.Fatalf("could not list users: %v", err)
	}

	log.Println("ListUsers Response:", "\n ", outputJSON(listUsers))
}

// RemoveUser demo
func RemoveUser(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := user11
	request.Meta = metaWithReqID()

	log.Println("RemoveUser Request:", "\n ", outputJSON(&request))

	removeUser, err := c.RemoveUser(ctx, &request)
	if err != nil {
		log.Fatalf("could not remove user: %v", err)
	}

	log.Println("RemoveUser Response:", "\n ", outputJSON(&removeUser))
}

// CreateGroup demo
func CreateGroup(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := group11
	request.Meta = &emptyMeta

	log.Println("CreateGroup Request:", "\n ", outputJSON(&request))

	createGroup, err := c.CreateGroup(ctx, &request)
	if err != nil {
		log.Fatalf("could not create group: %v", err)
	}

	log.Println("CreateGroup Response:", "\n ", outputJSON(createGroup))
}

// ListGroups demo
func ListGroups(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := generic1
	request.Meta = metaWithReqID()

	log.Println("ListGroups Request:", "\n ", outputJSON(&request))

	listGroups, err := c.ListGroups(ctx, &request)
	if err != nil {
		log.Fatalf("could not list groups: %v", err)
	}

	log.Println("ListGroups Response:", "\n ", outputJSON(listGroups))
}

// GetGroupMembers demo
func GetGroupMembers(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := group11
	request.Meta = &emptyMeta

	log.Println("GetGroupMembers Request:", "\n ", outputJSON(&request))

	getGroupMembers, err := c.GetGroupMembers(ctx, &request)
	if err != nil {
		log.Fatalf("could not list groups: %v", err)
	}

	log.Println("GetGroupMembers Response:", "\n ", outputJSON(getGroupMembers))
}

// AddUserToGroup demo
func AddUserToGroup(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := groupMember1
	request.Meta = metaWithReqID()

	log.Println("AddUserToGroup Request:", "\n ", outputJSON(&request))

	getGroupMembers, err := c.AddUserToGroup(ctx, &request)
	if err != nil {
		log.Fatalf("could not list groups: %v", err)
	}

	log.Println("AddUserToGroup Response:", "\n ", outputJSON(getGroupMembers))
}

// RemoveUserFromGroup demo
func RemoveUserFromGroup(c adaptersvc.AdapterClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	request := groupMember1
	request.Meta = &emptyMeta

	log.Println("RemoveUserFromGroup Request:", "\n ", outputJSON(&request))

	getGroupMembers, err := c.RemoveUserFromGroup(ctx, &request)
	if err != nil {
		log.Fatalf("could not list groups: %v", err)
	}

	log.Println("RemoveUserFromGroup Response:", "\n ", outputJSON(getGroupMembers))
}

// outputJSON convert output to json
func outputJSON(resp interface{}) string {
	b, err := json.MarshalIndent(resp, "  ", "  ")
	if err != nil {
		log.Println("error:", err)
	}
	return string(b)
}

// randToken generate random token
func randToken() string {
	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
