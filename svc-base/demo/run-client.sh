#!/usr/bin/env bash

# run all of the methods against a server on localhost
go run client/client.go "$@"

# connect to a specific gRPC IP & port
# go run main.go -grpc_ip=10.0.0.1 -grpc_port=8888

# run the specified method only
# go run main.go -method=CreateUser
# go run main.go -method=ListUsers
# go run main.go -method=RemoveUser
# go run main.go -method=CreateGroup
# go run main.go -method=ListGroups
# go run main.go -method=GetGroupMembers
# go run main.go -method=AddUserToGroup
# go run main.go -method=RemoveUserFromGroup
