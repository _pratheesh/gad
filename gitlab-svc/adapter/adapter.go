package adapter

import (
	"bytes"
	pb "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/bloom42/libs/rz-go"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

//GitlabAdapter implements base adapter interface
type GitlabAdapter struct{}

var (
	gitlabAccessToken   = ""
	gitlabGroupEndpoint = ""
	gitlabUsersEndpoint = ""
	client              = &http.Client{}
	//reservedUsersList = []string{"admin", "root", "il2_api_user", "ghost"} //users we don't want to return to the controller's diff engine
	reservedUsersList []string
)

const (
	privateTokenHeader string = "PRIVATE_TOKEN"
	xPage              string = "x-page"
	xTotalPage         string = "x-total-pages"
	xNextPage          string = "x-next-page"
)


type userModel struct {
	ID 			int `json:"id"`
	Name		string `json:"name"`
	Username 	string `json:"username"`
	Email 		string `json:"email"`
}

type groupModel struct {
	GroupID  int    `json:"id"`
	FullName string `json:"full_name"`
	ParentID int    `json:"parent_id"`
	Name     string `json:"name"`
}

//Startup Invoked in ../server/main.go at the top of main()
func (d *GitlabAdapter) Startup(log *rz.Logger, excludedUsers []string) {
	gitlabAccessToken = GetEnvOrExit("GITLAB_ACCESS_TOKEN", log)
	gitlabGroupEndpoint = GetEnvOrExit("GITLAB_GROUPS_URL", log)
	gitlabUsersEndpoint = GetEnvOrExit("GITLAB_USERS_URL", log)
	reservedUsersList = excludedUsers
}

// Removes the extra 'space' in the full name path
func convertFullNameToKeycloakPath(fullName string) string {
	return "/" + strings.ReplaceAll(fullName, " / ", "/")
}

// Return it's key, otherwise it will return -1 and a bool of false.
func findUser(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

//CreateUser in gitlab
func (d *GitlabAdapter) CreateUser(ctx *context.Context, request *pb.User) (*pb.User, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Creating user: " + request.GetUsername())

	// build URL request
	req, err := http.NewRequest("POST", gitlabUsersEndpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)

	// user creation URL parameters
	params := req.URL.Query()
	params.Add("username", request.Username)
	params.Add("email", request.Email)
	params.Add("name", request.Username)
	params.Add("reset_password", "true")
	params.Add("can_create_group", "false")
	params.Add("provider", "openid_connect")
	params.Add("extern_uid", request.Username)
	params.Add("skip_confirmation", "true")
	req.URL.RawQuery = params.Encode()

	// Send req using http client
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API user creation returns 201 if successful
	if resp.StatusCode != 201 {
		return nil, errors.New(resp.Status)
	}

	// Read response and un-marshal data
	var userModel userModel
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &userModel)
	if err != nil {
		return nil, err
	}

	// Return User
	return &pb.User{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Id:        strconv.Itoa(userModel.ID),
		Username:  userModel.Username,
		Email:     userModel.Email,
		FirstName: userModel.Name,
		LastName:  userModel.Name,
	}, nil
}

//RemoveUser from gitlab
func (d *GitlabAdapter) RemoveUser(ctx *context.Context, request *pb.User) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Removing user: " + request.Username)

	// set user ID of user to be removed
	uid := request.Id

	// Formulate the base URL
	base, err := url.Parse(gitlabUsersEndpoint)
	if err != nil {
		return nil, err
	}

	// Attach the user ID to the base URL
	base.Path += uid
	urlPath := base.String()

	// Send req using http client
	req, err := http.NewRequest("DELETE", urlPath, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)
	req.Header.Set("Content-type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API returns 204 for successful removal of user
	if resp.StatusCode != 204 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Generic response to controller
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil

}

//ListUsers in gitlab
func (d *GitlabAdapter) ListUsers(ctx *context.Context, request *pb.Generic) (*pb.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting all users...")

	// Build Get Users API
	req, err := http.NewRequest("GET", gitlabUsersEndpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)

	// Set additional paramaters to return 100 results per request (max in Gitlab)
	params := req.URL.Query()
	params.Add("per_page", strconv.Itoa(100))
	req.URL.RawQuery = params.Encode()

	// Send req using http client
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API get users returns 200 if successful
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	// Close out connection
	defer resp.Body.Close()

	// Read through Header information and pullout current/total user pages to iterate through
	currentPage, err := strconv.Atoi(resp.Header.Get(xPage))
	if err != nil {
		return nil, err
	}
	totalPages, err := strconv.Atoi(resp.Header.Get(xTotalPage))
	if err != nil {
		return nil, err
	}

	users, err := getAllUsers(currentPage, totalPages)
	if err != nil {
		return nil, err
	}

	log.Info(fmt.Sprintf("Received %d users", len(users)))
	return &pb.Users{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Users: users,
	}, nil
}

func getAllUsers(currentPage int, totalPages int) ([]*pb.User, error) {
	var users = make([]*pb.User,0)

	for ; currentPage <= totalPages; currentPage++ {
		req, err := http.NewRequest("GET", gitlabUsersEndpoint, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set(privateTokenHeader, gitlabAccessToken)

		newPage := strconv.Itoa(currentPage)
		params := req.URL.Query()
		params.Add("per_page", strconv.Itoa(100))
		params.Add("page", newPage)
		req.URL.RawQuery = params.Encode()

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		// Gitlab API get users returns 200 if successful
		if resp.StatusCode != 200 {
			return nil, errors.New(resp.Status)
		}

		defer resp.Body.Close()

		// Read and parse API request
		var userModels []userModel
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(body, &userModels)
		if err != nil {
			return nil, err
		}

		users = updateUserList(userModels, users)


		xNextPage := resp.Header.Get(xNextPage)
		if xNextPage == "" || xNextPage == "0" {
			break
		}
	}
	return users, nil
}

func updateUserList(userModels []userModel,users []*pb.User) []*pb.User {
	// Update User list that will be sent to the controller
	for _, userModel := range userModels {
		_, reservedUser := findUser(reservedUsersList, userModel.Username)
		if reservedUser {
			continue
		}
		user := pb.User{Id: strconv.Itoa(userModel.ID), Username: userModel.Username, Email: userModel.Email}
		users = append(users, &user)
	}
	return users
}

//CreateGroup in gitlab
func (d *GitlabAdapter) CreateGroup(ctx *context.Context, request *pb.Group) (*pb.Group, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Creating group: " + request.GetPath())

	groupPath := request.Path

	// Return true if comma or colon char.
	groupSeparator := func(t rune) bool {
		return t == '/'
	}
	// Separate into fields with func.
	groupFields := strings.FieldsFunc(groupPath, groupSeparator)

	parentID := -1
	fullName := ""
	var teamGroup *groupModel

	for _, groupField := range groupFields {
		if fullName == "" {
			fullName = groupField
		} else {
			fullName = fullName + " / " + groupField
		}

		groupDetails, err := getGitlabTeamInfo(groupField, fullName)
		if err != nil {
			return nil, err
		}

		if groupDetails != nil {
			parentID = groupDetails.GroupID
			teamGroup = groupDetails
			continue
		} else {
			requestBody := groupRequestBody(parentID, groupField)
            teamGroup,err = postGroup(requestBody)
            if err != nil {
            	return nil, err
			}

			parentID = teamGroup.GroupID
		}
	}
	if teamGroup == nil {
		return nil, errors.New("teamGroup is nil")
	}

	return &pb.Group{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Id:          strconv.Itoa(teamGroup.GroupID),
		Name:        teamGroup.Name,
		Description: "",
		Path:        convertFullNameToKeycloakPath(teamGroup.FullName),
	}, nil
}

func groupRequestBody(parentID int, groupField string) []byte {
	var requestBody []byte
	if parentID == -1 {
		requestBody, _ = json.Marshal(map[string]string{
			"name":       groupField,
			"visibility": "public",
			"path":       strings.ReplaceAll(groupField, " ", "-"),
		})
	} else {
		requestBody, _ = json.Marshal(map[string]string{
			"name":       groupField,
			"visibility": "public",
			"path":       strings.ReplaceAll(groupField, " ", "-"),
			"parent_id":  strconv.Itoa(parentID),
		})
	}
	return requestBody
}

func postGroup(requestBody []byte) (*groupModel, error){

	req, err := http.NewRequest("POST", gitlabGroupEndpoint, bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)
	req.Header.Set("Content-type", "application/json")

	// Send req using http client
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 201 {
		return nil, errors.New(resp.Status)
	}
	defer resp.Body.Close()

	var groupModel groupModel
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &groupModel)
	if err != nil {
		return nil, err
	}

	return &groupModel, nil
}

//ListGroups in gitlab
func (d *GitlabAdapter) ListGroups(ctx *context.Context, request *pb.Generic) (*pb.Groups, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting groups...")

	// Build Get Groups API
	req, err := http.NewRequest("GET", gitlabGroupEndpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)

	// Execute API request
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API Get Groups returns 200 if successful
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	defer resp.Body.Close()

	currentPage, err := strconv.Atoi(resp.Header.Get(xPage))
	if err != nil {
		return nil, err
	}
	totalPages, err := strconv.Atoi(resp.Header.Get(xTotalPage))
	if err != nil {
		return nil, err
	}

	var groups  []*pb.Group

	for ; currentPage <= totalPages; currentPage++ {
        groups, err = listGroup(currentPage, groups)
		if err != nil {
			return nil, err
		}

		xNextPage := resp.Header.Get(xNextPage)
		if xNextPage == "" || xNextPage == "0" {
			break
		}
	}

	log.Info(fmt.Sprintf("received %d groups", len(groups)))

	return &pb.Groups{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}, Groups: groups}, nil
}


func listGroup(currentPage int, groups []*pb.Group) ([]*pb.Group, error) {
	req, err := http.NewRequest("GET", gitlabGroupEndpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)


	newPage := strconv.Itoa(currentPage)
	params := req.URL.Query()
	params.Add("page", newPage)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API Get Groups returns 200 if successful
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	defer resp.Body.Close()

	var groupModels []groupModel
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &groupModels)
	if err != nil {
		return nil, err
	}

	for _, groupModel := range groupModels {
		group := pb.Group{Id: strconv.Itoa(groupModel.GroupID), Name: groupModel.Name, Path: convertFullNameToKeycloakPath(groupModel.FullName)}
		groups = append(groups, &group)
	}
	return groups, nil

}

//GetGroupMembers in gitlab
func (d *GitlabAdapter) GetGroupMembers(ctx *context.Context, request *pb.Group) (*pb.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting group members for group: " + request.GetPath())
	groupID := request.GetId()

	// Build Get Group Members API
	base, err := url.Parse(gitlabGroupEndpoint)
	if err != nil {
		return nil, err
	}
	base.Path += groupID + "/members"
	urlString := base.String()
	req, err := http.NewRequest("GET", urlString, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add(privateTokenHeader, gitlabAccessToken)

	// Set additional paramaters to return 100 results per request.  This lowers the amount of loop through iterations (max in Gitlab).
	params := req.URL.Query()
	params.Add("per_page", strconv.Itoa(100))
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API Get Groups returns 200 if successful
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	// Close out connection
	defer resp.Body.Close()

	// Read through Header information and pullout current/total user pages to iterate through
	currentPage, err := strconv.Atoi(resp.Header.Get(xPage))
	if err != nil {
		return nil, err
	}
	totalPages, err := strconv.Atoi(resp.Header.Get(xTotalPage))
	if err != nil {
		return nil, err
	}

	var users []*pb.User // empty user slice that will be populated
	for ; currentPage <= totalPages; currentPage++ {
		resp, err := getGroupMemeber(currentPage, urlString)
		if err != nil {
			return nil, err
		}

        users, err = processMembers(resp, users)
		if err != nil {
			return nil, err
		}

		xNextPage := resp.Header.Get(xNextPage)
		if xNextPage == "" || xNextPage == "0" {
			break
		}

		// Close out connection
		defer resp.Body.Close()
	}

	log.Info(fmt.Sprintf("received %d users\n", len(users)))
	return &pb.Users{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}, Users: users}, nil
}

func processMembers(resp *http.Response, users []*pb.User )([]*pb.User , error) {
	// Sort through and unmarshal data.
	var userModels []userModel
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &userModels)
	if err != nil {
		return nil, err
	}

	// Parse response and append 'users' slice
	for _, userModel := range userModels {
		_, reservedUser := findUser(reservedUsersList, userModel.Username)
		if reservedUser {
			continue
		}
		user := pb.User{Id: strconv.Itoa(userModel.ID), Username: userModel.Username, Email: userModel.Email}
		users = append(users, &user)
	}
	return users, nil
}

func getGroupMemeber(currentPage int, urlString string) (*http.Response, error) {
	req, err := http.NewRequest("GET", urlString, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add(privateTokenHeader, gitlabAccessToken)

	newPage := strconv.Itoa(currentPage)
	params := req.URL.Query()
	params.Add("per_page", strconv.Itoa(100))
	params.Add("page", newPage)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

//AddUserToGroup in gitlab
func (d *GitlabAdapter) AddUserToGroup(ctx *context.Context, request *pb.GroupMember) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info(fmt.Sprintf("Adding user(%s) for group: %s",  request.User, request.Group.Path))

	// Extracting group and user information
	group := request.GetGroup()
	user := request.GetUser()

	// Iterate through each user and build the Rest API URL
	base, err := url.Parse(gitlabGroupEndpoint)
	if err != nil {
		return nil, err
	}

	base.Path += group.Id + "/members"
	params := url.Values{}
	params.Add("user_id", user.Id)
	params.Add("access_level", "20")
	base.RawQuery = params.Encode()

	urlString := base.String()

	// Send API request
	req, err := http.NewRequest("POST", urlString, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add(privateTokenHeader, gitlabAccessToken)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API user add to group returns 201 if successful
	if resp.StatusCode != 201 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Return success
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil
}

//RemoveUserFromGroup in gitlab
func (d *GitlabAdapter) RemoveUserFromGroup(ctx *context.Context, request *pb.GroupMember) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info(fmt.Sprintf("Removing user(%s) for group: %s",  request.User, request.Group.Path))

	// Extracting group and user information
	group := request.GetGroup()
	user := request.GetUser()

	// Iterate through each user and build the Rest API URL
	base, err := url.Parse(gitlabGroupEndpoint)
	if err != nil {
		return nil, err
	}

	base.Path += group.Id + "/members/" + user.Id
	urlString := base.String()

	// Send API request
	req, err := http.NewRequest("DELETE", urlString, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add(privateTokenHeader, gitlabAccessToken)
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API returns 204 for successful removal of user from group
	if resp.StatusCode != 204 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Return response back to controller
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil
}

func getGitlabTeamInfo(groupName, fullName string) (*groupModel, error) {
	req, err := http.NewRequest("GET", gitlabGroupEndpoint, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set(privateTokenHeader, gitlabAccessToken)
	params := req.URL.Query()
	params.Add("search", groupName)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Gitlab API returns 200 for successful retrieval of group information
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	var gitlabGroupInfo []groupModel
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &gitlabGroupInfo)

	for _, groupInfo := range gitlabGroupInfo {
		if groupInfo.FullName == fullName {
			group := &groupModel{GroupID: groupInfo.GroupID, Name: groupInfo.Name, FullName: groupInfo.FullName, ParentID: groupInfo.ParentID}
			return group, nil
		}
	}
	return nil, nil
}

