package adapter

import (
	"fmt"
	"gitlab.com/bloom42/libs/rz-go"
	"os"
)

//GetEnvOrExit get environment variable if exists or exit
func GetEnvOrExit(envKey string, logger *rz.Logger) string {
	// Used for Env variables (this should be a 'common' thing amongst all adapters)
	value, ok := os.LookupEnv(envKey)
	if !ok {
		logger.Fatal(fmt.Sprintf("Environment variable: %s is required but missing", envKey))
	}
	return value
}
