package adapter

import (
	pb "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"context"
	"encoding/json"
	"gitlab.com/bloom42/libs/rz-go"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

const (
	userEndpoint = "api/v4/users/"
	groupEndpoint = "api/v4/groups/"
	token = "blahblah"
)

var user1 = &pb.User{
	Meta: &pb.Meta{
		RequestId: "1",
		Status:    200,
		Requester: "requester",
	},
	Id:        "2",
	Username:  "test1",
	Email:     "test1@test.com",
	FirstName: "test1FirstName",
	LastName:  "test1LastName",
}

var user2 = &pb.User{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
	Id:        "3",
	Username:  "test2",
	Email:     "test2@test.com",
	FirstName: "test2FirstName",
	LastName:  "test2LastName",
}

var generic = &pb.Generic{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},

}

type createGroupRequest struct {
	Name string 		`json:"name"`
	ParentID string    `json:"parent_id"`
}

func TestGitlabAdapter(t *testing.T) {


	ue := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			testCreateUserRequest(w, r, t)
		case "DELETE":
			testRemoveUserRequest(w, r, t)
		case "GET":
			testListUsersRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	ru := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "DELETE":
			testRemoveUserRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	ge := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			mockCreateGroupRequest(w, r, t)
		case "GET":
			testListGroupsRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	gm := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			mockAddUserToGroupRequest(w, r, t)
		case "GET":
			mockGetGroupMembersRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}
	})

	rmg := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "DELETE":
			mockRemoveUserFromGroup(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}
	})

	handler := http.NewServeMux()
	handler.HandleFunc("/" + userEndpoint, ue)
	handler.HandleFunc("/" + userEndpoint + "2", ru)
	handler.HandleFunc("/" + groupEndpoint, ge)
	handler.HandleFunc("/" + groupEndpoint + "6/members", gm)
	handler.HandleFunc("/" + groupEndpoint + "6/members/6", rmg)

	srv := httptest.NewServer(handler)
	defer srv.Close()

	os.Setenv("GITLAB_GROUPS_URL", srv.URL + "/" + groupEndpoint)
	os.Setenv("GITLAB_USERS_URL", srv.URL + "/" + userEndpoint)
	os.Setenv("GITLAB_ACCESS_TOKEN", token)

	adapter := GitlabAdapter{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	log := rz.FromCtx(ctx)
	adapter.Startup(log, []string{"root", "ghost_user"})

	testCreateUser(ctx, adapter, t)
	testListUsers(ctx, adapter, t)
	testRemoveUser(ctx, adapter, t)

	testListGroups(ctx, adapter, t)
	testCreateGroup(ctx, adapter, t)

	testGetGroupMembers(ctx, adapter, t)
	testAddUserToGroup(ctx, adapter, t)
	testRemoveUserFromGroup(ctx, adapter, t)
}

func testCreateUser(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	u, err := adapter.CreateUser(&ctx, user1)
	if err != nil {
		t.Errorf("error in creating user %v", err)
	} else {
		validateUserResponse(u, user1, t)
	}
}

func testCreateUserRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateRequestHeader(r, t)
	validateQueryParam(r,"username", "test1", t)
	validateQueryParam(r,"email", "test1@test.com", t)
	validateQueryParam(r,"name", "test1", t)
	validateQueryParam(r,"reset_password", "true", t)
	validateQueryParam(r,"can_create_group", "false", t)
	validateQueryParam(r,"provider", "openid_connect", t)
	validateQueryParam(r,"extern_uid", "test1", t)
	validateQueryParam(r,"skip_confirmation", "true", t)

	resp :=`{
    "id": 2,
    "name": "test1",
    "username": "test1",
    "state": "active",
    "avatar_url": "https://secure.gravatar.com/avatar/94fba03762323f286d7c3ca9e001c541?s=80&d=identicon",
    "web_url": "https://code_dev_url.com/test1",
    "created_at": "2020-11-21T01:31:59.924Z",
    "bio": null,
    "location": null,
    "public_email": "",
    "skype": "",
    "linkedin": "",
    "twitter": "",
    "website_url": "",
    "organization": null,
    "job_title": "",
    "work_information": null,
    "last_sign_in_at": null,
    "confirmed_at": "2020-11-21T01:31:59.604Z",
    "last_activity_on": null,
    "email": "test1@test.com",
    "theme_id": 1,
    "color_scheme_id": 1,
    "projects_limit": 100000,
    "current_sign_in_at": null,
    "identities": [
        {
            "provider": "openid_connect",
            "extern_uid": "test1"
        }
    ],
    "can_create_group": false,
    "can_create_project": true,
    "two_factor_enabled": false,
    "external": false,
    "private_profile": false,
    "is_admin": false
}`

	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(resp))
}

func testRemoveUser(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	g, err := adapter.RemoveUser(&ctx, user1)
	if err != nil {
		t.Errorf("error in removing user %v", err)
	} else {
		validateGenericResponse(g, user1, t)
	}

}

func testRemoveUserRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "DELETE", t)
	validateRequestHeader(r, t)

	resp :=""

	b,_ := json.Marshal(resp)
	w.WriteHeader(http.StatusNoContent)
	_, _ = w.Write(b)
}

func testListUsers(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	us, err := adapter.ListUsers(&ctx, generic)
	if err != nil {
		t.Errorf("error in listing users %v", err)
	} else {
		if len(us.GetUsers()) != 2 {
			t.Errorf("error in number users %v", us.GetUsers())
		}

		for _, u := range us.GetUsers() {
			if u.GetUsername() == user1.GetUsername() {
				validateUserResponse(u, user1, t)
			} else {
				validateUserResponse(u, user2, t)
			}
		}
	}

}

func testListUsersRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	validateRequestHeader(r, t)

	resp :=`[
    {
        "id": 3,
        "name": "test2",
        "username": "test2",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/f2c97b1f2d2898cd2d6466ce95d4ba33?s=80&d=identicon",
        "web_url": "https://code_dev_url.com/test2",
        "created_at": "2020-11-21T01:32:38.037Z",
        "bio": null,
        "location": null,
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": null,
        "job_title": "",
        "work_information": null,
        "last_sign_in_at": null,
        "confirmed_at": "2020-11-21T01:32:37.714Z",
        "last_activity_on": null,
        "email": "test2@test.com",
        "theme_id": 1,
        "color_scheme_id": 1,
        "projects_limit": 100000,
        "current_sign_in_at": null,
        "identities": [
            {
                "provider": "openid_connect",
                "extern_uid": "test2"
            }
        ],
        "can_create_group": false,
        "can_create_project": true,
        "two_factor_enabled": false,
        "external": false,
        "private_profile": false,
        "is_admin": false
    },
    {
        "id": 2,
        "name": "test1",
        "username": "test1",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/94fba03762323f286d7c3ca9e001c541?s=80&d=identicon",
        "web_url": "https://code_dev_url.com/test1",
        "created_at": "2020-11-21T01:31:59.924Z",
        "bio": null,
        "location": null,
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": null,
        "job_title": "",
        "work_information": null,
        "last_sign_in_at": null,
        "confirmed_at": "2020-11-21T01:31:59.604Z",
        "last_activity_on": null,
        "email": "test1@test.com",
        "theme_id": 1,
        "color_scheme_id": 1,
        "projects_limit": 100000,
        "current_sign_in_at": null,
        "identities": [
            {
                "provider": "openid_connect",
                "extern_uid": "test1"
            }
        ],
        "can_create_group": false,
        "can_create_project": true,
        "two_factor_enabled": false,
        "external": false,
        "private_profile": false,
        "is_admin": false
    },
    {
        "id": 1,
        "name": "Administrator",
        "username": "root",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
        "web_url": "https://code_dev_url.com/root",
        "created_at": "2020-11-21T00:50:13.904Z",
        "bio": null,
        "location": null,
        "public_email": "",
        "skype": "",
        "linkedin": "",
        "twitter": "",
        "website_url": "",
        "organization": null,
        "job_title": "",
        "work_information": null,
        "last_sign_in_at": "2020-11-21T01:22:49.865Z",
        "confirmed_at": "2020-11-21T00:50:13.745Z",
        "last_activity_on": "2020-11-21",
        "email": "admin@example.com",
        "theme_id": 1,
        "color_scheme_id": 1,
        "projects_limit": 100000,
        "current_sign_in_at": "2020-11-21T01:22:49.865Z",
        "identities": [],
        "can_create_group": true,
        "can_create_project": true,
        "two_factor_enabled": false,
        "external": false,
        "private_profile": false,
        "is_admin": true
    }
]`

	w.Header().Set("x-page","1")
	w.Header().Set("x-total-pages","1")
	w.Header().Set("x-next-page","0")

	_, _ = w.Write([]byte(resp))
}

func testCreateGroup(ctx context.Context, adapter GitlabAdapter, t *testing.T) {

	gs, err := adapter.CreateGroup(&ctx, &pb.Group{
		Meta: &pb.Meta{
			RequestId: "2",
			Status:    200,
			Requester: "requester",
		},
		Id:          "",
		Name:        "Tron",
		Description: "Tron",
		Path:        "/Platform Two/Tron",
	})
	if err != nil {
		t.Errorf("error in create group %v", err)
		t.Fail()
	}

	if gs.GetName() != "Tron" {
		t.Errorf("name should be Tron, found %s", gs.GetName())
		t.Fail()
	}

	if gs.GetPath() != "/Platform Two/Tron" {
		t.Errorf("found path: %s", gs.GetPath())
		t.Fail()
	}

	if gs.GetId() != "11" {
		t.Errorf("found id: %s", gs.GetId())
		t.Fail()
	}
}

func mockCreateGroupRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	var createGroupRequest createGroupRequest
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.Errorf("error reading request body: %v", err)
		t.Fail()
	}
	err = json.Unmarshal(body, &createGroupRequest)
	if err != nil {
		t.Errorf("error reading request body: %v", err)
		t.Fail()
	}
	if createGroupRequest.Name == "Platform Two" {
		testCreatePlatformTwoGroupRequest(w, r, t)
	} else if createGroupRequest.Name == "Tron" {
		testCreateTronGroupRequest(w, r, t)
	} else {
		t.Errorf("error mocking serivce")
		t.Fail()
	}
}

func testCreateTronGroupRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateRequestHeader(r, t)

	resp := `{
	   "id": 11,
	   "web_url": "https://code_dev_url.com/groups/Platform-Two/Tron",
	   "name": "Tron",
	   "path": "Tron",
	   "description": "",
	   "visibility": "private",
	   "share_with_group_lock": false,
	   "require_two_factor_authentication": false,
	   "two_factor_grace_period": 48,
	   "project_creation_level": "developer",
	   "auto_devops_enabled": null,
	   "subgroup_creation_level": "maintainer",
	   "emails_disabled": null,
	   "mentions_disabled": null,
	   "lfs_enabled": true,
	   "default_branch_protection": 2,
	   "avatar_url": null,
	   "request_access_enabled": true,
	   "full_name": "Platform Two / Tron",
	   "full_path": "Platform-Two/Tron",
	   "created_at": "2020-11-21T01:45:28.793Z",
	   "parent_id": 10,
	   "projects": [],
	   "shared_projects": []
	}`
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(resp))
}

func testCreatePlatformTwoGroupRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateRequestHeader(r, t)

	resp :=`{
    "id": 10,
    "web_url": "https://code_dev_url.com/groups/Platform-Two",
    "name": "Platform Two",
    "path": "Platform-Two",
    "description": "",
    "visibility": "private",
    "share_with_group_lock": false,
    "require_two_factor_authentication": false,
    "two_factor_grace_period": 48,
    "project_creation_level": "developer",
    "auto_devops_enabled": null,
    "subgroup_creation_level": "maintainer",
    "emails_disabled": null,
    "mentions_disabled": null,
    "lfs_enabled": true,
    "default_branch_protection": 2,
    "avatar_url": null,
    "request_access_enabled": true,
    "full_name": "Platform Two",
    "full_path": "Platform-Two",
    "created_at": "2020-11-21T01:40:46.737Z",
    "parent_id": null,
    "projects": [],
    "shared_projects": []
}`
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(resp))
}

func testListGroups(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	gs, err := adapter.ListGroups(&ctx, generic)
	if err != nil {
		t.Errorf("error in listing groups %v", err)
	} else {
		if len(gs.GetGroups()) != 2 {
			t.Errorf("error in number users %v", gs.GetGroups())
		}
		for _, g := range gs.GetGroups() {
			if g.GetName() == "Platform One" {
				if g.GetPath() != "/Platform One" {
					t.Errorf("error wrong path name %v", g)
				}

			} else if g.GetName() == "Tron" {
				if g.GetPath() != "/Platform One/Tron" {
					t.Errorf("error wrong path name %v", g)
				}

			} else {
				t.Errorf("error invalid group %v", g)
			}
		}
	}

}

func testListGroupsRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	validateRequestHeader(r, t)

	resp :=`[
    {
        "id": 5,
        "web_url": "https://code_dev_url.com/groups/Platform-One",
        "name": "Platform One",
        "path": "Platform-One",
        "description": "",
        "visibility": "public",
        "share_with_group_lock": false,
        "require_two_factor_authentication": false,
        "two_factor_grace_period": 48,
        "project_creation_level": "developer",
        "auto_devops_enabled": null,
        "subgroup_creation_level": "maintainer",
        "emails_disabled": null,
        "mentions_disabled": null,
        "lfs_enabled": true,
        "default_branch_protection": 2,
        "avatar_url": null,
        "request_access_enabled": true,
        "full_name": "Platform One",
        "full_path": "Platform-One",
        "created_at": "2020-11-21T01:40:46.737Z",
        "parent_id": null
    },
    {
        "id": 6,
        "web_url": "https://code_dev_url.com/groups/Platform-One/Tron",
        "name": "Tron",
        "path": "Tron",
        "description": "",
        "visibility": "public",
        "share_with_group_lock": false,
        "require_two_factor_authentication": false,
        "two_factor_grace_period": 48,
        "project_creation_level": "developer",
        "auto_devops_enabled": null,
        "subgroup_creation_level": "maintainer",
        "emails_disabled": null,
        "mentions_disabled": null,
        "lfs_enabled": true,
        "default_branch_protection": 2,
        "avatar_url": null,
        "request_access_enabled": true,
        "full_name": "Platform One / Tron",
        "full_path": "Platform-One/Tron",
        "created_at": "2020-11-21T01:45:28.793Z",
        "parent_id": 5
    }
]`
	w.Header().Set("x-page","1")
	w.Header().Set("x-total-pages","1")
	w.Header().Set("x-next-page","0")
	_, _ = w.Write([]byte(resp))
}

func mockGetGroupMembersRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	validateRequestHeader(r, t)


	resp :=`[
   {
       "id": 1,
       "name": "Administrator",
       "username": "root",
       "state": "active",
       "avatar_url": "https://secure.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
       "web_url": "https://code_dev_url.com/root",
       "access_level": 50,
       "expires_at": null
   },
   {
       "id": 5,
       "name": "test3",
       "username": "test3",
       "state": "active",
       "avatar_url": "https://secure.gravatar.com/avatar/59c02e9fd89cfb6fa54b67e846d0631b?s=80&d=identicon",
       "web_url": "https://code_dev_url.com/test3",
       "access_level": 30,
       "expires_at": null
   },
   {
       "id": 6,
       "name": "test4",
       "username": "test4",
       "state": "active",
       "avatar_url": "https://secure.gravatar.com/avatar/33ead0fffce3518ee97d59348a3708af?s=80&d=identicon",
       "web_url": "https://code_dev_url.com/test4",
       "access_level": 30,
       "expires_at": null
   }
]`
	w.Header().Set("x-page","1")
	w.Header().Set("x-total-pages","1")
	w.Header().Set("x-next-page","0")
	_, _ = w.Write([]byte(resp))
}

func testGetGroupMembers(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	gm, err := adapter.GetGroupMembers(&ctx, &pb.Group{
		Meta:    &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
		},
		Id:          "6",
		Name:        "Tron",
		Description: "",
		Path:        "/Platform One/Tron",
	})

	if err != nil {
		t.Errorf("error in listing groups members: %v", err)
		t.Fail()
	}
	if len(gm.GetUsers()) != 2 {
		t.Errorf("Found more than 2 users: %v", gm.GetUsers())
		t.Fail()
	}
	for _, user := range gm.GetUsers() {
		if user.Username == "test3" {
			if user.Id != "5" {
				t.Errorf("Wrong id for user, found: %s", user.Id)
				t.Fail()
			}
		} else if user.Username == "test4" {
			if user.Id != "6" {
				t.Errorf("Wrong id for user, found: %s", user.Id)
				t.Fail()
			}
		} else {
			t.Errorf("Unexpected user, found: %s", user.Id)
			t.Fail()
		}
	}
}

func mockAddUserToGroupRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateRequestHeader(r, t)

	validateQueryParam(r,"user_id", "10", t)
	validateQueryParam(r,"access_level", "20", t)
	w.WriteHeader(http.StatusCreated)
}

func testAddUserToGroup(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	meta := &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	}
	resp, err := adapter.AddUserToGroup(&ctx, &pb.GroupMember{
		Meta: meta,
		Group: &pb.Group{
			Meta:        meta,
			Id:          "6",
			Name:        "Tron",
			Description: "",
			Path:        "/Platform One/Tron",
		},
		User:  &pb.User{
			Meta:      meta,
			Id:        "10",
			Username:  "test10",
			Email:     "test10@gmail.com",
			FirstName: "test10",
			LastName:  "test10",
		},
	})

	if err != nil {
		t.Errorf("Found error: %v", err)
		t.Fail()
	}

	if resp.GetMeta().RequestId != "2" {
		t.Errorf("Wrong request id found: %s", resp.GetMeta().GetRequestId())
		t.Fail()
	}

	if resp.GetMeta().GetRequester() != "requester" {
		t.Errorf("Unexpected requester found: %s", resp.GetMeta().GetRequester())
		t.Fail()
	}
}

func mockRemoveUserFromGroup(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "DELETE", t)
	validateRequestHeader(r, t)

	w.WriteHeader(http.StatusNoContent)
}

func testRemoveUserFromGroup(ctx context.Context, adapter GitlabAdapter,  t *testing.T) {
	meta := &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	}
	resp, err := adapter.RemoveUserFromGroup(&ctx, &pb.GroupMember{
		Meta: meta,
		Group: &pb.Group{
			Meta:        meta,
			Id:          "6",
			Name:        "Tron",
			Description: "",
			Path:        "/Platform One/Tron",
		},
		User:  &pb.User{
			Meta:      meta,
			Id:        "6",
			Username:  "test6",
			Email:     "test6@gmail.com",
			FirstName: "test6",
			LastName:  "test6",
		},
	})

	if err != nil {
		t.Errorf("Found error: %v", err)
		t.Fail()
	}

	if resp.GetMeta().RequestId != "2" {
		t.Errorf("Wrong request id found: %s", resp.GetMeta().GetRequestId())
		t.Fail()
	}

	if resp.GetMeta().GetRequester() != "requester" {
		t.Errorf("Unexpected requester found: %s", resp.GetMeta().GetRequester())
		t.Fail()
	}
}

func validateUserResponse(u *pb.User, orig *pb.User, t *testing.T) {
	if u == nil {
		t.Errorf("error in user %v", u)
	}
	if u.GetUsername() != orig.GetUsername(){
		t.Errorf("error in user %v", u)
	}
	if u.GetEmail()!= orig.GetEmail(){
		t.Errorf("error in user %v", u)
	}
	if u.GetUsername() != orig.GetUsername(){
		t.Errorf("error in user %v", u)
	}
	/*
	if u.GetFirstName() != orig.GetUsername(){
		t.Errorf("error in creating user %v", u)
	}
	if u.GetLastName() != orig.GetUsername(){
		t.Errorf("error in creating user %v", u)
	}
	*/

}

func validateGenericResponse(g *pb.Generic, u *pb.User, t *testing.T) {
	if g == nil {
		t.Errorf("error Generic should not be nil")
	} else {
		if g.Meta.GetRequester() != u.GetMeta().GetRequester() {
			t.Errorf("error requester mismatch %v", g)
		}
		if g.Meta.GetRequestId() != u.GetMeta().RequestId {
			t.Errorf("error requestId mismatch %v", g)
		}
		if g.Meta.GetStatus() != 200 {
			t.Errorf("error status mismatch %v", g)
		}
	}
}

func validateQueryParam(r *http.Request, key string, value string, t *testing.T) {
	temp := r.URL.Query().Get(key)
	if  temp != value {
		t.Errorf("error in param %v should be %v found %v", key, value, temp)
	}
}

func validateRequestMethod(r *http.Request, method string, t *testing.T) {
	if r.Method != method {
		t.Errorf("error request method should be %v got %v",method, r.Method)
	}
}

func validateRequestHeader(r *http.Request, t *testing.T) {
	tok := r.Header.Get("PRIVATE_TOKEN")
	if tok != token {
		t.Errorf("error token not set correctly, got %v", tok)
	}
}


