package main

import (
	ad "gitlab.com/_pratheesh/gad/gitlab-svc/adapter"
	"gitlab.com/_pratheesh/gad/svc-base/baseadapter"
)

func main() {
	baseadapter.Start(&ad.GitlabAdapter{})
}
