Gitlab SVC
-------------
Gitlab service adapter for group authorization distribution.

## Dependencies

This project depends on the [svc-base](https://gitlab.com/_pratheesh/gad/svc-base) project as well as other modules specified in [go.mod](go.mod).

## Use

This is intended to be used as part of the [group-sync-controller](https://gitlab.com/_pratheesh/gad/group-sync-controller) project.

## Behavior

|Method|Supported|Notes|
|------|---------|-----|
|CreateUser|Yes|Users by default dont have the rights to create a group |
|RemoveUser|Yes||
|ListUser|Yes||
|CreateGroup|Yes|A group is created for each entry in the path if not already there. For example, `/Platform One/Tron/Puckboard`, A group will be created at each level, Platform One, Platform One/Tron, Platform One/Tron/Puckboard|
|ListGroup|Yes||
|AddUserToGroup|Yes|Users is added to the group at the reporter level |
|RemoveUserFromGroup|Yes||
|GetGroupMembers|Yes||

## Development
1) Since this project has a private dependency in order to be able to pull down the svc-base module, you will need to create ~/.netrc to include your token:
```aidl
machine domain.com
  login yourusername
  password yourAccessToken
```
2) Make sure `GOPRIVATE="gitlab.com/_pratheesh/gad/svc-base"` in your `go env`


## Minimum Required Environment Variables Used
```$xslt
#access token for gitlab admin user
GITLAB_ACCESS_TOKEN=<access_token>

#url path for groups endpoint
GITLAB_GROUPS_URL=https://3.32.25.89/api/v4/groups/

#url path for user endpoint
GITLAB_USERS_URL=https://3.32.25.89/api/v4/users/
```

## Run the adapter
The adapter is built on top of the svc-base module. The svc-base also supports other common configurations for all adapters. See https://gitlab.com/_pratheesh/gad/svc-base for detail.
```aidl
go run main.go
```

## Run unit tests
```
go test ./...
```