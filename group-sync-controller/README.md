### Environment Variables Used:
```
#keycloak token endpoint url
KEYCLOAK_TOKEN_ENDPOINT=https://localhost:8443/auth/realms/baby-yoda/protocol/openid-connect/token

#Example keycloak api endpoints for different IL
#il2 endpoint: https://localhost:8443/auth/realms/baby-yoda/groups/il2
#il4 endpoint: https://localhost:8443/auth/realms/baby-yoda/groups/il4
#il5 endpoint: https://localhost:8443/auth/realms/baby-yoda/groups/il5
KEYCLOAK_GROUP_ENDPOINT=https://localhost:8443/auth/realms/baby-yoda/groups/il2

#keycloak username. This user needs to created in the baby-yoda realm. Username is hardcoded to keycloak, only the users listed below will be able to make calls to the custom API.
#for il2 use: il2_api_user
#for il4 use: il4_api_user
#for il5 use: il5_api_user
KEYCLOAK_API_USER_NAME=il2_api_user

#password configured given keycloak username 
KEYCLOAK_API_PASSWORD=keycloak123!@#

#polling interval for this controller to poll the keycloak in seconds
KEYCLOAK_POLL_INTERVAL_SECONDS=30

#defines a list of adapters this controller should be talking to. format is <adapterName>@<adapterAddress>. 
#<adapterName> is referrenced in the DEVELOPER_APPS and COLLABORATOR_APPS envs
WORKLOAD_ADAPTERS=gitlab@localhost:50051,jira@localhost:50051

#tells the controller which adapters are considerred developer applications
DEVELOPER_APPS=gitlab,jira

#tells the controller which adapters are considerred collaborator applications
COLLABORATOR_APPS=jira

#tells controller to use given cert file to authenticate with the adapters
#CA_CERT_FILE="./ca-cert.pem"
```

### Keycloak
To understand keycloak group struct in P1, read: https://confluence.il2.code_dev_url.com/pages/viewpage.action?spaceKey=P1&title=Tool+Group+Permissions

To run a local keycloak for testing with the custom api checkout https://repo1.code_dev_url.com/platform-one/private/big-bang/apps/keycloak/-/tree/group-api
Follow the README in the p1-image folder to run Keycloak locally.

For testing in IL2 environment, A `il2_api_user` will need to be created in the baby-yoda realm for the custom api to work. In order for regular users to be picked up by the controller, make sure users are added to `Impact Level 2 Authorized` group and the corresponding `developer` or `collaborator` group. When creating the new users, make sure to clear all user actions and check email verified. 


### Run the app
Once environment variables are configured, run the controller by 
```aidl
go run main.go
```

### Unit Tests
To run unit tests: `go test ./...`

### Docker Commands
```
docker rm group-sync-controller
docker build -t group-sync-controller:dev-latest .
docker run --env-file var.env group-sync-controller:dev-latest
```

### MISC
* Use SSL Cert for gRPC Server Authentication. https://dev.to/techschoolguru/how-to-secure-grpc-connection-with-ssl-tls-in-go-4ph

