import * as kc from './keycloak'
import * as gl from './gitlab'

describe('Test Group Sync', function () {

    const devUser = 'developer1';
    const colUser = 'collaborator1';
    const password = 'keycloak123!@#';
    const il2_api_user = 'il2_api_user';
    const developerUserId = 'devUid';
    const collaboratorUserId = 'colUid';

    it('Clean up Gitlab and keycloak before test', function () {
        gl.removeAllUsers();
        gl.removeAllGroups();
        gl.getUsersCount().then(count => cy.log("Total users in gitlab: " + count));
        gl.getGroupsCount().then(count => cy.log("Total groups in gitlab: " + count));
        kc.setTokenLifespan();
        kc.removeAllUsers().then(done => {

            cy.log("Adding users to bravvo group...")
            kc.addNewUsersToGroups([kc.il2Group, kc.bravvosDeveloperGroup], 110)


            kc.createUser(il2_api_user, []).then(uid => {
                kc.clearUserActions(uid);
                kc.setUserPassword(uid, password);
            });
            kc.getUsersCount().then(count => {
                cy.log("current user count = " + count);
            });
        });

    });

    it('Waiting for all users and groups to be synced', function () {
        cy.wait(120000);
    });

    it('verify there more than 100 users in Gitlab', function () {
        gl.getUsersCount().then(count => {
            expect(count).to.be.greaterThan(100);
        })
    });

    it('Setup Keycloak user', function () {
        kc.createUser(devUser, [kc.il2Group, kc.bravvosDeveloperGroup, kc.puckboardDeveloperGroup])
            .then(uid => {
                cy.task('set', {key: developerUserId, value: uid})
                kc.clearUserActions(uid);
                kc.setUserPassword(uid, password);
            });

        kc.createUser(colUser, [kc.il2Group, kc.bravvosCollaboratorGroup])
            .then(uid => {
                cy.task('set', {key: collaboratorUserId, value: uid})
                kc.clearUserActions(uid)
                kc.setUserPassword(uid, password);
            });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function () {
        cy.wait(60000)
    });

    //verify user is created
    //verify group is created
    //verify user is added to the group
    it('Verify Gitlab state', function () {
        gl.verifyUserInGroup(devUser, kc.bravvosGroup)
        // gl.verifyUserInGroup(devUser, kc.puckboardGroup)
        gl.verifyUserNotExist(colUser)
    });

    //test developer can login successfully
    gl.login(devUser, password);

    //remove user from the group
    it('Remove user from Bravvo developer group on Keycloak', function () {
        cy.task('get', developerUserId).then(uid => {
            kc.removeUserFromGroup(uid, kc.bravvosDeveloperGroupId)
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function () {
        cy.wait(60000)
    });

    //verify user is removed from the group
    it('Verify user removed from Bravvo group in Gitlab', function () {
        gl.verifyUserInGroup(devUser, kc.puckboardGroup)
        gl.verifyUserNotInGroup(devUser, kc.bravvosGroup)
    });

    it('Remove user from Puckboard group in Keycloak', function () {
        cy.task('get', developerUserId).then(uid => {
            kc.removeUserFromGroup(uid, kc.puckboardDeveloperGroupId)
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function () {
        cy.wait(60000)
    });

    //verify user is removed from the group
    it('Verify user removed from Puckboard group in Gitlab', function () {
        gl.verifyUserNotExist(devUser)
    });

});


describe('Test User removed from IL Group', function () {
    const devUser = 'developer1';
    const password = 'keycloak123!@#';
    const il2_api_user = "il2_api_user";
    const developerUserId = 'devUid';

    it('Setup Keycloak user', function () {
        kc.removeAllUsers();
        kc.createUser(il2_api_user, []).then(uid=> {
            kc.clearUserActions(uid);
            kc.setUserPassword(uid, password);
        });

        kc.createUser(devUser, [kc.il2Group, kc.puckboardDeveloperGroup]).then(uid => {
            cy.task('set', {key: developerUserId, value: uid})
            kc.clearUserActions(uid);
            kc.setUserPassword(uid, password);
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function() {
        cy.wait(120000);
    });

    //verify user is created
    //verify group is created
    //verify user is added to the group
    it('Verify Gitlab state', function () {
        gl.verifyUserInGroup(devUser, kc.puckboardGroup);
    });

    gl.login(devUser, password);

    it('Remove user from IL2 group', function () {
        cy.task('get', developerUserId).then(uid => {
            kc.removeUserFromGroup(uid, kc.il2GroupId)
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function () {
        cy.wait(60000);
    });

    //verify user is added to the group
    it('Verify user is removed from IL2 Gitlab', function () {
        gl.verifyUserNotExist(devUser)
    });
});
