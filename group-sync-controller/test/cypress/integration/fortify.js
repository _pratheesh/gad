
const baseUrl = "https://fortify.dev.code_dev_url.com/api/v1"
const adminUser = "admin"
const adminPass = "fortify123!@#"


export function getToken() {
    return cy.request({
        method: 'POST',
        url: baseUrl + '/tokens',
        headers: {
            'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
        },
        body: {
            type: "UnifiedLoginToken",
        }
    }).then(response => {
            expect(response.status).to.eq(201);
            return response.body['data']['token'];
        }
    )
}


