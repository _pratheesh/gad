import * as kc from './keycloak'
import * as sq from './sonarqube'

describe('Test Group Sync', function() {
    const devUser = 'developer1';
    const colUser = 'collaborator1';
    const password = 'keycloak123!@#';
    const il2_api_user = 'il2_api_user';
    const developerUserId = 'devUid';
    const collaboratorUserId = 'colUid';
    let massUsers = []

    it('Clean up Gitlab and keycloak before test', function () {
        kc.setTokenLifespan();
        kc.removeAllUsers().then(done => {

            cy.log("Adding users to bravvo group...")
            massUsers = kc.addNewUsersToGroups([kc.il2Group, kc.bravvosDeveloperGroup], 56, password);

            kc.getUsersCount().then(count => {
                cy.log("current user count = " + count);
            });
        }).then(done => {
            sq.removeAllUsers();
            sq.removeAllGroups();
        });
    });

    // it('Waiting for all users and groups to be synced', function () {
    //     cy.wait(120000);
    // });

    it('Setup keycloak user', function() {
        kc.createUser(il2_api_user, []).then(uid=> {
            kc.clearUserActions(uid);
            kc.setUserPassword(uid, password);
        });

        kc.createUser(devUser, [kc.il2Group, kc.bravvosDeveloperGroup, kc.puckboardDeveloperGroup])
            .then(uid=> {
                cy.task('set', { key: developerUserId, value: uid})
                kc.clearUserActions(uid);
                kc.setUserPassword(uid, password);
            });

        kc.createUser(colUser, [kc.il2Group, kc.bravvosCollaboratorGroup])
            .then(uid => {
                cy.task('set', { key: collaboratorUserId, value: uid})
                kc.clearUserActions(uid)
                kc.setUserPassword(uid, password);
            });
    });

    it("Login all users", function () {
        massUsers.forEach(username => {
            sq.login(username, password);
        });
        sq.login(devUser, password)
        sq.login(colUser, password)
    });

    it('Verify mass users are created', function() {
        sq.getAllUsers().then(users => {
            expect(users.length).to.be.greaterThan(50);
        })
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function() {
        cy.wait(60000)
    });

    //verify user is created
    //verify group is created
    //verify user is added to the group
    it('Verify sonarqube state', function() {
        sq.verifyUserInGroup(devUser, kc.bravvosGroup)
        sq.verifyUserInGroup(devUser, kc.puckboardGroup)
        sq.verifyUserNotExist(colUser)
    });

    //remove user from the group
    it('Remove user from bravvo developer group on keycloak', function() {
        cy.task('get', developerUserId).then(uid => {
            kc.removeUserFromGroup(uid, kc.bravvosDeveloperGroupId)
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function() {
        cy.wait(60000)
    });

    //verify user is removed from the group
    it('Verify user removed from Bravvo group in Sonarqube', function() {
        sq.verifyUserInGroup(devUser, kc.puckboardGroup)
        sq.verifyUserNotInGroup(devUser, kc.bravvosGroup)
    });

    it('Remove user from Puckboard group in keycloak', function() {
        cy.task('get', developerUserId).then(uid => {
            kc.removeUserFromGroup(uid, kc.puckboardDeveloperGroupId)
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function() {
        cy.wait(60000)
    });

    //verify user is removed from the group
    it('Verify user removed from Puckboard group in Sonarqube', function() {
        sq.verifyUserNotExist(devUser)
    });
});


describe('Test User removed from IL Group', function() {
    const devUser = 'developer1';
    const password = 'keycloak123!@#';
    const il2_api_user = "il2_api_user";
    const developerUserId = 'devUid';

    it('Clean up keycloakd and sonarqube', function() {
        kc.removeAllUsers().then(done => {
            sq.removeAllUsers();
            sq.removeAllGroups();
        });
    });

    it('Set up keycloak', function() {
        kc.createUser(il2_api_user, []).then(uid=> {
            kc.clearUserActions(uid);
            kc.setUserPassword(uid, password);
        });

        kc.createUser(devUser, [kc.il2Group, kc.puckboardDeveloperGroup]).then(uid=> {
            cy.task('set', { key: developerUserId, value: uid})
            kc.clearUserActions(uid);
            kc.setUserPassword(uid, password);
        });
    });

    it('Log in developer user', function() {
        sq.login(devUser, password);
    })

    //sleep for group sync poll interval
    it('Wait for group sync', function() {
        cy.wait(120000);
    });

    //verify user is created
    //verify group is created
    //verify user is added to the group
    it('Verify sonarqube state', function() {
        sq.verifyUserInGroup(devUser, kc.puckboardGroup);
    });

    it('Remove user from IL2 group', function() {
        cy.task('get', developerUserId).then(uid => {
            kc.removeUserFromGroup(uid, kc.il2GroupId)
        });
    });

    //sleep for group sync poll interval
    it('Wait for group sync', function() {
        cy.wait(60000);
    });

    //verify user is added to the group
    it('Verify user is removed from IL2 sonarqube', function() {
        sq.verifyUserNotExist(devUser)
    });
});

