process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

export const il2Group = "/Impact Level 2 Authorized";
export const il2GroupId = "00eb8904-5b88-4c68-ad67-cec0d2e07aa6";

export const bravvosGroup = "/Platform One/Iron Bank/Braavos";
export const bravvosDeveloperGroup = "/Platform One/Iron Bank/Braavos/IL2/development/developer";
export const bravvosCollaboratorGroup = "/Platform One/Iron Bank/Braavos/IL2/development/collaborator";
export const bravvosDeveloperGroupId = "81430ace-659f-46b0-a5cd-e6ad2b3f3fee";
export const bravvosCollaboratorGroupId = "04d465c2-0722-4d96-a7dc-4c33560d997c";

export const puckboardGroup = "/Platform One/Tron/Puckboard";
export const puckboardDeveloperGroup = "/Platform One/Tron/Puckboard/IL2/development/developer";
export const puckboardDeveloperGroupId = "00dcad67-e6a4-4a7c-94b6-4b4cb500c82a";

export const perfTestGroup0DevGroup = "/PerfTestParentGroup/group0/IL2/development/developer"

export const platformOneGroupId = "a8604cc9-f5e9-4656-802d-d05624370245";
export const baseUrl = 'https://keycloak-admin.dev.code_dev_url.com';
// export const baseUrl = 'https://localhost:8443';
// const adminUsername = 'admin';
// const adminPass = 'pass';
const adminUsername = 'keycloak';
const adminPass = 'keycloak123!@#';

export function setTokenLifespan() {
    getToken().then(token => {
        cy.request({
            method: 'PUT',
            url: baseUrl + '/auth/admin/realms/master',
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: {
                accessTokenLifespan: 36000,
            }
        }).then( response => {
            expect(response.status).to.eq(204)
        })
    })
}

function getToken() {
    return cy.request({
        method: 'POST',
        url: baseUrl + '/auth/realms/master/protocol/openid-connect/token',
        form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
        body: {
            username: adminUsername,
            password: adminPass,
            grant_type: 'password',
            client_id: 'admin-cli',
        }
    }).then(response =>
        response.body['access_token']
    )
}

export function removeAllUsers() {
    return getToken().then(token => {
        cy.request({
            method: 'GET',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users?max=1000',
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
                expect(response.status).to.eq(200);
                getUsersCount().then(count => {
                    expect(response.body.length).to.eq(count);
                });
                response.body.forEach(user => {
                    expect(user).to.have.property('id');
                    const delUrl = baseUrl + '/auth/admin/realms/baby-yoda/users/' + user['id']
                    cy.request({
                        method: 'DELETE',
                        url: delUrl,
                        headers: {
                            'Authorization': 'Bearer ' + token
                        },
                    }).then(response =>
                        expect(response.status).to.eq(204)
                    )
                });
                getUsersCount().then(count => {
                    expect(count).to.eq(0);
                })
                return cy.wrap("done");
            }
        )
    })
}

export function removeUser(userId) {
    getToken().then(token => {
        cy.request({
            method: 'DELETE',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users' + userId,
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response =>
            expect(response.status).to.eq(204)
        )
    })
}

export function getUsersCount() {
    return getToken().then(token => {
        cy.request({
            method: 'GET',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users/count',
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
                expect(response.status).to.eq(200);
                return response.body;
        });
    })
}

export function createUser(username, groups) {
    return getToken().then(token => {
        cy.request({
            method: 'POST',
            // failOnStatusCode: false,
            url: baseUrl + '/auth/admin/realms/baby-yoda/users',
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: {
                email: username + "@revacomm.com",
                emailVerified: true,
                enabled: true,
                firstName: username + "F",
                lastName: username + "L",
                username: username,
                groups: groups,
            }
        }).then(response =>
            response.headers['location'].split("/")[8]
        )
    })
}

export function getGroupByName(groupName) {
    return getToken().then(token => {
        cy.request({
            method: 'GET',
            url: baseUrl + '/auth/admin/realms/baby-yoda/groups/?search=' + groupName,
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
            // expect(response.status).to.eq(200);
            const groups = response.body;
            let gid = null
            groups.forEach(group => {
                if (group['name'] === groupName) {
                    gid = group['id'];
                }
            });
            return gid
        });
    });
}

export function removeGroupByName(groupName) {
    getGroupByName(groupName).then(gid => {
        if (gid != null) {
            getToken().then(token => {
                cy.request({
                    method: 'DELETE',
                    failOnStatusCode: false,
                    url: baseUrl + '/auth/admin/realms/baby-yoda/groups/' + gid,
                    headers: {
                        'Authorization': 'Bearer ' + token
                    },
                }).then(response => {
                    expect(response.status).to.eq(204);
                })
            })
        }
    })
}

export function removeGroupById(groupId) {
    return getToken().then(token => {
        cy.request({
            method: 'DELETE',
            failOnStatusCode: false,
            url: baseUrl + '/auth/admin/realms/baby-yoda/groups/' + groupId,
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
            expect(response.status).to.eq(204);
        })
    })
}

export function createTopLevelGroup(newGroupName) {
    return getToken().then(token => {
        cy.request({
            method: 'POST',
            failOnStatusCode: false,
            url: baseUrl + '/auth/admin/realms/baby-yoda/groups/',
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: {
                name: newGroupName,
            }
        }).then(response =>
            response.headers['location'].split("/")[8]
        )
    })
}

export function createSubGroup(parentGroupId, newGroupName) {
    return getToken().then(token => {
        cy.request({
            method: 'POST',
            failOnStatusCode: false,
            url: baseUrl + '/auth/admin/realms/baby-yoda/groups/' + parentGroupId + "/children",
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: {
                name: newGroupName,
            }
        }).then(response =>
            response.body["id"]
        )
    })
}

export function clearUserActions(userId) {
    getToken().then(token => {
        cy.request({
            method: 'PUT',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users/' + userId,
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: {
                requiredActions: []
            }
        }).then(response => {
            expect(response.status).to.eq(204)
        })
    })
}

export function removeUserFromGroup(userId, groupId) {
    getToken().then(token => {
        cy.request({
            method: 'DELETE',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users/' + userId + '/groups/' + groupId,
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
            expect(response.status).to.eq(204)
        })
    })
}

export function addUserToGroup(userId, groupId) {
    getToken().then(token => {
        cy.request({
            method: 'PUT',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users/' + userId + '/groups/' + groupId,
            headers: {
                'Authorization': 'Bearer ' + token
            },
        }).then(response => {
            expect(response.status).to.eq(204)
        })
    })
}

export function setUserPassword(userId, password) {
    getToken().then(token => {
        cy.request({
            method: 'PUT',
            url: baseUrl + '/auth/admin/realms/baby-yoda/users/' + userId + '/reset-password',
            headers: {
                'Authorization': 'Bearer ' + token
            },
            body: {
                type: "password",
                temporary: false,
                value: password,
            }
        }).then(response => {
            expect(response.status).to.eq(204)
        })
    })
}

export function addNewUsersToGroups(groups, userCount, password) {
    let i;
    let users = []
    for (i = 0; i < userCount; i++) {
        let username = "perfTestUser" + i;
        createUser(username, groups).then(uid => {
            clearUserActions(uid);
            setUserPassword(uid, password);
        });
        users.push(username);
    }
    return users;
}

export function populateUsersAndGroups(groupCount, usersPerGroupCount) {
    const password = "keycloak123!@3"
    const parentGroup = "PerfTestParentGroup";
    //clean up existing users and groups
    removeAllUsers();
    removeGroupByName(parentGroup);

    //create parent group
    createTopLevelGroup(parentGroup).then(parentGid => {
        let i;
        for (i = 0; i < groupCount; i++) {
            //create group
            let groupName = "group" + i;
            createSubGroup(parentGid, groupName).then(gid => {

                //create IL2 sub group
                createSubGroup(gid, "IL2").then(il2Gid => {
                    //create development sub group
                    createSubGroup(il2Gid, "development" ).then(developmentGid => {
                        //create developer sub group
                        createSubGroup(developmentGid, "developer").then(devGid => {
                            let j;
                            for (j = 0; j < usersPerGroupCount; j++) {
                                createUser(groupName + "_user" + j, [])
                                    .then(uid => {
                                        // cy.task('set', {key: developerUserId, value: uid})
                                        clearUserActions(uid);
                                        setUserPassword(uid, password);
                                        addUserToGroup(uid, devGid)
                                    });
                            }
                        })
                    })
                })
            })
        }
    })
}

