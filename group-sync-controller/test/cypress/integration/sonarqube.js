import * as kc from "./keycloak";
import * as cm from "./commons";

const adminUser = "admin"
const adminPass = "admin"
const baseUrl = "https://sonarqube.dev.code_dev_url.com"
const pageSize = "500"
const pageCount = 3

export function login(username, password) {
    cy.clearLocalStorage();
    cy.clearCookies();
    cy.visit(baseUrl + '/sessions/new?return_to=%2F');
    cy.wait(2000)
    cy.get('span').click();
    cy.url().should('contains', kc.baseUrl + '/auth/realms/baby-yoda/protocol/openid-connect/auth');
    // cy.url().should('contains', 'https://sonarqube.dev.code_dev_url.com/');
    // cy.visit('https://keycloak-admin.dev.code_dev_url.com/auth/realms/baby-yoda/protocol/openid-connect/auth?response_type=code&redirect_uri=https%3A%2F%2Fsonarqube.dev.code_dev_url.com%2Foauth2%2Fcallback%2Foidc&state=p2spbvean5s65b0a5u62cd4420&client_id=il2_00eb8904-5b88-4c68-ad67-cec0d2e07aa6_sonarqube&scope=openid');
    cy.get('#username').click();
    cy.get('#username').type(username);
    cy.get('#password').type(password);
    cy.get('#kc-login').click();
    cy.wait(1000)
}

export function getAllUsers() {
    let reqArray = [];
    for(let i = 1; i <= pageCount; i++) {
        const req = () => {
            return cy.request({
                method: 'GET',
                url: baseUrl + '/api/users/search?ps=' + pageSize + '&p=' + i,
                headers: {
                    'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
                }
            }).then(response => {
                expect(response.status).to.eq(200);
                return response.body['users']
            });
        }
        reqArray.push(req);
    }

    return cm.all(...reqArray).then((responses) => {
        let allUsers = [];
        responses.forEach(rsp => {
            allUsers.push(...rsp);
        })
        cy.log("Total users count = " + allUsers.length);
        return cy.wrap(allUsers);
    })
}

export function removeAllUsers() {
    getAllUsers().then(users => {
        users.forEach(u => {
            let login = u['login']
            //DO NOT remove admin user
            if (login === 'admin') {
                return
            }
            cy.request({
                method: 'POST',
                url: baseUrl + '/api/users/deactivate?login=' + login,
                headers: {
                    'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
                }
            }).then(res => {
                expect(res.status).to.eq(200)
            })
        })
    })
}

export function getAllGroups() {
    let reqArray = [];
    for(let i = 1; i <= pageCount; i++) {
        const req = () => {
            return cy.request({
                method: 'GET',
                url: baseUrl + '/api/user_groups/search?ps=' + pageSize + '&p=' + i,
                headers: {
                    'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
                }
            }).then(response => {
                expect(response.status).to.eq(200);
                return response.body['groups']
            });
        }
        reqArray.push(req);
    }

    return cm.all(...reqArray).then((responses) => {
        let allGroups = [];
        responses.forEach(rsp => {
            allGroups.push(...rsp);
        })
        cy.log("Total groups count = " + allGroups.length);
        return cy.wrap(allGroups);
    })
}

export function removeAllGroups() {
    getAllGroups().then(groups => {
        groups.forEach(g => {
            let groupName = g['name'];
            //DO NOT remove the default groups
            if (groupName === 'sonar-administrators' || groupName === 'sonar-users') {
                return;
            }
            cy.request({
                method: 'POST',
                url: baseUrl + '/api/user_groups/delete?name=' + groupName,
                headers: {
                    'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
                }
            }).then(res => {
                expect(res.status).to.eq(204);
            })
        })
    })
}



export function verifyUserInGroup(username, group) {
    cy.request({
        method: 'GET',
        url: baseUrl + '/api/users/search?ps=500&p=1&q=' + username,
        headers: {
            'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
        }
    }).then(response => {
        expect(response.status).to.eq(200);
        const users = response.body['users'];
        expect(users.length).to.equal(1);
        users.forEach(u => {
            expect(u['externalIdentity']).to.eq(username);
            expect(u['externalProvider']).to.eq('oidc');
            expect(u['active']).to.eq(true);
            expect(u['groups']).to.include.members([group])
        })
    })
}

export function verifyUserNotExist(username) {
    cy.request({
        method: 'GET',
        url: baseUrl + '/api/users/search?ps=' + pageSize + '&p=1&q=' + username,
        headers: {
            'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
        }
    }).then(response => {
        expect(response.status).to.eq(200)
        const users = response.body['users']
        expect(users).to.have.lengthOf(0)
    })
}

export function verifyUserNotInGroup(username, group) {
    cy.request({
        method: 'GET',
        url: baseUrl + '/api/users/search?ps=' + pageSize + '&p=1&q=' + username,
        headers: {
            'Authorization': 'Basic ' + btoa(adminUser + ":" + adminPass)
        }
    }).then(response => {
        expect(response.status).to.eq(200)
        const users = response.body['users']
        users.forEach(u => {
            expect(u['externalIdentity']).to.eq(username);
            expect(u['externalProvider']).to.eq('oidc');
            expect(u['active']).to.eq(true);
            expect(u['groups']).not.to.include.members([group])
        })
    })
}