import * as kc from "./keycloak";
import * as cm from "./commons";

const apiToken = "UZ8txs6SeEKx1DsD5YUb"
const baseUrl = "https://code_dev_url.com"


export function login(username, password) {
    it('Login ' + username + ' to Gitlab', () => {
        cy.visit(baseUrl);
        cy.wait(2000)
        cy.url().should('contains', kc.baseUrl + '/auth/realms/baby-yoda/protocol/openid-connect/auth');
        cy.get('#username').type(username);
        cy.get('#password').type(password);
        cy.get('#kc-login').click();
        // cy.get('#kc-form-login').submit();
        cy.wait(1000)
        cy.url().should('contains', baseUrl);
    });
}

export function getAllUsers() {
    return cy.request({
        method: 'GET',
        url: baseUrl + '/api/v4/users?per_page=100',
        headers: {
            'PRIVATE-TOKEN': apiToken
        }
    }).then(resp => {
        let pageCount = parseInt(resp.headers['x-total-pages']);
        let reqArray = [];

        for(let i = 1; i <= pageCount; i++) {
            const req = () => {
                return cy.request({
                    method: 'GET',
                    url: baseUrl + '/api/v4/users?per_page=100&page=' + i,
                    headers: {
                        'PRIVATE-TOKEN': apiToken
                    }
                }).then(response => {
                    expect(response.status).to.eq(200);
                    return response.body
                });
            }
            reqArray.push(req);
        }

        return cm.all(...reqArray).then((responses) => {
            let allUsers = [];
            responses.forEach(rsp => {
               allUsers.push(...rsp);
            })
            cy.log("Total users count = " + allUsers.length);
            return cy.wrap(allUsers);
        })
    })
}

export function getUsersCount() {
    return cy.request({
        method: 'GET',
        url: baseUrl + '/api/v4/users',
        headers: {
            'PRIVATE-TOKEN': apiToken
        }
    }).then(resp => {
        return parseInt(resp.headers["x-total"])
    })
}

export function getGroupsCount() {
    return cy.request({
        method: 'GET',
        url: baseUrl + '/api/v4/groups',
        headers: {
            'PRIVATE-TOKEN': apiToken
        }
    }).then(resp => {
        return parseInt(resp.headers["x-total"])
    })
}

export function removeAllUsers() {
    getAllUsers().then(users=> {
        users.forEach(u => {
            let username = u['username']
            //DO NOT remove admin user
            if (username === 'root' || username === 'ghost_user') {
                return
            }
            cy.log("removing user: " + username);
            let uid = u['id']
            cy.request({
                method: 'DELETE',
                url: baseUrl + '/api/v4/users/' + uid,
                headers: {
                    'PRIVATE-TOKEN': apiToken
                }
            }).then(res => {
                //404 could happen if group sync actually remove this user before we get to it
                expect(res.status).to.be.oneOf([404,204]);
            })
        })
    });
}

export function getAllGroups() {
    return cy.request({
        method: 'GET',
        url: baseUrl + '/api/v4/groups',
        headers: {
            'PRIVATE-TOKEN': apiToken
        }
    }).then(resp => {
        let pageCount = parseInt(resp.headers['x-total-pages']);
        let reqArray = [];

        for(let i = 1; i <= pageCount; i++) {
            const req = () => {
                return cy.request({
                    method: 'GET',
                    url: baseUrl + '/api/v4/groups?page=' + i,
                    headers: {
                        'PRIVATE-TOKEN': apiToken
                    }
                }).then(response => {
                    expect(response.status).to.eq(200);
                    return response.body;
                });
            }
            reqArray.push(req);
        }

        return all(...reqArray).then((responses) => {
            let allGroups = [];
            responses.forEach(rsp => {
                allGroups.push(...rsp);
            })
            cy.log("Total groups count = " + allGroups.length);
            return cy.wrap(allGroups);
        })
    })
}

export function removeAllGroups() {
    getAllGroups().then(groups => {
        groups.forEach(g => {
            cy.log("Removing group = " + g['name']);
            let gid = g['id'];
            cy.request({
                method: 'DELETE',
                failOnStatusCode: false,
                url: baseUrl + '/api/v4/groups/' + gid,
                headers: {
                    'PRIVATE-TOKEN': apiToken
                }
            }).then(res => {
                //404 could be returned, if this group's parent group is already deleted.
                expect(res.status).to.be.oneOf([404,202]);
            })
        })
    })
}

export function getAllMembersInGroup(gid) {
    return cy.request({
        method: 'GET',
        url: baseUrl + '/api/v4/groups/' + gid + '/members',
        headers: {
            'PRIVATE-TOKEN': apiToken
        }
    }).then(resp => {
        let pageCount = parseInt(resp.headers['x-total-pages']);
        let reqArray = [];

        for(let i = 1; i <= pageCount; i++) {
            const req = () => {
                return cy.request({
                    method: 'GET',
                    url: baseUrl + '/api/v4/groups/' + gid + '/members?page=' + i,
                    headers: {
                        'PRIVATE-TOKEN': apiToken
                    }
                }).then(response => {
                    expect(response.status).to.eq(200);
                    return response.body;
                });
            }
            reqArray.push(req);
        }

        return all(...reqArray).then((responses) => {
            let allMembers = [];
            responses.forEach(rsp => {
                allMembers.push(...rsp);
            })
            cy.log("Total groups count = " + allMembers.length);
            return cy.wrap(allMembers);
        })
    })
}

export function verifyUserInGroup(username, group) {
    getAllGroups().then(groups => {
        let foundGroup = groups.find(g => {
            let groupPath = g['full_name']
            groupPath = groupPath.split(" / ").map(function(item) {
                return item.trim()
            }).join("/")
            groupPath = "/" + groupPath;
            cy.log('path = ' + groupPath);
            return groupPath === group;
        });

        let gid = foundGroup['id'];
        getAllMembersInGroup(gid).then(members => {
            let set = new Set();
            members.map(item => set.add(item['username']));
            expect(set.has(username)).to.eq(true);
        });
    })
}


export function verifyUserNotInGroup(username, group) {
    getAllGroups().then(groups => {
        let foundGroup = groups.find(g => {
            let groupPath = g['full_name']
            groupPath = groupPath.split(" / ").map(function(item) {
                return item.trim()
            }).join("/")
            groupPath = "/" + groupPath;
            cy.log('path = ' + groupPath);
            return groupPath === group;
        });

        let gid = foundGroup['id'];
        getAllMembersInGroup(gid).then(members => {
            let set = new Set();
            members.map(item => set.add(item['username']));
            expect(set.has(username)).to.eq(false);
        });
    })
}

export function verifyUserNotExist(username) {
    getAllUsers().then(users => {
        let set = new Set();
        users.map(item => set.add(item['username']));
        expect(set.has(username)).to.eq(false);
    })
}
