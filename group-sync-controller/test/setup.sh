#!/bin/sh

set -ex
SONAR_URL=https://sonarqube.dev.code_dev_url.com
SONAR_ADMIN_USER=admin
SONAR_ADMIN_PASS=admin
SONAR_CLIENT_SECRET=131df552-877f-442a-9cbb-c38791b1cdb8
KEYCLOAK_URL=https://keycloak-admin.dev.code_dev_url.com

while [ $(curl -sw '%{http_code}' "$SONAR_URL/sessions/new" -o /dev/null) -ne 200 ]; do
  sleep 10;
done
#add curl to set Scops to "openid" only

curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.forceAuthentication -d value=true $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.enabled -d value=true $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.clientId.secured -d value=il2_00eb8904-5b88-4c68-ad67-cec0d2e07aa6_sonarqube $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.issuerUri -d value=$KEYCLOAK_URL/auth/realms/baby-yoda $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.clientSecret.secured -d value=$SONAR_CLIENT_SECRET $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.core.serverBaseURL -d value=$SONAR_URL $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.scopes -d value=openid $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.groupsSync -d value=false $SONAR_URL/api/settings/set
curl -u $SONAR_ADMIN_USER:$SONAR_ADMIN_PASS -d key=sonar.auth.oidc.loginButtonText -d value=SSO $SONAR_URL/api/settings/set