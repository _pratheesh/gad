How to run e2e test? 
1. Start up `dev-groupsync` t-rex dev branch pipeline
2. Run `setup.sh` to configure SSO for Sonarqube
3. Login to https://sonarqube.dev.code_dev_url.com as `admin` user. 
4. Create a access token under My Account -> Security -> Generate a new token
5. Update `SONAR_TOKEN` with the above token in apps/group-sync-app/dev/secrets/adapter/sonarqube-settings-enc.env under `dev-groupsync` branch
6. Push change up to remote branch to kick off the pipeline
7. Monitor T-REX pipeline. Delete the `group-sync` app for the new secret to take effect. After app is deleted, Argo will resync the app with the latest change. 
8. Run cypress test in `integration/test.js`