package main

import (
	"log"
	"strconv"
	"strings"

	utils "gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	"gitlab.com/_pratheesh/gad/group-sync-controller/controller"
)

func main() {
	adapterStr := utils.GetEnvOrExit("WORKLOAD_ADAPTERS")
	adapters := strings.Split(adapterStr, ",")
	adapterConfigs := make([]controller.AppAdapterConfig, 0)
	for _, adapterString := range adapters {
		adapterNameAndAddr := strings.Split(adapterString, "@")
		adapterConfig := controller.AppAdapterConfig{Name: adapterNameAndAddr[0], Address: adapterNameAndAddr[1]}
		adapterConfigs = append(adapterConfigs, adapterConfig)
	}

	developerAppsStr := utils.GetEnvOrExit("DEVELOPER_APPS")
	devAppsArray := strings.Split(developerAppsStr, ",")

	collaboratorAppsStr := utils.GetEnvOrExit("COLLABORATOR_APPS")
	collaboratorAppsArray := strings.Split(collaboratorAppsStr, ",")

	developerApps := *utils.NewStringHashSetFromArray(devAppsArray)
	collaboratorApps := *utils.NewStringHashSetFromArray(collaboratorAppsArray)
	tokenEndpoint := utils.GetEnvOrExit("KEYCLOAK_TOKEN_ENDPOINT")
	groupEndpoint := utils.GetEnvOrExit("KEYCLOAK_GROUP_ENDPOINT")
	apiUserName := utils.GetEnvOrExit("KEYCLOAK_API_USER_NAME")
	apiPassword := utils.GetEnvOrExit("KEYCLOAK_API_PASSWORD")

	config := controller.KeycloakConfig{TokenEndpoint: tokenEndpoint, GroupEndpoint: groupEndpoint, Username: apiUserName, Password: apiPassword}
	pollIntervalSecondsString := utils.GetEnvOrExit("KEYCLOAK_POLL_INTERVAL_SECONDS")
	pollIntervalSeconds, err := strconv.Atoi(pollIntervalSecondsString)
	if err != nil {
		log.Fatalf("Trying to get keycloak polling interval env variable. Unable to convert to int: %s", pollIntervalSecondsString)
	}
	certFile := utils.GetEnv("CA_CERT_FILE")
	controller.Start(config, pollIntervalSeconds, adapterConfigs, certFile, developerApps, collaboratorApps)
}
