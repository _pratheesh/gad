package commons

import (
	"os"
	"testing"
)

func TestUtils(t *testing.T) {
	os.Setenv("TESTENV", "testenv")
	result := GetEnv("TESTENV")
	if result != "testenv" {
		t.Errorf("Expect testenv to be returned. Found %v", result)
	}
	result = GetEnv("TESTENV_NOT_THERE")
	if result != "" {
		t.Errorf("Expect empty string to be returned. Found %v", result)
	}

	result = GetEnvOrExit("TESTENV")
	if result != "testenv" {
		t.Errorf("Expect testenv to be returned. Found %v", result)
	}
}