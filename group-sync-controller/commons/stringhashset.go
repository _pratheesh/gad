package commons

//StringHashSet a set for strings
type StringHashSet struct {
	items map[string]bool
}

//NewStringHashSet returns a new StringHashSet
func NewStringHashSet() *StringHashSet {
	return &StringHashSet{items: make(map[string]bool)}
}

//NewStringHashSetFromArray returns a new StringHashSet with the elements in the given array
func NewStringHashSetFromArray(elements []string) *StringHashSet {
	hashSet := StringHashSet{items: make(map[string]bool)}
	for _, element := range elements {
		hashSet.Add(element)
	}
	return &hashSet
}

//Add an new item to the StringHashSet
func (s *StringHashSet) Add(item string) {
	s.items[item] = true
}

//AddAll adds items to the StringHashSet
func (s *StringHashSet) AddAll(items ...string) {
	for _, item := range items {
		s.Add(item)
	}
}

//Remove item from StringHashSet
func (s *StringHashSet) Remove(items ...string) {
	for _, item := range items {
		delete(s.items, item)
	}
}

//Contains checks if all the items are in the the StringHashSet, false otherwise
func (s *StringHashSet) Contains(items ...string) bool {
	for _, item := range items {
		if _, contains := s.items[item]; !contains {
			return false
		}
	}
	return true
}

//ContainsAny return true if any of the items are in the StringHashSet
func (s *StringHashSet) ContainsAny(items ...string) bool {
	for _, item := range items {
		if _, contains := s.items[item]; contains {
			return true
		}
	}
	return false
}

//Size returns the number of items in the StringHashSet
func (s *StringHashSet) Size() int {
	return len(s.items)
}

//IsEmpty return true if the StringHashSet is empty, false otherwise
func (s *StringHashSet) IsEmpty() bool {
	return s.Size() == 0
}

//GetItemsNotContainedIn returns the items that are not in the given StringHashSet as a StringHashSet
func (s *StringHashSet) GetItemsNotContainedIn(o *StringHashSet) *StringHashSet {
	result := NewStringHashSet()
	for items := range s.items {
		if !o.Contains(items) {
			result.Add(items)
		}
	}
	return result
}

//Values returns the StringHashSet as an array
func (s *StringHashSet) Values() []string {
	values := make([]string, s.Size())
	idx := 0
	for item := range s.items {
		values[idx] = item
		idx++
	}
	return values
}

