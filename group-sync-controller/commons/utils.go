package commons

import (
	"log"
	"os"
)

//GetEnvOrExit returns the value for the given Environment Variable key. If the environment is not there a log.Fatal is called
func GetEnvOrExit(envKey string) string {
	value, ok := os.LookupEnv(envKey)
	if !ok {
		log.Fatalf("Environment variable: %s is required but missing", envKey)
	}
	return value
}

//GetEnv returns the value for the given Environment Variable key. If the environment is not, empty string is returned
func GetEnv(envKey string) string {
	value, ok := os.LookupEnv(envKey)
	if !ok {
		return ""
	}
	return value
}
