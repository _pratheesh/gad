package controller

import "gitlab.com/_pratheesh/gad/group-sync-controller/commons"

type keycloakCache struct {
	id             int
	groupsMap      map[string]keycloakGroup
	usersMap       map[string]keycloakUser
	userToRolesMap map[string]commons.StringHashSet
}

//returns a copy of all users in keycloak
func (s *keycloakCache) GetUsers() []keycloakUser {
	users := make([]keycloakUser, 0, len(s.usersMap))
	for _, user := range s.usersMap {
		users = append(users, user)
	}
	return users
}

func (s *keycloakCache) GetID() int {
	return s.id
}

//returns a copy of all groups in keycloak
func (s *keycloakCache) GetGroups() []keycloakGroup {
	groups := make([]keycloakGroup, 0, len(s.groupsMap))
	for _, group := range s.groupsMap {
		groups = append(groups, group)
	}
	return groups
}

func (s *keycloakCache) GetRolesForUser(username string) *commons.StringHashSet {
	roles, exists := s.userToRolesMap[username]
	if !exists {
		return commons.NewStringHashSet()
	}
	return &roles
}

//Given the group full path, returns a copy of all users in the group. nil if group doesn't exist.
func (s *keycloakCache) GetUsersInGroup(groupPath string) []keycloakUser {
	keycloakGroup, exists := s.groupsMap[groupPath]
	if exists {
		users := make([]keycloakUser, 0, len(keycloakGroup.Members))
		for _, groupMember := range keycloakGroup.Members {
			user, found := s.usersMap[groupMember.Username]
			if found {
				users = append(users, user)
			}
		}
		return users
	}
	return nil
}

func (s *keycloakCache) containsGroup(groupPath string) bool {
	_, exists := s.groupsMap[groupPath]
	return exists
}
