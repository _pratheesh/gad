package controller

import (
	"gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	as "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"context"
	"google.golang.org/grpc"
	"testing"
)

type client struct {
	t *testing.T
}

func (s *client) CreateUser(ctx context.Context, request *as.User, opts ...grpc.CallOption) (*as.User, error) {
	if request.Username != "user3" {
		s.t.Errorf("Expected to add user3 but found: %v", request)
	}
	return &as.User{
		Meta:      nil,
		Id:        "3",
		Username:  request.Username,
		Email:     request.Email,
		FirstName: request.FirstName,
		LastName:  request.LastName,
	}, nil
}

func (s *client) ListUsers(ctx context.Context, request *as.Generic, opts ...grpc.CallOption) (*as.Users, error) {
	return &as.Users{
		Meta: nil,
		Users: []*as.User{
			{
				Meta:      nil,
				Id:        "1",
				Username:  "user1",
				Email:     "user1@gmail.com",
				FirstName: "user1",
				LastName:  "user1",
			},
			{
				Meta:      nil,
				Id:        "4",
				Username:  "user4",
				Email:     "user4@gmail.com",
				FirstName: "user4",
				LastName:  "user4",
			},
		},
	}, nil
}

func (s *client) RemoveUser(ctx context.Context, request *as.User, opts ...grpc.CallOption) (*as.Generic, error) {
	if request.Username != "user4" {
		s.t.Errorf("Expected to add user4 but found: %v", request)
	}
	return &as.Generic{Meta: &as.Meta{
		RequestId: "id",
		Status:    200,
		Requester: "controller",
	}}, nil
}

func (s *client) CreateGroup(ctx context.Context, request *as.Group, opts ...grpc.CallOption) (*as.Group, error) {
	if request.Path != "/Dod/Platform1/Bravvo" {
		s.t.Errorf("Expected to create Bravvo group but found: %v", request)
	}
	return &as.Group{
		Meta:        nil,
		Id:          "BravvoGroupId",
		Name:        "Bravvo",
		Description: "Bravvo",
		Path:        "/Dod/Platform1/Bravvo",
	}, nil
}

func (s *client) ListGroups(ctx context.Context, request *as.Generic, opts ...grpc.CallOption) (*as.Groups, error) {
	return &as.Groups{
		Meta: nil,
		Groups: []*as.Group{
			{
				Meta:        nil,
				Id:          "TronGroupId",
				Name:        "Tron",
				Description: "Tron",
				Path:        "/Dod/Platform1/Tron",
			},
		},
	}, nil
}

func (s *client) GetGroupMembers(ctx context.Context, request *as.Group, opts ...grpc.CallOption) (*as.Users, error) {
	if request.Path == "/Dod/Platform1/Bravvo" {
		return &as.Users{
			Meta: nil,
			Users: []*as.User{
				{
					Meta:      nil,
					Id:        "1",
					Username:  "user1",
					Email:     "user1@gmail.com",
					FirstName: "user1",
					LastName:  "user1",
				},
			},
		}, nil
	}
	if request.Path == "/Dod/Platform1/Tron" {
		return &as.Users{
			Meta: nil,
			Users: []*as.User{
				{
					Meta:      nil,
					Id:        "1",
					Username:  "user1",
					Email:     "user1@gmail.com",
					FirstName: "user1",
					LastName:  "user1",
				},
			},
		}, nil
	}
	return nil, nil
}

func (s *client) AddUserToGroup(ctx context.Context, request *as.GroupMember, opts ...grpc.CallOption) (*as.Generic, error) {
	if request.GetUser().Username != "user3" {
		s.t.Errorf("Expected to add user3 to tron group but found: %v", request)
	}
	if request.GetGroup().Path != "/Dod/Platform1/Tron" {
		s.t.Errorf("Expected to add user3 to tron group but found: %v", request)
	}
	return &as.Generic{Meta: &as.Meta{
		RequestId: "id",
		Status:    200,
		Requester: "controller",
	}}, nil
}

func (s *client) RemoveUserFromGroup(ctx context.Context, request *as.GroupMember, opts ...grpc.CallOption) (*as.Generic, error) {
	if request.User.Username != "user1" {
		s.t.Errorf("Expected to remove user1 from Bravvo group but found: %v", request)
	}
	if request.Group.Path != "/Dod/Platform1/Bravvo" {
		s.t.Errorf("Expected to remove user1 from Bravvo group but found: %v", request)
	}
	return &as.Generic{Meta: &as.Meta{
		RequestId: "id",
		Status:    200,
		Requester: "controller",
	}}, nil
}

func TestSyncApp(t *testing.T) {
	adapterName := "gitlab"
	adapterLogger := &AdapterLogger{Name: adapterName}
	developerApps := commons.NewStringHashSetFromArray([]string{adapterName})
	collaboratorApps := commons.NewStringHashSetFromArray([]string{})

	usersMap := make(map[string]keycloakUser)
	usersMap["user1"] = keycloakUser{
		Username:  "user1",
		Email:     "user1@mgmail.com",
		FirstName: "user1",
		LastName:  "user1",
	}
	usersMap["user2"] = keycloakUser{
		Username:  "user2",
		Email:     "user2@mgmail.com",
		FirstName: "user2",
		LastName:  "user2",
	}
	usersMap["user3"] = keycloakUser{
		Username:  "user3",
		Email:     "user3@mgmail.com",
		FirstName: "user3",
		LastName:  "user3",
	}
	groupsMap := make(map[string]keycloakGroup)
	groupsMap["/Dod/Platform1/Tron"] = keycloakGroup{
		Name: "Tron",
		Path: "/Dod/Platform1/Tron",
		Members: []keycloakGroupMember{
			{"user1", []string{"developer"}},
			{"user3", []string{"developer"}},
		},
	}
	groupsMap["/Dod/Platform1/Bravvo"] = keycloakGroup{
		Name:    "Bravvo",
		Path:    "/Dod/Platform1/Bravvo",
		Members: []keycloakGroupMember{{"user2", []string{"collaborator"}}},
	}
	userToRoleMap := make(map[string]commons.StringHashSet)
	userToRoleMap["user1"] = *commons.NewStringHashSetFromArray([]string{"developer"})
	userToRoleMap["user2"] = *commons.NewStringHashSetFromArray([]string{"collaborator"})
	userToRoleMap["user3"] = *commons.NewStringHashSetFromArray([]string{"developer"})

	cache := &keycloakCache{
		id:             0,
		groupsMap:      groupsMap,
		usersMap:       usersMap,
		userToRolesMap: userToRoleMap,
	}
	client := &client{t}

	syncApp(adapterName, adapterLogger, cache, *developerApps, *collaboratorApps, client)
}
