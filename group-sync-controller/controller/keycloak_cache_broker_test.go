package controller

import (
	"reflect"
	"testing"
)

func TestCacheBroker(t *testing.T) {

	usersMap := make(map[string]keycloakUser)
	usersMap["user1"] = keycloakUser{
		Username:  "user1",
		Email:     "user1@mgmail.com",
		FirstName: "user1",
		LastName:  "user1",
	}
	usersMap["user2"] = keycloakUser{
		Username:  "user2",
		Email:     "user2@mgmail.com",
		FirstName: "user2",
		LastName:  "user2",
	}
	groupsMap := make(map[string]keycloakGroup)
	groupsMap["group1"] = keycloakGroup{
		Name:  "group1",
		Path:  "/Dod/Platform1/group1",
		Members: []keycloakGroupMember{{"User2", []string{"developer"}}},
	}
	groupsMap["group2"] = keycloakGroup{
		Name:  "group2",
		Path:  "/Dod/Platform1/group2",
		Members: []keycloakGroupMember{{"User2", []string{"collaborator"}}},
	}
	cache := keycloakCache{
		id:        0,
		groupsMap: groupsMap,
		usersMap:  usersMap,
	}

	cacheBroker := newKeycloakCacheBroker()
	go cacheBroker.start()
	chan1 := cacheBroker.Subscribe()
	chan2 := cacheBroker.Subscribe()
	cacheBroker.Publish(cache)
	keycloakCache1 := <-chan1
	keycloakCache2 := <-chan2
	if !reflect.DeepEqual(keycloakCache1, cache) {
		t.Errorf("cache not equal: %v %v", keycloakCache1, cache)
	}
	if !reflect.DeepEqual(keycloakCache2, cache) {
		t.Errorf("cache not equal: %v %v", keycloakCache2, cache)
	}
	//test cache is copied
	cache.id = 2
	if keycloakCache1.id != 0 {
		t.Errorf("cache not copied!: %v", keycloakCache1)
	}
}
