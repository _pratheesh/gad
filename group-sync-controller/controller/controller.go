package controller

import (
	"gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	"context"
	"crypto/rand"
	"fmt"
	"log"
	"time"

	as "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
)

const nameOfController = "GROUP_SYNC_CONTROLLER"

//AppAdapterConfig adapter config for the workload app
type AppAdapterConfig struct {
	Name    string
	Address string
}

//AdapterLogger logger for given adapter
type AdapterLogger struct {
	Name string
}

//Printf format print with adapter name prefix
func (a *AdapterLogger) Printf(format string, v ...interface{}) {
	log.Printf(a.Name+": "+format, v...)
}

//Println will print new line with adapter name prefix
func (a *AdapterLogger) Println(format string) {
	log.Println(a.Name + ": " + format)
}

//returns two list of users. First one is a list of keycloak users that's not in the app users list.
//Second one is a list of app users that's not in the keycloak users list.
func diffUsers(keycloakUsers []keycloakUser, appUsers *as.Users) ([]*as.User, []*as.User) {
	appUsersMap := make(map[string]*as.User, len(appUsers.GetUsers()))
	for _, x := range appUsers.GetUsers() {
		appUsersMap[x.Username] = x
	}

	usersToAdd := make([]*as.User, 0)
	for _, keycloakUser := range keycloakUsers {
		_, found := appUsersMap[keycloakUser.Username]
		if !found {
			usersToAdd = append(usersToAdd, &as.User{
				Username:  keycloakUser.Username,
				Email:     keycloakUser.Email,
				FirstName: keycloakUser.FirstName,
				LastName:  keycloakUser.LastName})
		} else {
			delete(appUsersMap, keycloakUser.Username)
		}
	}

	usersToRemove := make([]*as.User, 0)
	for _, value := range appUsersMap {
		usersToRemove = append(usersToRemove, value)
	}

	return usersToAdd, usersToRemove
}

//returns two list of groups. First list is groups to be added. Second list is groups to be removed.
func diffGroups(keycloakGroups []keycloakGroup, appGroups *as.Groups) ([]*as.Group, []*as.Group) {
	appGroupsMap := make(map[string]*as.Group, len(appGroups.GetGroups()))
	for _, x := range appGroups.GetGroups() {
		appGroupsMap[x.GetPath()] = x
	}

	groupsToAdd := make([]*as.Group, 0)
	for _, keycloakGroup := range keycloakGroups {
		_, found := appGroupsMap[keycloakGroup.Path]
		if !found {
			groupsToAdd = append(groupsToAdd, &as.Group{Name: keycloakGroup.Name, Path: keycloakGroup.Path})
		} else {
			delete(appGroupsMap, keycloakGroup.Path)
		}
	}

	groupsToRemove := make([]*as.Group, 0)
	for _, value := range appGroupsMap {
		groupsToRemove = append(groupsToRemove, value)
	}

	return groupsToAdd, groupsToRemove
}

func randToken() string {
	b := make([]byte, 8)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func newRequestMeta() *as.Meta {
	return &as.Meta{
		RequestId: randToken(),
		Status:    0,
		Requester: nameOfController,
	}
}

func getRolesWeCare(adapterName string, developerApps commons.StringHashSet, collaboratorApps commons.StringHashSet) *commons.StringHashSet {
	roles := commons.NewStringHashSet()
	if developerApps.Contains(adapterName) {
		roles.Add("developer")
	}
	if collaboratorApps.Contains(adapterName) {
		roles.Add("collaborator")
	}
	return roles
}

func syncApp(adapterName string, adapterLogger *AdapterLogger, keycloakCache *keycloakCache,
	developerApps commons.StringHashSet, collaboratorApps commons.StringHashSet, c as.AdapterClient) {

	keycloakUsers := keycloakCache.GetUsers()
	keycloakGroups := keycloakCache.GetGroups()
	adapterLogger.Printf("====== Processing Keycloak Cache ID: %d =====", keycloakCache.id)

	//filter out users that doesn't need to exist in this adapter
	rolesWeCare := getRolesWeCare(adapterName, developerApps, collaboratorApps)
	relevantUsers := make([]keycloakUser, 0)
	for _, keycloakUser := range keycloakUsers {
		setOfRoles := keycloakCache.GetRolesForUser(keycloakUser.Username)
		//if setOfRoles contains any of rolesWeCare, we keep it. Otherwise we filter it out.
		if rolesWeCare.ContainsAny(setOfRoles.Values()...) {
			relevantUsers = append(relevantUsers, keycloakUser)
		}
	}

	keycloakUsers = relevantUsers

	// sync the users
	usernameToUsersMap := make(map[string]*as.User)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Minute)
	defer cancel()
	appUsers, err := c.ListUsers(ctx, &as.Generic{Meta: newRequestMeta()})
	if err != nil {
		adapterLogger.Printf("Could not get users: %v", err)
		return
	}
	adapterLogger.Printf("# of users received: %d", len(appUsers.Users))
	for _, appUser := range appUsers.Users {
		usernameToUsersMap[appUser.Username] = appUser
	}
	usersToAdd, usersToRemove := diffUsers(keycloakUsers, appUsers)

	userToAdd(adapterLogger, usersToAdd, c, usernameToUsersMap)
	userToRemove(adapterLogger, usersToRemove, c)

	//sync the groups
	ctx, cancel = context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	appGroups, err := c.ListGroups(ctx, &as.Generic{Meta: newRequestMeta()})
	if err != nil {
		adapterLogger.Printf("Could not get groups: %v", err)
		return
	}
	adapterLogger.Printf("# of groups received: %d", len(appGroups.Groups))
	//preprocess the groups, retain only the team groups
	appTeamGroups := make([]*as.Group, 0)
	for _, appGroup := range appGroups.Groups {
		if keycloakCache.containsGroup(appGroup.GetPath()) {
			appTeamGroups = append(appTeamGroups, appGroup)
		}
	}
	appGroups = &as.Groups{Groups: appTeamGroups}
	groupsToAdd, _ := diffGroups(keycloakGroups, appGroups)
	finalAppGroups := append(make([]*as.Group, 0), appGroups.Groups...)
	finalAppGroups = addGroups(adapterLogger, groupsToAdd, c, finalAppGroups)

	//sync the group membership
	syncGroupMembers(adapterLogger, finalAppGroups, usernameToUsersMap, c, keycloakCache)
	adapterLogger.Println("All Done")
}

func syncGroupMembers(adapterLogger *AdapterLogger, finalAppGroups []*as.Group, usernameToUsersMap map[string]*as.User,
	c as.AdapterClient, keycloakCache *keycloakCache) {

	for _, appGroup := range finalAppGroups {
		appGroup.Meta = newRequestMeta()
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		adapterLogger.Printf("Processing group memberships for %s", appGroup.Path)
		appGroupMembers, err := c.GetGroupMembers(ctx, appGroup)
		if err != nil {
			adapterLogger.Printf("Error getting group members: %v", err)
		} else {
			keycloakGroupMembers := keycloakCache.GetUsersInGroup(appGroup.Path)
			if keycloakGroupMembers == nil {
				adapterLogger.Printf("No keycloak group exists for path: %s", appGroup.Path)
				continue
			}
			membersToAdd, membersToRemove := diffUsers(keycloakGroupMembers, appGroupMembers)
			addMembers(adapterLogger, membersToAdd, c, appGroup, usernameToUsersMap)
			removeMembers(adapterLogger, membersToRemove, c, appGroup)
		}
		adapterLogger.Printf("Done processing group memberships for %s", appGroup.Path)
	}
}

func addGroups(adapterLogger *AdapterLogger, groupsToAdd []*as.Group, c as.AdapterClient,
	finalAppGroups []*as.Group) []*as.Group {

	if len(groupsToAdd) != 0 {
		for _, groupToAdd := range groupsToAdd {
			groupToAdd.Meta = newRequestMeta()
			adapterLogger.Printf("Request to add new group: %v", groupToAdd)
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			newGroup, err := c.CreateGroup(ctx, groupToAdd)
			if err != nil {
				adapterLogger.Printf("Error creating groups: %v", err)
			} else {
				adapterLogger.Printf("Created groups: %v", newGroup)
				finalAppGroups = append(finalAppGroups, newGroup)
			}
		}
	}
	return finalAppGroups
}

func addMembers(adapterLogger *AdapterLogger, membersToAdd []*as.User, c as.AdapterClient, appGroup *as.Group, usernameToUsersMap map[string]*as.User) {
	if len(membersToAdd) != 0 {
		//add in user id
		for _, memberToAdd := range membersToAdd {
			username := memberToAdd.Username
			user, found := usernameToUsersMap[username]
			if found {
				adapterLogger.Printf("Adding member(%s) to group: %s", username, appGroup.Name)
				memberToAdd.Id = user.Id
				ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
				defer cancel()
				status, err := c.AddUserToGroup(ctx, &as.GroupMember{Meta: newRequestMeta(), Group: appGroup, User: memberToAdd})
				if err != nil {
					adapterLogger.Printf("error adding user to group: %v", err)
				} else {
					adapterLogger.Printf("Response from adding user to group: %v", status)
				}
			} else {
				adapterLogger.Printf("%s not found in the users map. Skipping", username)
			}
		}
	}
}

func removeMembers(adapterLogger *AdapterLogger, membersToRemove []*as.User, c as.AdapterClient, appGroup *as.Group) {
	if len(membersToRemove) != 0 {
		adapterLogger.Printf("Removing members(%v) to group:%s", membersToRemove, appGroup.Name)
		for _, memberToRemove := range membersToRemove {
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			status, err := c.RemoveUserFromGroup(ctx, &as.GroupMember{Meta: newRequestMeta(), Group: appGroup, User: memberToRemove})
			if err != nil {
				adapterLogger.Printf("error remove users: %v", err)
			} else {
				adapterLogger.Printf("Response from removing users from group: %v", status)
			}
		}
	}
}

func userToAdd(adapterLogger *AdapterLogger, usersToAdd []*as.User, c as.AdapterClient, usernameToUsersMap map[string]*as.User) {
	adapterLogger.Printf("# of Users to be created: %d", len(usersToAdd))
	if len(usersToAdd) != 0 {
		for _, userToAdd := range usersToAdd {
			userToAdd.Meta = newRequestMeta()
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			newUser, err := c.CreateUser(ctx, userToAdd)
			if err != nil {
				adapterLogger.Printf("Error creating users %s: %v", userToAdd, err)
			} else {
				adapterLogger.Printf("Created user: %v", newUser)
				usernameToUsersMap[newUser.Username] = newUser
			}
		}
	}
}

func userToRemove(adapterLogger *AdapterLogger, usersToRemove []*as.User, c as.AdapterClient) {
	adapterLogger.Printf("# of Users to be removed: %v", len(usersToRemove))
	if len(usersToRemove) != 0 {
		for _, userToRemove := range usersToRemove {
			userToRemove.Meta = newRequestMeta()
			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()
			status, err := c.RemoveUser(ctx, userToRemove)
			if err != nil {
				adapterLogger.Printf("Error remove users: %v", err)
			} else {
				adapterLogger.Printf("Response from removing user: %v", status)
			}
		}
	}
}
