package controller

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestKeycloakPoller(t *testing.T) {

	srv := serverMock()
	defer srv.Close()
	tokenEndpoint := srv.URL + "/tokenEndpoint"
	groupEndpoint := srv.URL + "/groupEndpoint"
	username := "username"
	password := "password"

	config := KeycloakConfig{TokenEndpoint: tokenEndpoint, GroupEndpoint: groupEndpoint, Username: username, Password: password}
	poller := newKeycloakPoller(config)

	if poller.config.Password != password || poller.config.Username != username ||
		poller.config.TokenEndpoint != tokenEndpoint || poller.config.GroupEndpoint != groupEndpoint {
		t.Errorf("Config not set correctly %v", poller.config)
	}

	token, err := poller.getAccessToken()
	if token != "blahblahblah" {
		t.Errorf("error in retrieving token %v", token)
	}
	if err != nil {
		t.Errorf("error in retrieving token, err should be nil %v", err)
	}

	keycloakGroups, err := poller.getGroups(token)
	users := keycloakGroups.Users
	groups := keycloakGroups.Groups

	if len(users) != 2 {
		t.Errorf("Number of users should be 2 %v", users)
	}

	if len(groups) != 2 {
		t.Errorf("Number of users should be 2 %v", groups)
	}

	cache := poller.poll()
	if len(cache.GetUsers()) != 2 {
		t.Errorf("Number of users should be 2 %v", users)
	}

	if len(cache.GetGroups()) != 2 {
		t.Errorf("Number of groups should be 2 %v", groups)
	}
	r := cache.GetRolesForUser("developer5")
	if !r.Contains("developer") {
		t.Errorf("Developer5 should have a developer role %v", r)
	}
	if r.Contains("collaborator") {
		t.Errorf("Developer5 should not have a collaborator role %v", r)
	}



}


func serverMock() *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/tokenEndpoint", tokenMock)
	handler.HandleFunc("/groupEndpoint", groupMock)


	srv := httptest.NewServer(handler)

	return srv
}
type testToken struct {
	AccessToken      string `json:"access_token"`
	ExpiresIn        int    `json:"expires_in"`
	RefreshExpiresIn int    `json:"refresh_expires_in"`
	RefreshToken     string `json:"refresh_token"`
	TokenType        string `json:"token_type"`
	NotBeforePolicy  int    `json:"not-before-policy"`
	SessionState     string `json:"session_state"`
	Scope            string `json:"scope"`
}


func tokenMock(w http.ResponseWriter, r *http.Request) {
	t := testToken{
		AccessToken: "blahblahblah",
		ExpiresIn: 900,
		RefreshExpiresIn: 10800,
		RefreshToken: "blahblah",
		TokenType: "bearer",
		NotBeforePolicy: 0,
		SessionState: "111111",
		Scope: "",

	}
	b,_ := json.Marshal(t)
	_, _ = w.Write(b)
}

func groupMock(w http.ResponseWriter, r *http.Request) {
	g := `{
    "groups": [
        {
            "name": "Puckboard",
            "path": "/Platform One/Tron/Puckboard",
            "users": [
                {
                    "username": "developer5",
                    "roles": [
                        "developer"
                    ]
                }
            ]
        },
        {
            "name": "Braavos",
            "path": "/Platform One/Iron Bank/Braavos",
            "users": [
                {
                    "username": "developer5",
                    "roles": [
                        "developer"
                    ]
                },
                {
                    "username": "collaborator5",
                    "roles": [
                        "collaborator"
                    ]
                }
            ]
        }
    ],
    "users": [
        {
            "username": "collaborator5",
            "email": "collaborator5@revacomm.com",
            "firstName": "collaborator5",
            "lastName": "collaborator5"
        },
        {
            "username": "developer5",
            "email": "developer5@revacomm.com",
            "firstName": "developer5",
            "lastName": "developer5"
        }
    ]
}`
	_, _ = w.Write([]byte(g))
}

