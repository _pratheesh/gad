package controller

import (
	"gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	as "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
	"os"
	"time"
)

func getTLSOption(certFile string) grpc.DialOption {
	//default is insecure for dev
	tls := grpc.WithInsecure()
	if certFile != "" {
		cred, err := credentials.NewClientTLSFromFile(certFile, "")
		if err != nil {
			log.Panicf("Error reading cert file(%s): %v", certFile, err)
		}
		tls = grpc.WithTransportCredentials(cred)
	}
	return tls
}

//Start up the controller
func Start(config KeycloakConfig, pollIntervalSeconds int,
	adapters []AppAdapterConfig, certFile string, developerApps commons.StringHashSet, collaboratorApps commons.StringHashSet) {
	log.Println("Using keycloak token endpoint: " + config.TokenEndpoint)
	log.Println("Using keycloak group endpoint: " + config.GroupEndpoint)
	log.Println("Using keycloak API user: " + config.Username)
	log.Printf("Keycloak polling interval in seconds: %d\n", pollIntervalSeconds)
	log.Printf("Using adapters list: %v\n", adapters)
	log.Printf("Developer apps : %+v\n", developerApps)
	log.Printf("Collaborator apps : %+v\n", collaboratorApps)
	log.Printf("Using TLS cert for adapters: %s\n", certFile)
	keycloakCacheBroker := newKeycloakCacheBroker()
	go keycloakCacheBroker.start()
	//keycloakCacheChannel := make(chan keycloakCache)
	for _, adapter := range adapters {
		msgCh := keycloakCacheBroker.Subscribe()

		//create one go routine per adapter.
		go func(adapterConfig AppAdapterConfig) {
			for {
				select {
				//when there is new keycloak cache, sync them via adapter
				case keycloakCache := <-msgCh:
					adapterLogger := &AdapterLogger{Name: adapterConfig.Name}
					tls := getTLSOption(certFile)
					conn, err := grpc.Dial(adapterConfig.Address, tls, grpc.WithTimeout(10*time.Second))
					if err != nil {
						adapterLogger.Printf("Error connecting to adapter: %v", err)
						return
					}
					defer conn.Close()
					client := as.NewAdapterClient(conn)
					syncApp(adapterConfig.Name, adapterLogger, &keycloakCache, developerApps, collaboratorApps, client)
				}
			}
		}(adapter)
	}
	keycloakCacheService := newKeycloakPoller(config)

	ticker := time.NewTicker(time.Duration(pollIntervalSeconds) * time.Second)
	go func() {
		for ; true; <-ticker.C {
			//poll keycloak API and push result into channel
			keycloakCache := keycloakCacheService.poll()
			if keycloakCache != nil {
				keycloakCacheBroker.Publish(*keycloakCache)
			}
		}
	}()

	//never ending wait
	exit := make(chan string)
	for {
		select {
		case <-exit:
			os.Exit(0)
		}
	}
}
