package controller

import (
	"gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	as "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"testing"
)

func TestDiffUser(t *testing.T) {
	keycloakUsers := make([]keycloakUser, 0)
	keycloakUsers = append(keycloakUsers, keycloakUser{
		Username:  "user1",
		Email:     "user1@gmail.com",
		FirstName: "user1",
		LastName:  "user1",
	})
	keycloakUsers = append(keycloakUsers, keycloakUser{
		Username:  "user2",
		Email:     "user2@gmail.com",
		FirstName: "user2",
		LastName:  "user2",
	})
	appUsers := make([]*as.User, 0)
	appUsers = append(appUsers, &as.User{
		Meta:      nil,
		Id:        "0",
		Username:  "user1",
		Email:     "user1@gmail.com",
		FirstName: "user1",
		LastName:  "user1",
	})
	appUsers = append(appUsers, &as.User{
		Meta:      nil,
		Id:        "1",
		Username:  "user3",
		Email:     "user3@gmail.com",
		FirstName: "user3",
		LastName:  "user3",
	})
	usersFromApp := &as.Users{
		Meta:  nil,
		Users: appUsers,
	}
	usersToAdd, usersToRemove := diffUsers(keycloakUsers, usersFromApp)
	if len(usersToAdd) != 1 {
		t.Errorf("Only one user needs to be added. Found: %v", usersToAdd)
	} else {
		if usersToAdd[0].Username != "user2" {
			t.Errorf("Expect user2 only. Found: %v", usersToAdd)
		}
	}
	if len(usersToRemove) != 1 {
		t.Errorf("Only one user needs to be removed. Found: %v", usersToRemove)
	} else {
		if usersToRemove[0].Username != "user3" {
			t.Errorf("Expect user3 only. Found: %v", usersToRemove)
		}
	}
}

func TestDiffGroups(t *testing.T) {
	keycloakGroups := make([]keycloakGroup, 0)
	keycloakGroups = append(keycloakGroups, keycloakGroup{
		Name:    "Group1",
		Path:    "/Dod/Platform1/Group1",
		Members: []keycloakGroupMember{{"User1", []string{"developer"}}},
	})
	keycloakGroups = append(keycloakGroups, keycloakGroup{
		Name:    "Group2",
		Path:    "/Dod/Platform1/Group2",
		Members: []keycloakGroupMember{{"User2", []string{"developer"}}},
	})
	appGroups := make([]*as.Group, 0)
	appGroups = append(appGroups, &as.Group{
		Meta:        nil,
		Id:          "0",
		Name:        "Group1",
		Description: "Group1",
		Path:        "/Dod/Platform1/Group1",
	})
	appGroups = append(appGroups, &as.Group{
		Meta:        nil,
		Id:          "1",
		Name:        "Group3",
		Description: "Group3",
		Path:        "/Dod/Platform1/Group3",
	})
	groupsFromApp := &as.Groups{
		Meta:   nil,
		Groups: appGroups,
	}

	groupsToAdd, groupsToRemove := diffGroups(keycloakGroups, groupsFromApp)
	if len(groupsToAdd) != 1 {
		t.Errorf("Only one group needs to be added. Found: %v", groupsToAdd)
	} else {
		if groupsToAdd[0].Name != "Group2" {
			t.Errorf("Expect Group2 only. Found: %v", groupsToAdd)
		}
	}
	if len(groupsToRemove) != 1 {
		t.Errorf("Only one group needs to be removed. Found: %v", groupsToRemove)
	} else {
		if groupsToRemove[0].Name != "Group3" {
			t.Errorf("Expect Group3 only. Found: %v", groupsToRemove)
		}
	}
}

func TestAdapterConfig(t *testing.T) {
	adapterConfig := &AppAdapterConfig{
		Name:    "gitlab",
		Address: "localhost:50001",
	}

	if adapterConfig.Name != "gitlab" {
		t.Errorf("Expected gitlab, but found: %s", adapterConfig.Name)
	}
	if adapterConfig.Address != "localhost:50001" {
		t.Errorf("Expected localhost:50001, but found: %s", adapterConfig.Address)
	}
}

func TestRandomToken(t *testing.T) {
	token := randToken()
	if len(token) == 0 {
		t.Error("Token should not be empty.")
	}
}

func TestMetaData(t *testing.T) {
	meta := newRequestMeta()
	if len(meta.RequestId) == 0 {
		t.Error("Request id should not be empty")
	}
	if meta.Status != 0 {
		t.Errorf("Request status should be 0, but found %v", meta.Status)
	}
	if meta.Requester != nameOfController {
		t.Errorf("Requester is wrong, found %v", meta.Requester)
	}
}

func TestGetRolesWeCare(t *testing.T) {
	gitlabApp := "gitlab"
	sonarqubeApp := "sonarqube"
	devRole := "developer"
	colRole := "collaborator"
	developerApps := *commons.NewStringHashSetFromArray([]string{gitlabApp, sonarqubeApp})
	collaboratorApps := *commons.NewStringHashSetFromArray([]string{gitlabApp})
	roles := getRolesWeCare(gitlabApp, developerApps, collaboratorApps)
	if roles.Size() != 2 {
		t.Errorf("Expected only 1 role, but found %d", roles.Size())
	}
	if !roles.Contains(devRole, colRole) {
		t.Errorf("Expected developer and collaborator role for gitlab, found %v", roles)
	}
	roles = getRolesWeCare(sonarqubeApp, developerApps, collaboratorApps)
	if roles.Size() != 1 {
		t.Errorf("Expected only 1 role, but found %d", roles.Size())
	}
	if !roles.Contains(devRole) {
		t.Errorf("Expected only developer role for sonarqube, found %v", roles)
	}
}

func TestTlsOption(t *testing.T) {
	certFile := ""
	tls := getTLSOption(certFile)
	if tls == nil {
		t.Errorf("Expected tls to be not nil, but found %v", tls)
	}
}
