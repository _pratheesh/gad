package controller

import (
	"gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	"testing"
)

func TestGroupCache(t *testing.T) {
	usersMap := make(map[string]keycloakUser)
	usersMap["user1"] = keycloakUser{
		Username:  "user1",
		Email:     "user1@mgmail.com",
		FirstName: "user1",
		LastName:  "user1",
	}
	usersMap["user2"] = keycloakUser{
		Username:  "user2",
		Email:     "user2@mgmail.com",
		FirstName: "user2",
		LastName:  "user2",
	}
	groupsMap := make(map[string]keycloakGroup)
	groupsMap["/Dod/Platform1/group1"] = keycloakGroup{
		Name:    "group1",
		Path:    "/Dod/Platform1/group1",
		Members: []keycloakGroupMember{{"user1", []string{"developer"}}},
	}
	groupsMap["/Dod/Platform1/group2"] = keycloakGroup{
		Name:    "group2",
		Path:    "/Dod/Platform1/group2",
		Members: []keycloakGroupMember{{"user2", []string{"collaborator"}}},
	}
	userToRoleMap := make(map[string]commons.StringHashSet)
	userToRoleMap["user1"] = *commons.NewStringHashSetFromArray([]string{"developer"})
	userToRoleMap["user2"] = *commons.NewStringHashSetFromArray([]string{"collaborator"})

	cache := keycloakCache{
		id:             0,
		groupsMap:      groupsMap,
		usersMap:       usersMap,
		userToRolesMap: userToRoleMap,
	}

	rolesForUser := cache.GetRolesForUser("user1")
	if rolesForUser.Size() != 1 {
		t.Errorf("Only expect 1 role for user1. Found: %v", cache)
	}
	if rolesForUser.Contains("developer") != true {
		t.Errorf("developer role not found in user1. Found: %v", cache)
	}

	userRoles := cache.GetRolesForUser("user222")
	if userRoles.Size() != 0 {
		t.Errorf("Only expect 0 role for no existing user. Found: %v", userRoles)
	}

	usersInGroup1 := cache.GetUsersInGroup("/Dod/Platform1/group1")
	if len(usersInGroup1) != 1 {
		t.Errorf("Only expect 1 user in group 1. Found: %v", cache)
	}
	if usersInGroup1[0].Username != "user1" {
		t.Errorf("Only user1 is expected. Found: %v", cache)
	}
	users := cache.GetUsers()
	if len(users) != 2 {
		t.Errorf("Only expect 2 users total. Found: %v", cache)
	}
	if !cache.containsGroup("/Dod/Platform1/group1") {
		t.Errorf("Should contain group1. Found: %v", cache)
	}
	if !cache.containsGroup("/Dod/Platform1/group2") {
		t.Errorf("Should contain group2. Found: %v", cache)
	}
	if len(cache.GetGroups()) != 2 {
		t.Errorf("Should have only 2 groups total. Found: %v", cache)
	}
	if cache.GetID() != 0 {
		t.Errorf("Expect id 0. Found: %v", cache)
	}
}
