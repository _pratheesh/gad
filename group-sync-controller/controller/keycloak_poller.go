package controller

import (
	"gitlab.com/_pratheesh/gad/group-sync-controller/commons"
	"crypto/tls"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type keycloakUser struct {
	Username  string `json:"username"`
	Email     string `json:"email"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

type keycloakGroup struct {
	Name    string                `json:"name"`
	Path    string                `json:"path"` //unique identifier for group
	Members []keycloakGroupMember `json:"users"`
}

type keycloakGroupMember struct {
	Username string   `json:"username"`
	Roles    []string `json:"roles"`
}

type keycloakGroups struct {
	Groups []keycloakGroup `json:"groups"`
	Users  []keycloakUser  `json:"users"`
}

type keycloakPoller struct {
	cacheID int
	config  KeycloakConfig
}

//KeycloakConfig struct for endpoint and credentials
type KeycloakConfig struct {
	TokenEndpoint string
	GroupEndpoint string
	Username      string
	Password      string
}

func newKeycloakPoller(config KeycloakConfig) *keycloakPoller {
	keycloakPoller := &keycloakPoller{
		cacheID: 0,
		config:  config,
	}
	return keycloakPoller
}

func (s *keycloakPoller) poll() *keycloakCache {
	token, err := s.getAccessToken()
	if err != nil {
		log.Printf("Error getting keycloak access token: %v.", err)
		return nil
	}
	keycloakGroups, err := s.getGroups(token)
	if err != nil {
		log.Printf("Error getting keycloak groups: %v. Cache not updated.", err)
		return nil
	}
	groupsMap := make(map[string]keycloakGroup)
	usersMap := make(map[string]keycloakUser)
	userToRolesMap := make(map[string]commons.StringHashSet)
	for _, keycloakUser := range keycloakGroups.Users {
		usersMap[keycloakUser.Username] = keycloakUser
	}
	for _, keycloakGroup := range keycloakGroups.Groups {
		groupsMap[keycloakGroup.Path] = keycloakGroup
		for _, groupMember := range keycloakGroup.Members {
			roles, exist := userToRolesMap[groupMember.Username]
			if !exist {
				roles = *commons.NewStringHashSet()
				userToRolesMap[groupMember.Username] = roles
			}
			roles.AddAll(groupMember.Roles...)
		}
	}
	s.cacheID++
	return &keycloakCache{
		id:             s.cacheID,
		groupsMap:      groupsMap,
		usersMap:       usersMap,
		userToRolesMap: userToRolesMap,
	}
}

func (s *keycloakPoller) getAccessToken() (string, error) {
	//set InsecureSkipVerify to true to allow self-signed certificate
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	resp, err := client.PostForm(s.config.TokenEndpoint,
		url.Values{"username": {s.config.Username}, "password": {s.config.Password}, "grant_type": {"password"}, "client_id": {"admin-cli"}})
	if err != nil {
		log.Printf("Error getting token: %v", err)
		return "", err
	}
	if resp.StatusCode != 200 {
		return "", errors.New(resp.Status)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	var data map[string]interface{}
	err = json.Unmarshal([]byte(body), &data)
	if err != nil {
		return "", err
	}
	return data["access_token"].(string), nil
}

func (s *keycloakPoller) getGroups(token string) (*keycloakGroups, error) {
	log.Println("Requesting for keycloak group...")
	// Create a Bearer string by appending string access token
	var bearer = "Bearer " + token

	// Create a new request using http
	req, err := http.NewRequest("GET", s.config.GroupEndpoint, nil)

	// add authorization header to the req
	req.Header.Add("Authorization", bearer)

	// Send req using http Client

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("Error on keycloak response: %v.\n", err)
		return nil, err
	}
	if resp.StatusCode != 200 {
		log.Printf("Error calling keycloak api: %v\n", resp.Status)
		return nil, errors.New(resp.Status)
	}

	var groups keycloakGroups
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &groups)
	if err != nil {
		log.Printf("Error unmarshal keycloak obj: %v\n", err)
		return nil, err
	}
	log.Printf("Received keyloak groups count: %d\n", len(groups.Groups))

	defer resp.Body.Close()
	return &groups, nil
}
