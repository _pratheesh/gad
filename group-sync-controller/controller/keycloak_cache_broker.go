package controller

type keycloakCacheBroker struct {
	stopCh    chan struct{}
	publishCh chan keycloakCache
	subCh     chan chan keycloakCache
	unsubCh   chan chan keycloakCache
}

func newKeycloakCacheBroker() *keycloakCacheBroker {
	return &keycloakCacheBroker{
		stopCh:    make(chan struct{}),
		publishCh: make(chan keycloakCache, 1),
		subCh:     make(chan chan keycloakCache, 1),
		unsubCh:   make(chan chan keycloakCache, 1),
	}
}

//start the broker
func (b *keycloakCacheBroker) start() {
	subs := map[chan keycloakCache]struct{}{}
	for {
		select {
		case <-b.stopCh:
			return
		case msgCh := <-b.subCh:
			subs[msgCh] = struct{}{}
		case msgCh := <-b.unsubCh:
			delete(subs, msgCh)
		case msg := <-b.publishCh:
			for msgCh := range subs {
				// msgCh is buffered, use non-blocking send to protect the broker:
				select {
				case msgCh <- msg:
				default:
				}
			}
		}
	}
}

func (b *keycloakCacheBroker) Stop() {
	close(b.stopCh)
}

func (b *keycloakCacheBroker) Subscribe() chan keycloakCache {
	msgCh := make(chan keycloakCache, 1)
	b.subCh <- msgCh
	return msgCh
}

func (b *keycloakCacheBroker) Unsubscribe(msgCh chan keycloakCache) {
	b.unsubCh <- msgCh
}

func (b *keycloakCacheBroker) Publish(msg keycloakCache) {
	b.publishCh <- msg
}
