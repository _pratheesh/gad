package adapter

import (
	pb "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"context"
	"encoding/json"
	"gitlab.com/bloom42/libs/rz-go"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

const (
	token = "blahblah"
	pageSize = "500"
)

var user1 = &pb.User{
	Meta: &pb.Meta{
		RequestId: "1",
		Status:    200,
		Requester: "requester",
	},
	Id:        "2",
	Username:  "test1",
	Email:     "test1@test.com",
	FirstName: "test1FirstName",
	LastName:  "test1LastName",
}

var user2 = &pb.User{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
	Id:        "3",
	Username:  "test2",
	Email:     "test2@test.com",
	FirstName: "test2FirstName",
	LastName:  "test2LastName",
}

var generic = &pb.Generic{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},

}

var group1 = &pb.Group{
	Meta: &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	},
	Id:          "",
	Name:        "Tron",
	Description: "Tron",
	Path:        "/Platform One/Tron",
}

type createGroupRequest struct {
	Name string 		`json:"name"`
	ParentID string    `json:"parent_id"`
}

func TestGitlabAdapter(t *testing.T) {


	ue := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			testCreateUserRequest(w, r, t)
		case "GET":
			testListUsersRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	ru := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			testRemoveUserRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	cg := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			mockCreateGroupRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	lg := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "GET":
			testListGroupsRequest(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}

	})

	gm := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "GET":
			mockGetGroupMembersRequest(w, r, t)
		default:
			t.Errorf("error invalid group request, %v", r.Method)
		}
	})

	agm := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			mockAddUserToGroupRequest(w, r, t)
		default:
			t.Errorf("error invalid add user to group request, %v", r.Method)
		}
	})

	rgm := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		switch r.Method {
		case "POST":
			mockRemoveUserFromGroup(w, r, t)
		default:
			t.Errorf("error invalid user request, %v", r.Method)
		}
	})



	handler := http.NewServeMux()
	handler.HandleFunc("/api/users/search", ue)
	handler.HandleFunc("/api/users/deactivate", ru)
	handler.HandleFunc("/api/user_groups/create", cg)
	handler.HandleFunc("/api/user_groups/search", lg)
	handler.HandleFunc("/api/user_groups/users", gm)
	handler.HandleFunc("/api/user_groups/add_user", agm)
	handler.HandleFunc("/api/user_groups/remove_user", rgm)

	srv := httptest.NewServer(handler)
	defer srv.Close()

	os.Setenv("SONAR_URL", srv.URL)
	os.Setenv("SONAR_TOKEN", token)
	os.Setenv("API_RESPONSE_PAGE_SIZE", pageSize)


	adapter := SonarqubeAdapter{}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	log := rz.FromCtx(ctx)
	adapter.Startup(log, []string{"admin"})

	testCreateUser(ctx, adapter, t)
	testListUsers(ctx, adapter, t)
	testRemoveUser(ctx, adapter, t)

	testListGroups(ctx, adapter, t)
	testCreateGroup(ctx, adapter, t)

	testGetGroupMembers(ctx, adapter, t)

	testAddUserToGroup(ctx, adapter, t)

	testRemoveUserFromGroup(ctx, adapter, t)


}

func testCreateUser(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	_, err := adapter.CreateUser(&ctx, user1)
	if err == nil {
		t.Errorf("error, create user should return an error %v", err)
	}
}

func testCreateUserRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {

	t.Errorf("error, create user should not be called %v", r)

	resp :=`{
    "user": {
        "login": "test1",
        "name": "test1FirstName test1LastName",
        "email": "test1@test.com",
        "scmAccounts": [],
        "active": true,
        "local": false
    }
}`

	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write([]byte(resp))
}

func testRemoveUser(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	g, err := adapter.RemoveUser(&ctx, user1)
	if err != nil {
		t.Errorf("error in removing user %v", err)
	} else {
		validateGenericResponse(g, user1, t)
	}

}

func testRemoveUserRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateAuth(r, t)
	validateQueryParam(r,"login", user1.Id, t)


	resp :=`{
    "user": {
        "login": "test1",
        "name": "test1FirstName test1LastName",
        "active": false,
        "local": false,
        "externalIdentity": "test1",
        "externalProvider": "oidc",
        "groups": [],
        "scmAccounts": []
    }
}`

	b,_ := json.Marshal(resp)
	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(b)
}

func testListUsers(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	us, err := adapter.ListUsers(&ctx, generic)
	if err != nil {
		t.Errorf("error in listing users %v", err)
	} else {
		if len(us.GetUsers()) != 2 {
			t.Errorf("error in number users %v", us.GetUsers())
		}

		for _, u := range us.GetUsers() {
			if u.GetUsername() == user1.GetUsername() {
				validateUserResponse(u, user1, t)
			} else {
				validateUserResponse(u, user2, t)
			}
		}
	}

}

func testListUsersRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	validateAuth(r, t)
	validateQueryParam(r, "ps", pageSize, t)
	p := r.URL.Query().Get("p")
	resp := `{
    "paging": {
        "pageIndex": 1,
        "pageSize": 500,
        "total": 3
    },
	"users": [] }`

	if p == "1" {

		resp = `{
    "paging": {
        "pageIndex": 1,
        "pageSize": 500,
        "total": 3
    },
    "users": [
        {
            "login": "admin",
            "name": "Administrator",
            "active": true,
            "groups": [
                "sonar-administrators",
                "sonar-users"
            ],
            "tokensCount": 2,
            "local": true,
            "externalIdentity": "admin",
            "externalProvider": "sonarqube",
            "lastConnectionDate": "2020-12-18T20:24:12+0000"
        },
        {
            "login": "test1",
            "name": "test1FirstName test1LastName",
            "active": true,
            "email": "test1@test.com",
            "groups": [
                "sonar-users"
            ],
            "tokensCount": 0,
            "local": false,
            "externalIdentity": "test1",
            "externalProvider": "oidc",
            "avatar": "363902d72e0828f58f6fc6950ab39d44",
            "lastConnectionDate": "2020-12-18T20:56:55+0000"
        },
        {
            "login": "test2",
            "name": "test2FirstName test2LastName",
            "active": true,
            "email": "test2@test.com",
            "groups": [
                "sonar-users"
            ],
            "tokensCount": 0,
            "local": false,
            "externalIdentity": "test2",
            "externalProvider": "oidc",
            "avatar": "f1db9f0b5e7b11f9f55af5995e386d83",
            "lastConnectionDate": "2020-12-18T20:59:01+0000"
        }
    ]
}`
	}

	_, _ = w.Write([]byte(resp))
}

func testCreateGroup(ctx context.Context, adapter SonarqubeAdapter, t *testing.T) {

	gs, err := adapter.CreateGroup(&ctx, group1)
	if err != nil {
		t.Errorf("error in create group %v", err)
		t.Fail()
	}

	if gs.GetName() != group1.GetPath(){
		t.Errorf("Error in create group, name not set correctly, should equal path %s", gs.GetName())
		t.Fail()
	}

	if gs.GetDescription() != group1.GetDescription() {
		t.Errorf("Error in create group, description not set correctly  %s", gs.GetDescription())
		t.Fail()
	}

}

func mockCreateGroupRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {

	validateRequestMethod(r, "POST", t)
	gname := r.URL.Query().Get("name")
	description := r.URL.Query().Get("description")

	if gname != group1.GetPath() {
		t.Errorf("error in creating group, wrong group name %v", gname)
	}
	if description != group1.GetDescription() {
		t.Errorf("error in creating group, wrong description: %v", description)
	}

	resp := `{
    "group": {
        "id": "AXZ3tODWO-W0NAWr5gkx",
        "organization": "default-organization",
        "name": "/Platform One/Tron",
        "description": "Tron",
        "membersCount": 0,
        "default": false
    }
}`

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write([]byte(resp))
}


func testListGroups(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	gs, err := adapter.ListGroups(&ctx, generic)
	if err != nil {
		t.Errorf("error in listing groups %v", err)
	} else {
		if len(gs.GetGroups()) != 4 {
			t.Errorf("error in number groups %v", gs.GetGroups())
			t.Fail()
		}
		for _, g := range gs.GetGroups() {

			switch g.GetName() {
			case "/Platform One/Tron":

			case "/Platform Two/Bizzaro":

			case "sonar-users":

			case "sonar-administrators":

			default:
				t.Errorf("Unexpected group, found: %s", g)
			}

		}
	}

}

func testListGroupsRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	validateAuth(r, t)
	validateQueryParam(r, "ps", pageSize, t)
	p := r.URL.Query().Get("p")

	resp := `{
    "paging": {
        "pageIndex": 2,
        "pageSize": 500,
        "total": 124
    },
    "groups": []
}`
	if p == "1" {

		resp = `{
    "paging": {
        "pageIndex": 1,
        "pageSize": 500,
        "total": 4
    },
    "groups": [
        {
            "id": "AXZ4ph3UnVGBV5lMmWbv",
            "name": "/Platform Two/Bizzaro",
            "description": "",
            "membersCount": 0,
            "default": false
        },        
        {
            "id": "AXZ4pwgenVGBV5lMmWdd",
            "name": "/Platform One/Tron",
            "description": "",
            "membersCount": 0,
            "default": false
        },
        {
            "id": "AXZ4V6gXnVGBV5lMmMNv",
            "name": "sonar-administrators",
            "description": "System administrators",
            "membersCount": 1,
            "default": false
        },
        {
            "id": "AXZ4V6gYnVGBV5lMmMNw",
            "name": "sonar-users",
            "description": "Any new users created will automatically join this group",
            "membersCount": 1,
            "default": true
        }
    ]
}`
	}
	_, _ = w.Write([]byte(resp))
}

func mockGetGroupMembersRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "GET", t)
	validateAuth(r, t)
	validateQueryParam(r, "ps", pageSize, t)
	p := r.URL.Query().Get("p")

	resp := `{
    "users": [],
    "p": 2,
    "ps": 500,
    "total": 2
}`
	if p == "1" {
		resp = `{
			"users": [
		{
			"login": "admin",
			"name": "Administrator",
			"selected": true
		},
			{
				"login": "test1",
				"name": "test1FirstName test1LastName",
				"selected": true
			},
			{
				"login": "test2",
				"name": "test2FirstName test2LastName",
				"selected": true
			}
	],
		"p": 1,
		"ps": 500,
		"total": 3
	}`
	}

	_, _ = w.Write([]byte(resp))
}

func testGetGroupMembers(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	gm, err := adapter.GetGroupMembers(&ctx, &pb.Group{
		Meta:    &pb.Meta{
			RequestId: "2",
			Status:    200,
			Requester: "requester",
		},
		Id:          "6",
		Name:        "/Platform One/Tron",
		Description: "Tron",
		Path:        "/Platform One/Tron",
	})

	if err != nil {
		t.Errorf("error in listing groups members: %v", err)
		t.Fail()
	}
	if len(gm.GetUsers()) != 2 {
		t.Errorf("Found more than 2 users: %v", gm.GetUsers())
		t.Fail()
	}
	/*for _, user := range gm.GetUsers() {

	}
	*/

}

func mockAddUserToGroupRequest(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateAuth(r, t)

	validateQueryParam(r,"login", "test10", t)
	validateQueryParam(r,"name", "/Platform One/Tron", t)
	w.WriteHeader(http.StatusNoContent)
}

func testAddUserToGroup(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	meta := &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	}
	resp, err := adapter.AddUserToGroup(&ctx, &pb.GroupMember{
		Meta: meta,
		Group: &pb.Group{
			Meta:        meta,
			Id:          "6",
			Name:        "Tron",
			Description: "",
			Path:        "/Platform One/Tron",
		},
		User:  &pb.User{
			Meta:      meta,
			Id:        "test10",
			Username:  "test10",
			Email:     "test10@gmail.com",
			FirstName: "test10",
			LastName:  "test10",
		},
	})

	if err != nil {
		t.Errorf("Found error: %v", err)
		t.Fail()
	}

	if resp.GetMeta().RequestId != "2" {
		t.Errorf("Wrong request id found: %s", resp.GetMeta().GetRequestId())
		t.Fail()
	}

	if resp.GetMeta().GetRequester() != "requester" {
		t.Errorf("Unexpected requester found: %s", resp.GetMeta().GetRequester())
		t.Fail()
	}
}

func mockRemoveUserFromGroup(w http.ResponseWriter, r *http.Request, t *testing.T) {
	validateRequestMethod(r, "POST", t)
	validateAuth(r, t)

	w.WriteHeader(http.StatusNoContent)
}

func testRemoveUserFromGroup(ctx context.Context, adapter SonarqubeAdapter,  t *testing.T) {
	meta := &pb.Meta{
		RequestId: "2",
		Status:    200,
		Requester: "requester",
	}
	resp, err := adapter.RemoveUserFromGroup(&ctx, &pb.GroupMember{
		Meta: meta,
		Group: &pb.Group{
			Meta:        meta,
			Id:          "6",
			Name:        "Tron",
			Description: "",
			Path:        "/Platform One/Tron",
		},
		User:  &pb.User{
			Meta:      meta,
			Id:        "test6",
			Username:  "test6",
			Email:     "test6@gmail.com",
			FirstName: "test6",
			LastName:  "test6",
		},
	})

	if err != nil {
		t.Errorf("Found error: %v", err)
		t.Fail()
	}

	if resp.GetMeta().RequestId != "2" {
		t.Errorf("Wrong request id found: %s", resp.GetMeta().GetRequestId())
		t.Fail()
	}

	if resp.GetMeta().GetRequester() != "requester" {
		t.Errorf("Unexpected requester found: %s", resp.GetMeta().GetRequester())
		t.Fail()
	}
}

func validateUserResponse(u *pb.User, orig *pb.User, t *testing.T) {
	if u == nil {
		t.Errorf("error in user %v", u)
	}
	if u.GetUsername() != orig.GetUsername(){
		t.Errorf("error in user %v", u)
	}
	if u.GetEmail()!= orig.GetEmail(){
		t.Errorf("error in user %v", u)
	}
	if u.GetUsername() != orig.GetUsername(){
		t.Errorf("error in user %v", u)
	}
	/*
		if u.GetFirstName() != orig.GetUsername(){
			t.Errorf("error in creating user %v", u)
		}
		if u.GetLastName() != orig.GetUsername(){
			t.Errorf("error in creating user %v", u)
		}
	*/

}

func validateGenericResponse(g *pb.Generic, u *pb.User, t *testing.T) {
	if g == nil {
		t.Errorf("error Generic should not be nil")
	} else {
		if g.Meta.GetRequester() != u.GetMeta().GetRequester() {
			t.Errorf("error requester mismatch %v", g)
		}
		if g.Meta.GetRequestId() != u.GetMeta().RequestId {
			t.Errorf("error requestId mismatch %v", g)
		}
		if g.Meta.GetStatus() != 200 {
			t.Errorf("error status mismatch %v", g)
		}
	}
}

func validateQueryParam(r *http.Request, key string, value string, t *testing.T) {
	temp := r.URL.Query().Get(key)
	if  temp != value {
		t.Errorf("error in param %v should be %v found %v", key, value, temp)
	}
}

func validateRequestMethod(r *http.Request, method string, t *testing.T) {
	if r.Method != method {
		t.Errorf("error request method should be %v got %v",method, r.Method)
	}
}

func validateAuth(r *http.Request, t *testing.T) {
	u,_,_ := r.BasicAuth()
	if u != token {
		t.Errorf("error token not set correctly, got %v", u)
	}
}


