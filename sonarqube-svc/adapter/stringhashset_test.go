package adapter

import "testing"

func TestStringHashSet(t *testing.T) {
	stringSet := NewStringHashSetFromArray([]string{"test1", "test1", "test2"})

	values := stringSet.Values()
	if len(values) != 2 {
		t.Errorf("Expect 2 elements. Found %v", values)
	}

	if !contains(values, "test1") {
		t.Errorf("Expect test1 to be found %v", values)
	}

	if !contains(values, "test2") {
		t.Errorf("Expect test2 to be found %v", values)
	}
	if stringSet.IsEmpty() {
		t.Errorf("Should not be empty. Found %v", stringSet)
	}
	if stringSet.Size() != 2 {
		t.Errorf("Expect only 2 elements. Found %v", stringSet)
	}
	if !stringSet.Contains("test1") {
		t.Errorf("test1 should exist. Found %v", stringSet)
	}
	if !stringSet.Contains("test2") {
		t.Errorf("test2 should exist. Found %v", stringSet)
	}
	stringSet.Add("test3")
	if stringSet.Size() != 3 {
		t.Errorf("Expect only 3 elements. Found %v", stringSet)
	}
	if !stringSet.Contains("test3") {
		t.Errorf("test3 should exist. Found %v", stringSet)
	}

	bSet := NewStringHashSetFromArray([]string{"test1", "test2", "test4"})
	newSet := stringSet.GetItemsNotContainedIn(bSet)
	if newSet.Size() != 1 {
		t.Errorf("Expected only 1 element, found %v", newSet)
	}
	if !newSet.Contains("test3") {
		t.Errorf("test3 should exist. Found %v", stringSet)
	}

	stringSet.Remove("test1")
	if stringSet.Size() != 2{

		t.Errorf("size shoud be 4, test 1 was not removed. Found %v", stringSet)
	}


}

func contains(values []string, str string) bool {
	for _, val := range values {
		if val == str {
			return true
		}
	}
	return false
}