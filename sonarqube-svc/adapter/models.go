package adapter

// APICreateUser model
type APICreateUser struct {
	Login       string   `json:"login"`
	Name        string   `json:"name"`
	Email       string   `json:"email"`
	ScmAccounts []string `json:"scmAccounts"`
	Active      bool     `json:"active"`
	Local       bool     `json:"local"`
}

// CreateUserResposne model for gad
type CreateUserResposne struct {
	User APICreateUser `json:"user"`
}

// APIListUser model for gad
type APIListUser struct {
	Login            string   `json:"login"`
	Name             string   `json:"name"`
	Email            string   `json:"email"`
	ScmAccounts      []string `json:"scmAccounts"`
	Active           bool     `json:"active"`
	Local            bool     `json:"local"`
	ExternalIdentify string   `json:"externalIdentity"`
}

// ListUserResponse model for gad
type ListUserResponse struct {
	Users []APIListUser `json:"users"`
}

// APIGroup model for gad
type APIGroup struct {
	UUID         string `json:"uuid"`
	Organization string `json:"organization"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	MembersCount int    `json:"membersCount"`
	Default      bool   `json:"default"`
}

// CreateGroupResponse model for gad
type CreateGroupResponse struct {
	Group APIGroup `json:"group"`
}

// ListGroupResponse model for gad
type ListGroupResponse struct {
	Groups []APIGroup `json:"groups"`
}

// GroupMembersResponse model for gad
type GroupMembersResponse struct {
	Users []APICreateUser `json:"users"`
}
