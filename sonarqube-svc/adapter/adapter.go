package adapter

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"

	pb "gitlab.com/_pratheesh/gad/svc-base/proto/gen/go/service"
	"gitlab.com/bloom42/libs/rz-go"
)

var (
	client     = &http.Client{}
	usersCache = make(map[string]APIListUser)
)

// SonarqubeAdapter for GAD.
type SonarqubeAdapter struct {
	excludedUsers *StringHashSet
	baseURL       string
	token         string
	pageSize 	  string
}

// Startup Invoked in ../server/main.go at the top of main().
func (d *SonarqubeAdapter) Startup(log *rz.Logger, excludedUsers []string) {
	log.Log("Processing adapter logic")
	d.excludedUsers = NewStringHashSetFromArray(excludedUsers)
	d.baseURL = GetEnvOrExit("SONAR_URL")
	d.token = GetEnvOrExit("SONAR_TOKEN")
	pageSize := GetEnvOrExit("API_RESPONSE_PAGE_SIZE")
	_, err := strconv.Atoi(pageSize)
	if err != nil {
		log.Fatal("Unable to convert API_RESPONSE_PAGE_SIZE env to int")
	}
	d.pageSize = pageSize
}

// CreateUser for sonarqube.
func (d *SonarqubeAdapter) CreateUser(ctx *context.Context, request *pb.User) (*pb.User, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Creating user: " + request.GetUsername() + " not supported. User need to login to Sonarqube once to be created by OIDC.")

	return nil, errors.New("create User not supported. User need to login to Sonarqube once to be created by OIDC")
}

// RemoveUser from sonarqube
func (d *SonarqubeAdapter) RemoveUser(ctx *context.Context, request *pb.User) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Removing user: " + request.Username)

	// build URL request
	req, err := http.NewRequest("POST", d.baseURL+"/api/users/deactivate", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	// user creation URL parameters
	params := req.URL.Query()
	params.Add("login", request.Id)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Sonarqube returns 200 for successful deactivate of user. There is no user removal.
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Generic response to controller
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil

}

// ListUsers for sonarqube
func (d *SonarqubeAdapter) ListUsers(ctx *context.Context, request *pb.Generic) (*pb.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting all users...")

	// build URL request
	req, err := http.NewRequest("GET", d.baseURL+"/api/users/search", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	var users []*pb.User
	newUsersCache := make(map[string]APIListUser)
	for currentPage := 1; ; currentPage++ {
		params := url.Values{}
		params.Add("ps", d.pageSize)
		params.Add("p", strconv.Itoa(currentPage))
		req.URL.RawQuery = params.Encode()

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		// Gitlab API get users returns 200 if successful
		if resp.StatusCode != 200 {
			return nil, errors.New(resp.Status)
		}

		defer resp.Body.Close()

		// Read and parse API request
		var userModels ListUserResponse
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(body, &userModels)
		if err != nil {
			return nil, err
		}

		//if no more users, break out of pagination
		if len(userModels.Users) == 0 {
			break
		}

		// Update User list that will be sent to the controller
		users = updateUsersListUsers(d, userModels, newUsersCache, users)

	}
	usersCache = newUsersCache
	log.Info(fmt.Sprintf("Received %d users", len(users)))
	return &pb.Users{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Users: users,
	}, nil
}

func updateUsersListUsers(d *SonarqubeAdapter, userModels ListUserResponse, newUsersCache map[string]APIListUser, users []*pb.User) []*pb.User {
	for _, userModel := range userModels.Users {
		if d.excludedUsers.Contains(userModel.Login) {
			continue
		}
		user := pb.User{
			Id:       userModel.Login,
			Username: userModel.ExternalIdentify,
			Email:    userModel.Email,
		}
		users = append(users, &user)
		newUsersCache[userModel.Login] = userModel
	}
	return users
}

// CreateGroup for sonarqube
func (d *SonarqubeAdapter) CreateGroup(ctx *context.Context, request *pb.Group) (*pb.Group, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Creating group: " + request.GetPath())

	// build URL request
	req, err := http.NewRequest("POST", d.baseURL+"/api/user_groups/create", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	// user creation URL parameters
	params := req.URL.Query()
	params.Add("name", request.GetPath())
	params.Add("description", request.GetDescription())
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Sonarqube API user creation returns 200 if successful
	if resp.StatusCode != 200 {
		return nil, errors.New(resp.Status)
	}

	// Read response and un-marshal data
	var groupResponse CreateGroupResponse
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &groupResponse)
	if err != nil {
		return nil, err
	}

	return &pb.Group{
		Meta: &pb.Meta{
			RequestId: request.GetMeta().GetRequestId(),
			Status:    200,
			Requester: request.GetMeta().GetRequester(),
		},
		Id:          groupResponse.Group.UUID,
		Name:        groupResponse.Group.Name,
		Description: groupResponse.Group.Description,
		Path:        groupResponse.Group.Name,
	}, nil
}

// ListGroups for sonarqube
func (d *SonarqubeAdapter) ListGroups(ctx *context.Context, request *pb.Generic) (*pb.Groups, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting groups...")

	// build URL request
	req, err := http.NewRequest("GET", d.baseURL+"/api/user_groups/search", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	var groups []*pb.Group

	for currentPage := 1; ; currentPage++ {
		params := url.Values{}
		params.Add("ps", d.pageSize)
		params.Add("p", strconv.Itoa(currentPage))
		req.URL.RawQuery = params.Encode()

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		// Gitlab API get groups returns 200 if successful
		if resp.StatusCode != 200 {
			return nil, errors.New(resp.Status)
		}

		defer resp.Body.Close()

		// Read and parse API request
		var listGroupResponse ListGroupResponse
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(body, &listGroupResponse)
		if err != nil {
			return nil, err
		}

		//if no more groups, break out of pagination
		if len(listGroupResponse.Groups) == 0 {
			break
		}

		// Update User list that will be sent to the controller
		for _, groupModel := range listGroupResponse.Groups {
			group := pb.Group{
				Id:          groupModel.UUID,
				Name:        groupModel.Name,
				Description: groupModel.Description,
				Path:        groupModel.Name,
			}
			groups = append(groups, &group)
		}
	}

	log.Info(fmt.Sprintf("received %d groups", len(groups)))

	return &pb.Groups{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}, Groups: groups}, nil
}

// GetGroupMembers for sonarqube
func (d *SonarqubeAdapter) GetGroupMembers(ctx *context.Context, request *pb.Group) (*pb.Users, error) {
	log := rz.FromCtx(*ctx)
	log.Info("Getting group members for group: " + request.GetPath())

	// Gets the users cache pointer to prevent it changing while using it below
	var tempUserCache = usersCache
	// build URL request
	req, err := http.NewRequest("GET", d.baseURL+"/api/user_groups/users", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	var users []*pb.User

	for currentPage := 1; ; currentPage++ {
		params := url.Values{}
		params.Add("name", request.GetPath())
		params.Add("ps", d.pageSize)
		params.Add("p", strconv.Itoa(currentPage))
		req.URL.RawQuery = params.Encode()

		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}

		// Gitlab API get groups returns 200 if successful
		if resp.StatusCode != 200 {
			return nil, errors.New(resp.Status)
		}

		defer resp.Body.Close()

		// Read and parse API request
		var groupMembersResponse GroupMembersResponse
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(body, &groupMembersResponse)
		if err != nil {
			return nil, err
		}

		//if no more users, break out of pagination
		if len(groupMembersResponse.Users) == 0 {
			break
		}

		// Update User list that will be sent to the controller
		users = updateUserListGroupMembers(d, groupMembersResponse, tempUserCache, users)

	}

	log.Info(fmt.Sprintf("received %d users\n", len(users)))
	return &pb.Users{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}, Users: users}, nil
}

func updateUserListGroupMembers(d *SonarqubeAdapter, groupMembersResponse GroupMembersResponse, tempUserCache map[string]APIListUser,
	users []*pb.User) []*pb.User{

	for _, userModel := range groupMembersResponse.Users {
		if d.excludedUsers.Contains(userModel.Login) {
			continue
		}

		//look up cache for the external identify since that's keycloak username field
		userCache, found := tempUserCache[userModel.Login]
		if found {
			user := pb.User{
				Id:       userModel.Login,
				Username: userCache.ExternalIdentify,
				Email:    userModel.Email,
			}
			users = append(users, &user)
		}
	}
	return users
}

// AddUserToGroup for sonarqube
func (d *SonarqubeAdapter) AddUserToGroup(ctx *context.Context, request *pb.GroupMember) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info(fmt.Sprintf("Adding user(%s) for group: %s", request.User, request.Group.Path))

	// build URL request
	req, err := http.NewRequest("POST", d.baseURL+"/api/user_groups/add_user", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	// user creation URL parameters
	params := req.URL.Query()
	params.Add("login", request.User.Id)
	params.Add("name", request.Group.Path)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Sonarqube API returns 204 if successful
	if resp.StatusCode != 204 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Return success
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil
}

// RemoveUserFromGroup for sonarqube.
func (d *SonarqubeAdapter) RemoveUserFromGroup(ctx *context.Context, request *pb.GroupMember) (*pb.Generic, error) {
	log := rz.FromCtx(*ctx)
	log.Info(fmt.Sprintf("Removing user(%s) for group: %s", request.User, request.Group.Path))

	// build URL request
	req, err := http.NewRequest("POST", d.baseURL+"/api/user_groups/remove_user", nil)
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(d.token, "")

	// user creation URL parameters
	params := req.URL.Query()
	params.Add("login", request.User.Id)
	params.Add("name", request.Group.Path)
	req.URL.RawQuery = params.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Sonarqube API returns 204 if successful
	if resp.StatusCode != 204 {
		return nil, errors.New(resp.Status)
	}

	// Close out request
	defer resp.Body.Close()

	// Return response back to controller
	return &pb.Generic{Meta: &pb.Meta{
		RequestId: request.GetMeta().GetRequestId(),
		Status:    200,
		Requester: request.GetMeta().GetRequester(),
	}}, nil
}
