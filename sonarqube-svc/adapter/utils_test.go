package adapter

import (
	"os"
	"testing"
)

func TestUtils(t *testing.T) {
	os.Setenv("TESTENV", "testenv")
	result := GetEnvOrExit("TESTENV")
	if result != "testenv" {
		t.Errorf("Expect testenv to be returned. Found %v", result)
	}
}
