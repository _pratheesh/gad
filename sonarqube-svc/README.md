[![pipeline status](https://gitlab.com/_pratheesh/gad/sonarqube-svc/badges/master/pipeline.svg)](https://gitlab.com/_pratheesh/gad/sonarqube-svc/-/commits/master)

Sonarqube SVC
-------------
Sonarqube service adapter for group authorization distribution.

## Dependencies

This project depends on the [svc-base](https://gitlab.com/_pratheesh/gad/svc-base) project as well as other modules specified in [go.mod](go.mod).

## Use

This is intended to be used as part of the [group-sync-controller](https://gitlab.com/_pratheesh/gad/group-sync-controller) project.

## Behavior

|Method|Supported|Notes|
|------|---------|-----|
|CreateUser|No|A user has to login in by SSO first to be added into Sonarqube. The API could not be used because the SSO user and the one create by the API cant be tied together|
|RemoveUser|Yes|The user is deactivated and not removed completely|
|ListUser|Yes|External Identity is returned as the username|
|CreateGroup|Yes|Sonarqube does not have group hierarchy. The group is flattened as single string. For example this is the name of the Puckboard group`/Platform One/Tron/Puckboard`|
|ListGroup|Yes| For groups returned from the adapter, the path is the group name
|AddUserToGruop|Yes||
|RemoveUserFromGroup|Yes||
|GetGroupMembers|Yes||

## Development
1) Since this project has a private dependency in order to be able to pull down the svc-base module, you will need to create ~/.netrc to include your token:
```aidl
machine domain.com
  login yourusername
  password yourAccessToken
```
2) Make sure `GOPRIVATE="gitlab.com/_pratheesh/gad/svc-base"` in your `go env`

## Env Variables
```$xslt
SONAR_URL=https://sonarqube.t-rex.code_dev_url.com
SONAR_TOKEN=<your sonar token>
# the page size for all the sonarqube API responses
API_RESPONSE_PAGE_SIZE=500
```
Token can be retrieve from My Account -> Security -> Generate Token
